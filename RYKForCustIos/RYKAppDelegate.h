//
//  RYKAppDelegate.h
//  RYKForCustIos
//
//  Created by 宏超 陈 on 14-6-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RYKCustSignInController.h"

@interface RYKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) RYKCustSignInController * signIn;

@end
