//
//  RYKWmpData.m
//  RYKForCustIos
//
//  Created by hsit on 14-11-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKWmpData.h"

@interface RYKWmpData()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation RYKWmpData

- (void)parseData:(NSData *)data complete:(void (^)(NSMutableArray *array))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser *xml = [[NSXMLParser alloc] initWithData:data];
    xml.delegate = self;
    [xml parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKWmpData *item = [[RYKWmpData alloc] init];
        item.dictCode = [attributeDict objectForKey:@"dictCode"];
        item.dictName = [attributeDict objectForKey:@"dictName"];
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
