//
//  ZbCommonFunc.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-10-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZbCommonFunc : NSObject

+ (UIImage *)getImage:(CGSize)size withColor:(UIColor *)color;

@end
