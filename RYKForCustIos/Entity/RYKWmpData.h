//
//  RYKWmpData.h
//  RYKForCustIos
//
//  Created by hsit on 14-11-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKWmpData : NSObject

@property (nonatomic, strong) NSString *dictCode;
@property (nonatomic, strong) NSString *dictName;
@property (nonatomic, strong) void(^block) (NSMutableArray *);

- (void)parseData:(NSData *)data complete:(void(^)(NSMutableArray *array))block;

@end
