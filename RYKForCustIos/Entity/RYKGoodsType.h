//
//  ConsBrand.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKGoodsType : NSObject

@property (nonatomic, strong) NSString * typeName;             // 类型名称
@property (nonatomic, strong) NSString * typeCode;             // 类型值
@property (nonatomic, strong) NSMutableArray *nameArray;
@property (nonatomic, strong) NSMutableArray *codeArray;

- (void)parseData:(NSData *)data complete:(void(^)(NSArray * array))block;

@end
