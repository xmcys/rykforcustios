//
//  CustVersion.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKVersion.h"

@interface RYKVersion () <NSXMLParserDelegate>

@end

@implementation RYKVersion


- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.appName = [attributeDict objectForKey:@"appName"];
        self.appType = [attributeDict objectForKey:@"appType"];
        self.versionNo = [attributeDict objectForKey:@"versionNo"];
        self.buildCode = [attributeDict objectForKey:@"buildCode"];
        self.publishDate = [attributeDict objectForKey:@"publishDate"];
        self.content = [attributeDict objectForKey:@"content"];
        self.url = [attributeDict objectForKey:@"url"];
        
        if (![self.versionNo hasPrefix:@"v"] && ![self.versionNo hasPrefix:@"V"]) {
            self.versionNo = [NSString stringWithFormat:@"v%@", self.versionNo];
        }
    }
}


@end
