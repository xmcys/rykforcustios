//
//  RYKGoodsSortInfo.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKGoodsSortInfo.h"

@interface RYKGoodsSortInfo () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray * array);

@end

@implementation RYKGoodsSortInfo

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKGoodsSortInfo * item = [[RYKGoodsSortInfo alloc] init];
        item.brandId = [attributeDict objectForKey:@"brandId"];
        item.brandName = [attributeDict objectForKey:@"brandName"];
        item.serialNo = [attributeDict objectForKey:@"serialNo"];
        
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
