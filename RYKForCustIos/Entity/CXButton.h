//
//  CXButton.h
//  RYKForCustIos
//
//  Created by hsit on 14-11-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CXButton : UIButton

@property (nonatomic) int index;
@property (nonatomic, strong) NSString *specialId;

@end
