//
//  RYKStoreInfo.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKStoreInfo.h"

@implementation RYKStoreInfo
-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        RYKStoreInfo * item = [[RYKStoreInfo alloc] init];
        item.userName = [attributeDict objectForKey:@"userName"];
        item.linkPhone = [attributeDict objectForKey:@"linkPhone"];
        item.mobilPhone = [attributeDict objectForKey:@"mobilPhone"];
        item.userAddr = [attributeDict objectForKey:@"userAddr"];
        item.businesstime = [attributeDict objectForKey:@"businesstime"];
        if (item.businesstime == nil || [item.businesstime isEqualToString:@"null"]) {
            item.businesstime = @"";
        }
        item.storeIntro = [attributeDict objectForKey:@"storeIntro"];
        [self.array addObject:item];
    }
    
    
    
    
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
