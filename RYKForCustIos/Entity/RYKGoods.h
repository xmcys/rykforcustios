//
//  RYKGoods.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKGoods : NSObject

@property (nonatomic, strong) NSString *goodsId;         // 商品id
@property (nonatomic, strong) NSString *goodsName;       // 商品名
@property (nonatomic, strong) NSString *goodsCode;       // 条形码
@property (nonatomic, strong) NSString *goodsTypeName;   // 商品类型名
@property (nonatomic, strong) NSString *goodsTypeId;     // 商品类型id
@property (nonatomic, strong) NSString *goodsUnit;       // 商品规格
@property (nonatomic, strong) NSString *goodsPrice;      // 商品价格
@property (nonatomic) BOOL isOnSale;                     // 是否促销
@property (nonatomic) BOOL isDefaultUpload;              // 是否默认上架
@property (nonatomic, strong) NSString *onSalePrice;     // 促销价格
@property (nonatomic, strong) NSString *onSaleBeginDate;     // 促销开始日期
@property (nonatomic, strong) NSString *onSaleEndDate;      // 促销结束日期
@property (nonatomic, strong) NSString *stateName;          // 上架状态
@property (nonatomic, strong) NSString *picUrl01;           // 商品图片
@property (nonatomic, strong) NSString *picUrl02;
@property (nonatomic, strong) NSString *picUrl03;
@property (nonatomic, strong) NSString *picUrl04;
@property (nonatomic, strong) NSString *picUrl05;
@property (nonatomic, strong) NSString *brandDesc;        //产品介绍
@property (nonatomic, strong) NSString *brandPromotion;   //促销介绍


@property (nonatomic, strong) NSString *isTabacco;        // 1 卷烟 0 非烟


@property (nonatomic) BOOL isQuerySingleGoods;   

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;
- (void)infoFromOtherGoods:(RYKGoods *)tagGoods;

@end
