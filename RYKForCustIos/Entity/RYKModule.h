//
//  CustModule.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-7.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKModule : NSObject

+ (void)recordWithModCode:(NSString *)modCode operateId:(NSString *)operateId;

@end
