//
//  RYKCustomer.m
//  RYKForCustIos
//
//  Created by hsit on 14-7-1.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKCustomer.h"

@implementation RYKCustomer
-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        RYKCustomer * item = [[RYKCustomer alloc] init];
        item.userId = [attributeDict objectForKey:@"userId"];
        item.userName = [attributeDict objectForKey:@"userName"];
        item.contactName = [attributeDict objectForKey:@"contactName"];
        item.mobilphone = [attributeDict objectForKey:@"mobilphone"];
        item.sex = [attributeDict objectForKey:@"sex"];
        item.birthday = [attributeDict objectForKey:@"birthday"];
        item.userAddr = [attributeDict objectForKey:@"userAddr"];
        item.userAddrSec = [attributeDict objectForKey:@"userAddrSec"];
        if ([item.userAddrSec isEqualToString:@"null"]) {
            item.userAddrSec = @"";
        }
        item.userAddThi = [attributeDict objectForKey:@"userAddThi"];
        if ([item.userAddThi isEqualToString:@"null"]) {
            item.userAddThi = @"";
        }
        item.email = [attributeDict objectForKey:@"email"];
        item.regisTime = [attributeDict objectForKey:@"regisTime"];
        [self.array addObject:item];
    }
    
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
