//
//  RYKReplymsg.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-25.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKReplymsg : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString *answerCont;   //回复内容
@property (nonatomic, strong) NSString *answerPerson;  //回复人
@property (nonatomic, strong) NSString *opinionSeq;    //回复序列号



- (void)parseData:(NSData *)data;



@end
