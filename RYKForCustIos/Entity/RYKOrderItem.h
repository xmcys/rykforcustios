//
//  RYKOrderItem.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKOrderItem : NSObject

@property (nonatomic, strong) NSString * brandName;        // 订单商品名
@property (nonatomic, strong) NSString * brandUnit;        // 单位
@property (nonatomic, strong) NSString * brandId;          // 订单序列号
@property (nonatomic, strong) NSString * price;            // 单价
@property (nonatomic, strong) NSString * qtyOrder;         // 数量
@property (nonatomic, strong) NSString * amt;              // 金额

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
