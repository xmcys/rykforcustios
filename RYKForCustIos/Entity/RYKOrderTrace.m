//
//  RYKOrderTrace.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKOrderTrace.h"

@interface RYKOrderTrace () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(RYKOrderTrace *);

@end

@implementation RYKOrderTrace

- (void)parseData:(NSData *)data complete:(void (^)(RYKOrderTrace *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        NSString *flowCode = [attributeDict objectForKey:@"flowCode"];
        if ([flowCode isEqualToString:@"STORE_CONFIRM"]) {
            self.confirmFlowDate = [attributeDict objectForKey:@"flowDate"];
            self.confirmFlowEstTime = [attributeDict objectForKey:@"flowEstTime"];
            self.confirmFlowStatus = [attributeDict objectForKey:@"flowStatus"];
        } else if ([flowCode isEqualToString:@"DELV_PACK"]) {
            self.delvPackFlowDate = [attributeDict objectForKey:@"flowDate"];
            self.delvPackFlowEstTime = [attributeDict objectForKey:@"flowEstTime"];
            self.delvPackFlowStatus = [attributeDict objectForKey:@"flowStatus"];
        } else {
            self.finishFlowDate = [attributeDict objectForKey:@"flowDate"];
            self.finishFlowEstTime = [attributeDict objectForKey:@"flowEstTime"];
            self.finishFlowStatus = [attributeDict objectForKey:@"flowStatus"];
        }
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
