//
//  RYKGoodsUnit.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-27.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKGoodsUnit.h"

@interface RYKGoodsUnit () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray * array);

@end

@implementation RYKGoodsUnit

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        NSString *unitName = [attributeDict objectForKey:@"unitName"];
        
        [self.array addObject:unitName];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
