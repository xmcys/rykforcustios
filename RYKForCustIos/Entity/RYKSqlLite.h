//
//  TIISqlLite.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RYKChatMessage.h"

@interface RYKSqlLite : NSObject

+ (RYKSqlLite *)shareManager;
- (void)createMsgTable;
- (void)addNewMsg:(RYKChatMessage *)chatMsg;
- (NSArray *)getChatTargetList;
- (NSArray *)getMsgByTarget:(NSString *)target fromIndex:(NSInteger)startIndex withRows:(NSInteger)rows;
- (NSInteger)getUnReadCntByTarget:(NSString *)target;
- (void)changeSendStatus:(NSString *)msgDate toStatus:(NSString *)status;

@end
