//
//  CustInfo.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKCustInfo : NSObject

@property (nonatomic, strong) NSString * userId;           // 用户ID
@property (nonatomic, strong) NSString * userCode;          // 账号
@property (nonatomic, strong) NSString * userName;          // 店名
@property (nonatomic, strong) NSString * userTotalNum;      // 总关注数

@property (nonatomic, strong) NSMutableDictionary *customerDict;  // 顾客资料

- (void)parseData:(NSData *)data;
- (void)updateCustomerDict:(NSArray *)array;

@end
