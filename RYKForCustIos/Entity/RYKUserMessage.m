//
//  RYKUserMessage.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKUserMessage.h"
#import "RYKWebService.h"

@interface RYKUserMessage () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(RYKUserMessage *);

@end

@implementation RYKUserMessage

- (void)parseData:(NSData *)data complete:(void (^)(RYKUserMessage *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.user_code = [attributeDict objectForKey:@"userCode"];
        self.user_name = [attributeDict objectForKey:@"userName"];
        self.user_num = [attributeDict objectForKey:@"userNum"];
        self.order_num = [attributeDict objectForKey:@"orderNum"];
        self.userTotalNum = [attributeDict objectForKey:@"userTotalNum"];
        [RYKWebService sharedUser].userTotalNum = [attributeDict objectForKey:@"userTotalNum"];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
