//
//  RYKCustOrderStatus.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKCustOrderStatus : NSObject

@property (nonatomic, strong) NSString * orderId;         // 订单id
@property (nonatomic, strong) NSString * orderDate;       // 订单日期
@property (nonatomic, strong) NSString * orderDesc;       // 备注
@property (nonatomic, strong) NSString * qtyOrderSum;     // 数量
@property (nonatomic, strong) NSString * amtOrderSum;     // 金额
@property (nonatomic, strong) NSString * orderAddr;       // 地址
@property (nonatomic, strong) NSString * contactName;     // 姓名
@property (nonatomic, strong) NSString * mobilePhone;     // 电话
@property (nonatomic, strong) NSString * recvType;        // 送货方式
@property (nonatomic, strong) NSString * flowCode;

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
