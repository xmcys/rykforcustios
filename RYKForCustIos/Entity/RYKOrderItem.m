//
//  RYKOrderItem.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKOrderItem.h"

@interface RYKOrderItem () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation RYKOrderItem

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKOrderItem * curItem = [[RYKOrderItem alloc] init];
        curItem.brandName = [attributeDict objectForKey:@"brandName"];
        curItem.brandUnit = [attributeDict objectForKey:@"brandUnit"];
        curItem.brandId = [attributeDict objectForKey:@"brandId"];
        curItem.price = [attributeDict objectForKey:@"price"];
        curItem.amt = [attributeDict objectForKey:@"amt"];
        curItem.qtyOrder = [attributeDict objectForKey:@"qtyOrder"];
        
        [self.array addObject:curItem];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
