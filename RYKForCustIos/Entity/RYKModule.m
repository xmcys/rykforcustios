//
//  CustModule.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-7.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKModule.h"
#import "RYKWebService.h"

@interface RYKModule () <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSMutableData * data;

- (void)saveRecordWithModCode:(NSString *)modCode operateId:(NSString *)operateId;

@end

@implementation RYKModule

+ (void)recordWithModCode:(NSString *)modCode operateId:(NSString *)operateId
{
    RYKModule * cm = [[RYKModule alloc] init];
    [cm saveRecordWithModCode:modCode operateId:operateId];
}

- (void)saveRecordWithModCode:(NSString *)modCode operateId:(NSString *)operateId
{

    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    NSString * version = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSURL * mUrl = [[NSURL alloc] initWithString:[RYKWebService cmappLogInfoUrl:[RYKWebService sharedUser].userId appType:APP_TYPE version:version modCode:modCode operateId:operateId]];

    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:mUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.data = nil;
//    NSLog(@"客户模块使用记录失败：%@", [error description]);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
//        NSLog(@"客户模块使用记录失败：%@", [[httpURLResponse allHeaderFields] description]);
        self.data = nil;
    } else {
        self.data = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    NSLog(@"客户模块使用记录：%@", [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding]);
    self.data = nil;
}
@end
