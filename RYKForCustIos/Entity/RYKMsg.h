//
//  RYKMsg.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-20.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKMsg : NSObject<NSXMLParserDelegate>
@property (nonatomic, strong) NSString * opinionSeq;         // 留言序列号
@property (nonatomic, strong) NSString * opinionDate;        //留言的时间
@property (nonatomic, strong) NSString * opinionCont;        //留言的内容
@property (nonatomic, strong) NSString * userName;           //留言人
@property (nonatomic, strong) NSString * linkPhone;          //固定电话
@property (nonatomic, strong) NSString * conFlag;            //回复状态
@property (nonatomic, strong) NSString * answerCont;         //回复内容
@property (nonatomic, strong) NSString * answerPerson;       //回复人
@property (nonatomic, strong) NSString * answerDate;         //回复日期
@property (nonatomic, strong) NSString * opinionFlag;        //状态
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);



- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
