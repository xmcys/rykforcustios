//
//  ConsBrand.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKGoodsType.h"

@interface RYKGoodsType () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray * array);

@end


@implementation RYKGoodsType

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    self.nameArray = [[NSMutableArray alloc] init];
    self.codeArray = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKGoodsType * item = [[RYKGoodsType alloc] init];
        NSString *typeCode = [attributeDict objectForKey:@"brandType"];
        NSString *typeName = [attributeDict objectForKey:@"brandTypeName"];
        item.typeCode = typeCode;
        item.typeName = typeName;
        
        [self.nameArray addObject:[NSString stringWithString:typeName]];
        [self.codeArray addObject:[NSString stringWithString:typeCode]];
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
