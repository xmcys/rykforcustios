//
//  RYKUserMessage.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKUserMessage : NSObject

@property (nonatomic, strong) NSString * user_code;        // 许可证号
@property (nonatomic, strong) NSString * user_name;       // 用户名
@property (nonatomic, strong) NSString * user_num;        // 新增关注数
@property (nonatomic, strong) NSString * userTotalNum;    // 总关注数
@property (nonatomic, strong) NSString * order_num;       // 今日完成订单

- (void)parseData:(NSData *)data complete:(void(^)(RYKUserMessage *))block;

@end
