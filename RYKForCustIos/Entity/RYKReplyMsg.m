//
//  RYKReplymsg.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-25.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKReplymsg.h"

@implementation RYKReplymsg

-(void)parseData:(NSData *)data 
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"Test"]) {
        RYKReplymsg *replyMsg = [[RYKReplymsg alloc] init];
        replyMsg.opinionSeq = [attributeDict objectForKey:@"opinionSeq"];
        replyMsg.answerCont = [attributeDict objectForKey:@"answerCont"];
        replyMsg.answerPerson = [attributeDict objectForKey:@"answerPerson"];
    }
}



@end
