//
//  RYKChatMessage.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-8.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKChatMessage : NSObject

@property (nonatomic, strong) NSString *msgContent;    // 内容
@property (nonatomic, strong) NSString *contentType;   // 内容类型(1:文字 2:图片)
@property (nonatomic, strong) NSString *msgDate;       // 时间
@property (nonatomic, strong) NSString *msgTarget;     // 对象
@property (nonatomic, strong) NSString *operType;      // 操作类型(0:发 1:收)
@property (nonatomic, strong) NSString *sendStatus;    // 发送状态(0:未发送成功 1:发送成功 2:失败)
@property (nonatomic, strong) NSString *readStatus;    // 阅读状态(0:未读 1:已读)
@property (nonatomic, strong) NSString *localUrl;      // 本地图片路径(当为自己发的图片时)

@property (nonatomic, strong) NSString *showName;      // 显示名

@end
