//
//  RYKCustOrderStatus.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKCustOrderStatus.h"

@interface RYKCustOrderStatus () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation RYKCustOrderStatus

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKCustOrderStatus * curOrder = [[RYKCustOrderStatus alloc] init];
        curOrder.orderId = [attributeDict objectForKey:@"orderId"];
        curOrder.orderDesc = [attributeDict objectForKey:@"orderDesc"];
        if ([curOrder.orderDesc isEqualToString:@"null"]) {
            curOrder.orderDesc = @"";
        }
        curOrder.orderDate = [attributeDict objectForKey:@"orderDate"];
        curOrder.orderDate = [curOrder.orderDate substringToIndex:curOrder.orderDate.length - 3];
        curOrder.qtyOrderSum = [attributeDict objectForKey:@"qtyOrderSum"];
        curOrder.amtOrderSum = [attributeDict objectForKey:@"amtOrderSum"];
        curOrder.orderAddr = [attributeDict objectForKey:@"receiveAddr"];
        curOrder.contactName = [attributeDict objectForKey:@"contactName"];
        curOrder.mobilePhone = [attributeDict objectForKey:@"mobilePhone"];
        curOrder.recvType = [attributeDict objectForKey:@"recvType"];
        
        [self.array addObject:curOrder];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
