//
//  RYKChatMessage.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-8.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKChatMessage.h"

@interface RYKChatMessage ()

@end

@implementation RYKChatMessage

- (id)init
{
    self = [super init];
    if (self) {
        self.msgContent = @"";
        self.contentType = @"1";
        self.msgDate = @"";
        self.msgTarget = @"";
        self.operType = @"1";
        self.sendStatus = @"0";
        self.readStatus = @"0";
        self.localUrl = @"";
        
        self.showName = @"匿名";
    }
    return self;
}

@end
