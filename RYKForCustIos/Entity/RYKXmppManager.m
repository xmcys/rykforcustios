//
//  RYKXmppManager.m
//  RYKForCustIos
//
//  Created by hsit on 14-8-6.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKXmppManager.h"
#import "RYKChatMessage.h"
#import "RYKSqlLite.h"
#import <AudioToolbox/AudioToolbox.h>

static RYKXmppManager * XMPP_MANAGER; // 单例管理类

@interface RYKXmppManager ()

@end

@implementation RYKXmppManager

+ (RYKXmppManager* )sharedManager
{
    if (XMPP_MANAGER == nil) {
        XMPP_MANAGER = [[RYKXmppManager alloc] init];
        [XMPP_MANAGER setupStream];
    }
    return XMPP_MANAGER;
}

- (NSString *)myID
{
//    return @"1000000003";
    return [RYKWebService sharedUser].userId;
}

- (NSString *)myJIDStr
{
    return [NSString stringWithFormat:@"%@@%@", [self myID], XMPP_SERVICE_NAME];
}

- (XMPPJID *)myJID
{
    return [XMPPJID jidWithString:[self myJIDStr]];
}

- (void)setupStream
{
    self.xmppStream = [[XMPPStream alloc]init];
    [self.xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    self.xmppReconnect = [[XMPPReconnect alloc]init];
    [self.xmppReconnect activate:self.xmppStream];
}

- (BOOL)connectToServer
{
    [self.xmppStream setMyJID:[self myJID]];
    self.xmppStream.hostName = XMPP_SERVICE_HOST;
    self.xmppStream.hostPort = XMPP_SERVICE_PORT;
    
    NSError *error ;
    if (![self.xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error]) {
        NSLog(@"my connected error : %@",error.description);
        return NO;
    }
    NSLog(@"my connected success!");
    return YES;
}

- (void)disconnect
{
    [self.xmppStream disconnectAfterSending];
    return;
}

- (void)userLogin
{
    if ([self.xmppStream isConnected]) {
        NSString *psw = @"1";
        if (psw) {
            NSError *error ;
            if (![self.xmppStream authenticateWithPassword:psw error:&error]) {
                NSLog(@"error authenticate : %@",error.description);
            }
        }
    }
}

- (void)sendMessage:(NSString *)message toUser:(NSString *)user withDate:(NSString *)msgDate
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:message];
    NSXMLElement *msgEle = [NSXMLElement elementWithName:@"message"];
    [msgEle addAttributeWithName:@"type" stringValue:@"chat"];
    NSString *to = [NSString stringWithFormat:@"%@@%@", user, XMPP_SERVICE_NAME];
    [msgEle addAttributeWithName:@"to" stringValue:to];
    [msgEle addAttributeWithName:@"msgDate" stringValue:msgDate];
    [msgEle addChild:body];
    [self.xmppStream sendElement:msgEle];
}

- (void)localNotify:(RYKChatMessage *)chatMsg
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    return;
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    if (notification != nil) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        notification.alertBody = [NSString stringWithFormat:@"%@：%@", chatMsg.msgTarget, chatMsg.msgContent];
        notification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

#pragma mark - XMPPStreamDelegate
- (void)xmppStreamWillConnect:(XMPPStream *)sender
{
    NSLog(@"xmppStreamWillConnect");
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    NSLog(@"xmppStreamDidConnect");
    
    // 连接成功则登录
    [self userLogin];
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    NSLog(@"xmppStreamDidAuthenticate");
    
    // 登录成功切换状态为在线
    XMPPPresence *presence = [XMPPPresence presence];
	[[self xmppStream] sendElement:presence];
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
    NSLog(@"didNotAuthenticate:%@",error.description);
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    NSLog(@"didReceiveMessage  description: %@",message.description);
    
    NSString *msg = [[message elementForName:@"body"] stringValue];
    NSString *from = [[message attributeForName:@"from"] stringValue];
    NSArray *b = [from componentsSeparatedByString:@"@"];
    if (b.count > 0) {
        from = b[0];
    }
    
    RYKChatMessage *chatMsg = [[RYKChatMessage alloc] init];
    chatMsg.msgContent = msg;
    chatMsg.msgTarget = from;
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    NSString *dateStr = [dateFormatter stringFromDate:nowDate];
    chatMsg.msgDate = dateStr;
    if ([msg hasPrefix:@"http:"] && ([msg hasSuffix:@".jpg"] || [msg hasSuffix:@".png"])) {
        // 图片
        chatMsg.contentType = @"2";
    }
    
    [[RYKSqlLite shareManager] addNewMsg:chatMsg];
    [self localNotify:chatMsg];
    if (self.recvMsgDelegate && [self.recvMsgDelegate respondsToSelector:@selector(rykXmppManagerDidRecvMsg:)]) {
        [self.recvMsgDelegate rykXmppManagerDidRecvMsg:chatMsg];
    }
}

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message
{
    NSLog(@"didSendMessage:%@",message.description);
    NSString *msgDate = [[message attributeForName:@"msgDate"] stringValue];
    [[RYKSqlLite shareManager] changeSendStatus:msgDate toStatus:@"1"];
    if (self.recvMsgDelegate && [self.recvMsgDelegate respondsToSelector:@selector(rykXmppManagerDidSendMsg:)]) {
        [self.recvMsgDelegate rykXmppManagerDidSendMsg:msgDate];
    }
    
}

- (void)xmppStream:(XMPPStream *)sender didFailToSendMessage:(XMPPMessage *)message error:(NSError *)error
{
    NSLog(@"didFailToSendMessage:%@",error.description);
}

- (void)xmppStreamWasToldToDisconnect:(XMPPStream *)sender
{
    NSLog(@"xmppStreamWasToldToDisconnect");
}

- (void)xmppStreamConnectDidTimeout:(XMPPStream *)sender
{
    NSLog(@"xmppStreamConnectDidTimeout");
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    NSLog(@"xmppStreamDidDisconnect: %@",error.description);
}

#pragma mark - xmppReconnectDelegate
- (void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkReachabilityFlags)connectionFlags
{
    NSLog(@"didDetectAccidentalDisconnect:%u",connectionFlags);
}

- (BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkReachabilityFlags)reachabilityFlags
{
    NSLog(@"shouldAttemptAutoReconnect:%u",reachabilityFlags);
    return YES;
}

@end
