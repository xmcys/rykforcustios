//
//  RYKGoodsLib.m
//  RYKForCustIos
//
//  Created by hsit on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKGoodsLib.h"

@interface RYKGoodsLib ()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) void (^block)(NSArray *array);

@end

@implementation RYKGoodsLib

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser *xml = [[NSXMLParser alloc] initWithData:data];
    xml.delegate = self;
    [xml parse];
}

#pragma mark - NSXMLParseDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKGoodsLib *item = [[RYKGoodsLib alloc] init];
        item.sumTotal = [attributeDict objectForKey:@"sumTotal"];
        item.sumTabacco = [attributeDict objectForKey:@"sumTabacco"];
        item.sumOther = [attributeDict objectForKey:@"sumOther"];
        item.brandId = [attributeDict objectForKey:@"brandId"];
        item.brandName = [attributeDict objectForKey:@"brandName"];
        item.qtyOrders = [attributeDict objectForKey:@"qtyOrders"];
        item.barCode = [attributeDict objectForKey:@"barCode"];
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
