//
//  RYKCustName.m
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKCustName.h"

@implementation RYKCustName

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKCustName *item = [[RYKCustName alloc] init];
        item.contactName = [attributeDict objectForKey:@"contactName"];
        item.mobilePhone = [attributeDict objectForKey:@"mobilePhone"];
        
        item.orderId = [attributeDict objectForKey:@"orderId"];
        item.qtyOrderSum = [attributeDict objectForKey:@"qtyOrderSum"];
        item.amtOrderSum = [attributeDict objectForKey:@"amtOrderSum"];
        item.orderDate = [attributeDict objectForKey:@"orderDate"];

        [self.array addObject:item];
        
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}




@end
