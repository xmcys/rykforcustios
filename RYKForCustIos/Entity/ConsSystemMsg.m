//
//  CustSystemMsg.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsSystemMsg.h"

@interface ConsSystemMsg () <NSXMLParserDelegate>

@property (nonatomic, strong) NSString * tmp;
@property (nonatomic, strong) void(^block)(ConsSystemMsg *msg);

@end

@implementation ConsSystemMsg

- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (void)parseData:(NSData *)data complete:(void(^)(ConsSystemMsg *msg))block
{
    self.block = block;
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
    return;
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"code"]) {
        self.code = self.tmp;
    } else if ([elementName isEqualToString:@"msg"]){
        self.msg = self.tmp;
    } else if ([elementName isEqualToString:@"value"]){
        self.msg = self.tmp;
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]){
        self.code = [attributeDict objectForKey:@"ReturnFlag"];
    } else if ([elementName isEqualToString:@"result"]){
        self.code = [attributeDict objectForKey:@"ReturnFlag"];
        if ([self.code isEqualToString:@"1"]) {
            self.msg = [attributeDict objectForKey:@"PickupCode"];
            self.extra = [attributeDict objectForKey:@"OrderNo"];
        } else {
            self.msg = [attributeDict objectForKey:@"ErrorMsg"];
        }
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.block != nil) {
        self.block(self);
    }
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(consSystemMsgDidFinishingParse:)]) {
            [self.delegate consSystemMsgDidFinishingParse:self];
        }
    }
}

@end
