//
//  RYKCustName.h
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKCustName : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString *contactName;  //顾客姓名
@property (nonatomic, strong) NSString *mobilePhone;  //顾客电话
@property (nonatomic, strong) NSString *orderId;      //订单序列号
@property (nonatomic, strong) NSString *qtyOrderSum;  //订单量
@property (nonatomic, strong) NSString *amtOrderSum;    //订单金额
@property (nonatomic, strong) NSString *orderDate;    //订单日期
@property (nonatomic, strong) NSString *firstGoodsName;
@property (nonatomic, strong) NSString *firstGoodsPrice;
@property (nonatomic, strong) NSString *firstGoodsCnt;
@property (nonatomic, strong) NSString *firstGoodsUnit;
@property (nonatomic, strong) NSString *otherGoodsCnt;



@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);



- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;



@end
