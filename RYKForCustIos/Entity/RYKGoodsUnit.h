//
//  RYKGoodsUnit.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-27.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKGoodsUnit : NSObject

@property (nonatomic, strong) NSString *unitName;              // 规格名

- (void)parseData:(NSData *)data complete:(void(^)(NSArray * array))block;

@end
