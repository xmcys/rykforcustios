//
//  TIISqlLite.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKSqlLite.h"
#import "FMDB.h"
#import "RYKWebService.h"

#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__); } }

static RYKSqlLite * _SQLLITEMANAGER;

@interface RYKSqlLite ()

- (NSString *)dbPath;

@end

@implementation RYKSqlLite

+ (RYKSqlLite *)shareManager
{
    if (_SQLLITEMANAGER == nil) {
        _SQLLITEMANAGER = [[RYKSqlLite alloc] init];
    }
    return  _SQLLITEMANAGER;
}

- (NSString *)dbPath
{
    NSString *dbFileName = [NSString stringWithFormat:@"%@.sqlite", [RYKWebService sharedUser].userId];
    NSString* docsdir = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString* dbpath = [docsdir stringByAppendingPathComponent:dbFileName];
    return dbpath;
}

- (FMDatabase *)openDB
{
    NSString *dbPath = [self dbPath];
    FMDatabase *db= [FMDatabase databaseWithPath:dbPath];
    if (![db open]) {
        NSLog(@"Could not open db.");
        return nil;
    }
    return db;
}

- (void)createMsgTable
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS MsgData (msgId int identity(1,1) PRIMARY KEY,msgContent text,contentType text,msgDate text,msgTarget text,operType text,sendStatus text,readStatus text,localUrl text)"];
    [db close];
    return;
}

- (void)addNewMsg:(RYKChatMessage *)chatMsg
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    
    [db executeUpdate:@"INSERT INTO MsgData (msgContent,contentType,msgDate,msgTarget,operType,sendStatus,readStatus,localUrl) VALUES(?,?,?,?,?,?,?,?)", chatMsg.msgContent, chatMsg.contentType, chatMsg.msgDate, chatMsg.msgTarget, chatMsg.operType, chatMsg.sendStatus, chatMsg.readStatus, chatMsg.localUrl];
    
    [db close];
    return;
}

- (RYKChatMessage *)chatMsgFromRS:(FMResultSet *)rs
{
    RYKChatMessage *chatMsg = [[RYKChatMessage alloc] init];
    chatMsg.msgContent = [rs stringForColumn:@"msgContent"];
    chatMsg.contentType = [rs stringForColumn:@"contentType"];
    chatMsg.msgDate = [rs stringForColumn:@"msgDate"];
    chatMsg.msgTarget = [rs stringForColumn:@"msgTarget"];
    chatMsg.operType = [rs stringForColumn:@"operType"];
    chatMsg.sendStatus = [rs stringForColumn:@"sendStatus"];
    chatMsg.readStatus = [rs stringForColumn:@"readStatus"];
    chatMsg.localUrl = [rs stringForColumn:@"localUrl"];
    
    NSArray *d = [chatMsg.msgDate componentsSeparatedByString:@"."];
    chatMsg.msgDate = d[0];
    
    NSString *userName = [[RYKWebService sharedUser].customerDict objectForKey:chatMsg.msgTarget];
    if (userName) {
        chatMsg.showName = userName;
    }
    return chatMsg;
}

- (NSArray *)getChatTargetList
{
    NSMutableArray *msgArray = [[NSMutableArray alloc] init];
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return msgArray;
    }
    
    FMResultSet *rs = [db executeQuery:@"select * from  MsgData m join (select msgTarget,max(msgDate) as time from MsgData group by msgTarget) as tem on  tem.time=m.msgDate and tem.msgTarget=m.msgTarget order by msgDate desc"];
    while ([rs next]) {
        [msgArray addObject:[self chatMsgFromRS:rs]];
    }
    [rs close];
    [db close];
    return msgArray;
}

- (NSArray *)getMsgByTarget:(NSString *)target fromIndex:(NSInteger)startIndex withRows:(NSInteger)rows
{
    NSMutableArray *msgArray = [[NSMutableArray alloc] init];
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return msgArray;
    }
    
    FMResultSet *rs = [db executeQueryWithFormat:@"SELECT * FROM MsgData WHERE msgTarget = %@ ORDER BY msgDate DESC LIMIT %d, %d", target, startIndex, rows];
    while ([rs next]) {
        [msgArray addObject:[self chatMsgFromRS:rs]];
    }
    
    [db executeUpdateWithFormat:@"UPDATE MsgData SET readStatus = 1 WHERE msgTarget = %@ AND operType = 1", target];
    [rs close];
    [db close];
    return msgArray;
}

- (NSInteger)getUnReadCntByTarget:(NSString *)target
{
    NSInteger unReadCnt = 0;
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return unReadCnt;
    }
    
    FMResultSet *rs = [db executeQueryWithFormat:@"SELECT COUNT(*) AS UnReadCnt FROM MsgData WHERE msgTarget = %@ AND readStatus = 0 AND operType = 1", target];
    while ([rs next]) {
        NSString *result = [rs stringForColumn:@"UnReadCnt"];
        unReadCnt = [result intValue];
    }
    [rs close];
    [db close];
    return unReadCnt;
}

- (void)changeSendStatus:(NSString *)msgDate toStatus:(NSString *)status
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    
    [db executeUpdateWithFormat:@"UPDATE MsgData SET sendStatus = %@ WHERE msgDate = %@ AND operType = 0", status, msgDate];
    
    [db close];
    return;
}

@end
