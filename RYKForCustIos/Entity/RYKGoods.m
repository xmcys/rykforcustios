//
//  RYKGoods.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKGoods.h"
#import "RYKWebService.h"

@interface RYKGoods () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation RYKGoods

- (id)init
{
    self = [super init];
    if (self) {
        self.goodsCode = @"";
        self.goodsId = @"";
        self.goodsPrice = @"";
        self.goodsName = @"";
        self.goodsTypeName = @"";
        self.goodsTypeId = @"";
        self.goodsUnit = @"";
        self.isDefaultUpload = YES;
        self.isOnSale = NO;
        self.onSalePrice = @"";
        self.onSaleBeginDate = @"";
        self.onSaleEndDate = @"";
        self.picUrl01 = @"";
        self.picUrl02 = @"";
        self.picUrl03 = @"";
        self.picUrl04 = @"";
        self.picUrl05 = @"";
        self.brandDesc = @"";
        self.brandPromotion = @"";
        self.isQuerySingleGoods = NO;
    }
    return self;
}

- (void)infoFromOtherGoods:(RYKGoods *)tagGoods
{
    self.goodsCode = [NSString stringWithString:tagGoods.goodsCode];
    self.goodsId = [NSString stringWithString:tagGoods.goodsId];
    self.goodsPrice = [NSString stringWithString:tagGoods.goodsPrice];
    self.goodsName = [NSString stringWithString:tagGoods.goodsName];
    self.goodsTypeName = [NSString stringWithString:tagGoods.goodsTypeName];
    self.goodsTypeId = [NSString stringWithString:tagGoods.goodsTypeId];
    self.goodsUnit = [NSString stringWithString:tagGoods.goodsUnit];
    self.isOnSale = tagGoods.isOnSale;
    self.onSalePrice = [NSString stringWithString:tagGoods.onSalePrice];
    self.onSaleBeginDate = [NSString stringWithString:tagGoods.onSaleBeginDate];
    self.onSaleEndDate = [NSString stringWithString:tagGoods.onSaleEndDate];
    self.picUrl01 = [NSString stringWithString:tagGoods.picUrl01];
    self.picUrl02 = [NSString stringWithString:tagGoods.picUrl02];
    self.picUrl03 = [NSString stringWithString:tagGoods.picUrl03];
    self.picUrl04 = [NSString stringWithString:tagGoods.picUrl04];
    self.picUrl05 = [NSString stringWithString:tagGoods.picUrl05];
    self.brandDesc = [NSString stringWithString:tagGoods.brandDesc];
    self.brandPromotion = [NSString stringWithString:tagGoods.brandPromotion];
    return;
}

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        RYKGoods * curGoods = [[RYKGoods alloc] init];
        curGoods.goodsId = [attributeDict objectForKey:@"brandId"];
        curGoods.goodsName = [attributeDict objectForKey:@"brandName"];
        curGoods.goodsUnit = [attributeDict objectForKey:@"unit"];
        curGoods.goodsPrice = [attributeDict objectForKey:@"retailPrice"];
        curGoods.goodsTypeName = [attributeDict objectForKey:@"brandTypeName"];
        curGoods.goodsTypeId = [attributeDict objectForKey:@"brandType"];
        curGoods.goodsCode = [attributeDict objectForKey:@"barCode"];
        curGoods.stateName = [attributeDict objectForKey:@"usedFlag"];
        curGoods.onSaleBeginDate = [attributeDict objectForKey:@"startDate"];
        curGoods.onSaleEndDate = [attributeDict objectForKey:@"endDate"];
        curGoods.onSalePrice = [attributeDict objectForKey:@"finalPrice"];
        curGoods.picUrl01 = [attributeDict objectForKey:@"picUrl01"];
        curGoods.picUrl02 = [attributeDict objectForKey:@"picUrl02"];
        curGoods.picUrl03 = [attributeDict objectForKey:@"picUrl03"];
        curGoods.picUrl04 = [attributeDict objectForKey:@"picUrl04"];
        curGoods.picUrl05 = [attributeDict objectForKey:@"picUrl05"];
        curGoods.brandDesc = [attributeDict objectForKey:@"brandDesc"];
        curGoods.brandPromotion = [attributeDict objectForKey:@"brandPromotion"];
        
        if ([curGoods.picUrl01 isEqualToString:@""] || [curGoods.picUrl01 isEqualToString:@"null"]) {
            curGoods.picUrl01 = @"";
        } else {
            curGoods.picUrl01 = [RYKWebService itemImageFullUrl:curGoods.picUrl01];
        }
        
        if ([curGoods.picUrl02 isEqualToString:@""] || [curGoods.picUrl02 isEqualToString:@"null"]) {
            curGoods.picUrl02 = @"";
        } else {
            curGoods.picUrl02 = [RYKWebService itemImageFullUrl:curGoods.picUrl02];
        }
        
        if ([curGoods.picUrl03 isEqualToString:@""] || [curGoods.picUrl03 isEqualToString:@"null"]) {
            curGoods.picUrl03 = @"";
        } else {
            curGoods.picUrl03 = [RYKWebService itemImageFullUrl:curGoods.picUrl03];
        }
        
        if ([curGoods.picUrl04 isEqualToString:@""] || [curGoods.picUrl04 isEqualToString:@"null"]) {
            curGoods.picUrl04 = @"";
        } else {
            curGoods.picUrl04 = [RYKWebService itemImageFullUrl:curGoods.picUrl04];
        }
        
        if ([curGoods.picUrl05 isEqualToString:@""] || [curGoods.picUrl05 isEqualToString:@"null"]) {
            curGoods.picUrl05 = @"";
        } else {
            curGoods.picUrl05 = [RYKWebService itemImageFullUrl:curGoods.picUrl05];
        }
        
        if ([curGoods.brandDesc isEqualToString:@"null"] || [curGoods.brandDesc isEqualToString:@""]) {
            curGoods.brandDesc = @"";
        }
        if ([curGoods.brandPromotion isEqualToString:@"null"] || [curGoods.brandPromotion isEqualToString:@""]) {
            curGoods.brandPromotion = @"";
        }

        
        

        curGoods.isOnSale = YES;
        if ([[attributeDict objectForKey:@"discountFlag"] isEqualToString:@"0"]) {
            curGoods.isOnSale = NO;
            curGoods.onSaleBeginDate = @"";
            curGoods.onSaleEndDate = @"";
            curGoods.onSalePrice = @"";
        }
        if (curGoods.onSaleEndDate.length > 11) {
            curGoods.onSaleBeginDate = [curGoods.onSaleBeginDate substringToIndex:11];
        }
        if (curGoods.onSaleEndDate.length > 11) {
            curGoods.onSaleEndDate = [curGoods.onSaleEndDate substringToIndex:11];
        }
        
        [self.array addObject:curGoods];
        if (self.isQuerySingleGoods) {
            self.goodsId = [attributeDict objectForKey:@"brandId"];
            self.goodsName = [attributeDict objectForKey:@"brandName"];
            self.goodsPrice = [attributeDict objectForKey:@"retailPrice"];
        }
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
