//
//  ConsPikcer.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-11.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RYKPikcer;

@protocol RYKPikcerDelegate <NSObject>

- (void)rykPickerDelegate:(RYKPikcer *)picker didSelectRowAtIndex:(int)index;

@end

@interface PickerItem : NSObject

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSString * value;
@property (nonatomic) BOOL selected;

@end

@interface RYKPikcer : UIView

@property (nonatomic, strong) NSMutableArray * itemArray;
@property (nonatomic) id<RYKPikcerDelegate> delegate;
@property (nonatomic, strong) UIImageView * backgroundImage;
@property (nonatomic) int selectedIndex;

- (void)show;
- (void)hide;
- (void)reloadData;
- (void)setTitle:(NSString *)title;

@end
