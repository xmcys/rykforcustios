//
//  RYKXmppManager.h
//  RYKForCustIos
//
//  Created by hsit on 14-8-6.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
#import "RYKWebService.h"

@class RYKChatMessage;
@protocol rykXmppManagerRecvMsgDelegate <NSObject>

- (void)rykXmppManagerDidRecvMsg:(RYKChatMessage *)chatMsg;

@optional
- (void)rykXmppManagerDidSendMsg:(NSString *)msgDate;

@end

@interface RYKXmppManager : NSObject<XMPPStreamDelegate, XMPPReconnectDelegate>

@property(nonatomic,strong)XMPPStream*  xmppStream;
@property (nonatomic, strong) XMPPReconnect *xmppReconnect;
@property (nonatomic, weak) id<rykXmppManagerRecvMsgDelegate> recvMsgDelegate;

+ (RYKXmppManager* )sharedManager;//单例初始化方法
- (void)setupStream;
- (void)disconnect;
- (BOOL)connectToServer;
- (void)sendMessage:(NSString *)message toUser:(NSString *)user withDate:(NSString *)msgDate;

@end
