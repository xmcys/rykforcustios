//
//  RYKCustOrder.h
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKCustOrder : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSString *qtyOrderSum;  //总订单量
@property (nonatomic, strong) NSString *amtOderSum;   //订单金额
@property (nonatomic ,strong) void(^block)(RYKCustOrder *msg);

- (void)parseData:(NSData *)data complete:(void(^)(RYKCustOrder *msg))block;





@end
