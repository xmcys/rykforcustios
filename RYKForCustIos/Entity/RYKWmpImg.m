//
//  RYKWmpImg.m
//  RYKForCustIos
//
//  Created by hsit on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKWmpImg.h"

@interface RYKWmpImg ()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) void(^block)(RYKWmpImg *item);

@end

@implementation RYKWmpImg

- (void)parseData:(NSData *)data complete:(void (^)(RYKWmpImg *))block
{
    self.array = [[NSMutableArray alloc] init];
    self.block = block;
    NSXMLParser *xml = [[NSXMLParser alloc] initWithData:data];
    xml.delegate = self;
    [xml parse];
}
#pragma mark - NSXMLParseDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.userId = [attributeDict objectForKey:@"userId"];
        self.menuCode = [attributeDict objectForKey:@"menuCode"];
        self.menuName = [attributeDict objectForKey:@"menuName"];
        self.picUrl = [attributeDict objectForKey:@"picUrl"];
        self.flag = [attributeDict objectForKey:@"flag"];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
