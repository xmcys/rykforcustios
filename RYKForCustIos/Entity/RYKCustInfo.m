//
//  CustInfo.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKCustInfo.h"
#import "RYKCustomer.h"

@interface RYKCustInfo () <NSXMLParserDelegate>

@end

@implementation RYKCustInfo

- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (void)updateCustomerDict:(NSArray *)array
{
    self.customerDict = [[NSMutableDictionary alloc] init];
    for (RYKCustomer *customer in array) {
        [self.customerDict setObject:customer.userName forKey:customer.userId];
    }
    return;
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.userId = [attributeDict objectForKey:@"userId"];
    }
}

@end
