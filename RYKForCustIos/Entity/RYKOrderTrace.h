//
//  RYKOrderTrace.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKOrderTrace : NSObject

@property (nonatomic, strong) NSString * confirmFlowStatus;
@property (nonatomic, strong) NSString * confirmFlowEstTime;
@property (nonatomic, strong) NSString * confirmFlowDate;

@property (nonatomic, strong) NSString * delvPackFlowStatus;
@property (nonatomic, strong) NSString * delvPackFlowEstTime;
@property (nonatomic, strong) NSString * delvPackFlowDate;

@property (nonatomic, strong) NSString * finishFlowStatus;
@property (nonatomic, strong) NSString * finishFlowEstTime;
@property (nonatomic, strong) NSString * finishFlowDate;

- (void)parseData:(NSData *)data complete:(void(^)(RYKOrderTrace *))block;

@end
