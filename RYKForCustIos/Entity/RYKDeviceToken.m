//
//  RYKDeviceToken.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-1.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKDeviceToken.h"

static RYKDeviceToken * DEVICETOKEN; // 当前用户

@implementation RYKDeviceToken

+ (RYKDeviceToken *)sharedManager
{
    if (DEVICETOKEN == nil) {
        DEVICETOKEN = [[RYKDeviceToken alloc] init];
        DEVICETOKEN.deviceToken = @"";
    }
    return DEVICETOKEN;
}


@end
