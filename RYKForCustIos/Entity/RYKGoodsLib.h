//
//  RYKGoodsLib.h
//  RYKForCustIos
//
//  Created by hsit on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKGoodsLib : NSObject

@property (nonatomic, strong) NSString *sumTotal;
@property (nonatomic, strong) NSString *sumTabacco;
@property (nonatomic, strong) NSString *sumOther;
@property (nonatomic, strong) NSString *brandId;
@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) NSString *qtyOrders;
@property (nonatomic, strong) NSString *barCode;

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
