//
//  RYKStoreInfo.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKStoreInfo : NSObject<NSXMLParserDelegate>
@property (nonatomic, strong) NSString * userName;         // 店铺名称
@property (nonatomic, strong) NSString * linkPhone;        // 固定电话
@property (nonatomic, strong) NSString * mobilPhone;      // 移动电话
@property (nonatomic, strong) NSString * userAddr;        // 店铺地址
@property (nonatomic, strong) NSString * businesstime;      // 营业时间
@property (nonatomic, strong) NSString * storeIntro;        // 商家介绍
//@property (nonatomic, strong) NSString * businessScope;
@property (nonatomic, strong) NSString * delvIntro;         //送货信息
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);



- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;



@end
