//
//  RYKMsg.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-20.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKMsg.h"
#import "RYKWebService.h"
@implementation RYKMsg



-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        RYKMsg * item = [[RYKMsg alloc] init];
        item.opinionSeq = [attributeDict objectForKey:@"opinionSeq"];
        item.userName = [attributeDict objectForKey:@"userName"];
        item.opinionCont = [attributeDict objectForKey:@"opinionCont"];
        item.opinionDate = [attributeDict objectForKey:@"opinionDate"];
        item.linkPhone = [attributeDict objectForKey:@"linkPhone"];
        item.conFlag = [attributeDict objectForKey:@"conFlag"];
        item.answerPerson = [attributeDict objectForKey:@"answerPerson"];
        item.answerCont = [attributeDict objectForKey:@"answerCont"];
        item.answerDate = [attributeDict objectForKey:@"answerDate"];
        item.opinionFlag = [attributeDict objectForKey:@"opinionFlag"];

        [self.array addObject:item];
    }




}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
