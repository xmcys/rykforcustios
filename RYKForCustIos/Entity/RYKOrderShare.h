//
//  RYKOrderShare.h
//  RYKForCustIos
//
//  Created by hsit on 14-11-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKOrderShare : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userCode;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *shareUrl;
@property (nonatomic, strong) NSString *summary;

@property (nonatomic, strong) void (^block)(RYKOrderShare *item);

- (void)parseData:(NSData *)data complete:(void(^)(RYKOrderShare *item))block;

@end
