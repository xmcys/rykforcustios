//
//  CTTableView.m
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-2.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import "MyTableView.h"
#import <QuartzCore/QuartzCore.h>

#define CTTVHFView_HEIGHT 50

// 声明一个Header和Footer状态视图
@interface CTTVHFView : UIView

@property (nonatomic, strong) UILabel * title;
@property (nonatomic, strong) UILabel * lastUpdatedTime;
@property (nonatomic, strong) UIImageView * arrow;
@property (nonatomic, strong) UIActivityIndicatorView * indicator;
@property (nonatomic) int status;

@end

@implementation CTTVHFView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        // indicator默认为20x20大小
        self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.indicator.center = CGPointMake(CTTVHFView_HEIGHT, CTTVHFView_HEIGHT / 2);
        self.indicator.hidesWhenStopped = YES;
        [self addSubview:self.indicator];
        
        // 初始化箭头
        self.arrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 40)];
        self.arrow.center = CGPointMake(CTTVHFView_HEIGHT, CTTVHFView_HEIGHT / 2);
        self.arrow.image = [UIImage imageNamed:@"CTPullRefreshArrow"];
        [self addSubview:self.arrow];
        
        // 初始化标题
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 18)];
        self.title.center = CGPointMake(self.frame.size.width / 2, CTTVHFView_HEIGHT / 2 - 9);
        self.title.font = [UIFont boldSystemFontOfSize:12.0f];
        self.title.textColor = [UIColor darkGrayColor];
        self.title.backgroundColor = [UIColor clearColor];
        self.title.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.title];
        
        // 初始化更新时间
        self.lastUpdatedTime = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 18)];
        self.lastUpdatedTime.center = CGPointMake(self.frame.size.width / 2, CTTVHFView_HEIGHT / 2 + 9);
        self.lastUpdatedTime.font = [UIFont systemFontOfSize:12.0f];
        self.lastUpdatedTime.textColor = [UIColor lightGrayColor];
        self.lastUpdatedTime.backgroundColor = [UIColor clearColor];
        self.lastUpdatedTime.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.lastUpdatedTime];
    }
    return  self;
}

@end

@interface CTTableView ()

@property (nonatomic, strong) CTTVHFView * pullDownView;
@property (nonatomic, strong) CTTVHFView * pullUpView;
@property (nonatomic, strong) NSDateFormatter * dateFmt;

// 设置一些默认值
- (void)setDefault;

@end

@implementation CTTableView

-(id)init
{
    NSAssert(1 != 1, @"[CTTableView-init] 请使用其他实例化方法");
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setDefault];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setDefault];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    if (self = [super initWithFrame:frame style:style]) {
        [self setDefault];
    }
    return self;
}

// 为了让几个设置有效（如bgDefaultClear），需要将代码写在drawRect中
- (void)drawRect:(CGRect)rect
{
    if (self.bgDefaultClear) {
        // 将背景默认设置为透明
        self.backgroundColor = [UIColor clearColor];
        // Grouped样式需要设置backgroundView
        if (self.style == UITableViewStyleGrouped) {
            self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
            self.backgroundView.backgroundColor = [UIColor clearColor];
        }
    }
    
    [self addSubview:self.pullDownView];
    [self setTableFooterView:self.pullUpView];
}

- (void)setDefault
{
    self.dateFmt = [[NSDateFormatter alloc] init];
    [self.dateFmt setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    self.pullDownView = [[CTTVHFView alloc] initWithFrame:CGRectMake(0, -CTTVHFView_HEIGHT, self.frame.size.width, CTTVHFView_HEIGHT)];
    self.pullDownView.title.text = @"下拉可以刷新...";
    self.pullDownView.lastUpdatedTime.text = @"最后更新：从未刷新";
    
    self.pullUpView = [[CTTVHFView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, CTTVHFView_HEIGHT)];
    self.pullUpView.title.text = @"更多...";
    self.pullUpView.lastUpdatedTime.text = @"最后加载：从未加载";
    self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    
    self.status = CTTableViewStatusNormal;
    self.bgDefaultClear = YES;
    self.pullDownViewEnabled = YES;
    self.pullUpViewEnabled = YES;
    self.hidesPullUpViewWhenEmpty = YES;
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)ctTableViewDidScroll
{
    if (self.status == CTTableViewStatusLoading) {
        return;
    }
    
    CGFloat offsetY = self.contentOffset.y;
    
    // 下拉超过CTTVHFView_HEIGHT
    if (self.pullDownViewEnabled && offsetY < -CTTVHFView_HEIGHT) {
        [UIView animateWithDuration:0.2 animations:^{
            self.pullDownView.title.text = @"松开即可刷新...";
            self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            self.pullDownView.status = CTTableViewStatusPullDown;
        }];
        return;
    }
    
    if (self.pullDownView.status == CTTableViewStatusPullDown) {
        // 恢复下拉视图
        [UIView animateWithDuration:0.2 animations:^{
            self.pullDownView.title.text = @"下拉可以刷新...";
            self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
            self.pullDownView.status = CTTableViewStatusNormal;
        }];
    }
    
    // 如果上拖视图未被启用，或者被隐藏，或者被标识为“已加载全部数据”，则不进行动画
    if (!self.pullUpViewEnabled || self.pullUpView.hidden || self.pullUpView.status == CTTableViewStatusAllLoaded) {
        return;
    }
    
    int diffY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) + CTTVHFView_HEIGHT / 2 : CTTVHFView_HEIGHT / 2;
    
    if (offsetY > diffY) {
        [UIView animateWithDuration:0.2 animations:^{
            self.pullUpView.title.text = @"松开即可加载...";
            self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
            self.pullUpView.status = CTTableViewStatusPullUp;
        }];
        return;
    }
    
    if (self.pullUpView.status == CTTableViewStatusPullUp) {
        [UIView animateWithDuration:0.2 animations:^{
            self.pullUpView.title.text = @"更多...";
            self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            self.pullUpView.status = CTTableViewStatusNormal;
        }];
    }
    
}

- (void)ctTableViewDidEndDragging
{
    if (self.status == CTTableViewStatusLoading) {
        return;
    }
    
    CGFloat offsetY = self.contentOffset.y;
    
    // 如果下拉视图被启用，并且下拉超过CTTVHFView_HEIGHT
    if (self.pullDownViewEnabled && offsetY < -CTTVHFView_HEIGHT) {
        if ([self.ctTableViewDelegate respondsToSelector:@selector(ctTableViewDidPullDownToRefresh:)]) {
            if ([self.ctTableViewDelegate ctTableViewDidPullDownToRefresh:self]) {
                self.status = CTTableViewStatusLoading;
                self.pullDownView.status = CTTableViewStatusLoading;
                self.pullDownView.title.text = @"正在刷新...";
                self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                [self.pullDownView.indicator startAnimating];
                self.pullDownView.arrow.hidden = YES;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                [UIView animateWithDuration:0.2 animations:^{
                    self.contentInset = UIEdgeInsetsMake(CTTVHFView_HEIGHT * 1.2, 0, 0, 0);
                }];
            }
        }
        return;
    }
    
    // 如果上拖视图未被启用，或者被隐藏，或者被标识为“已加载全部数据”，则不进行动画
    if (!self.pullUpViewEnabled || self.pullUpView.hidden || self.pullUpView.status == CTTableViewStatusAllLoaded) {
        return;
    }
    
    // 当table中内容小于1屏时，设置偏差判断为 1/2 的CTTVHFView_HEIGHT，当内容超过1屏时，先扣去1屏的大小再设置偏差判断为剩余大小加1/2 CTTVHFView_HEIGHT
    int diffY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) + CTTVHFView_HEIGHT / 2 : CTTVHFView_HEIGHT / 2;
    
    if (offsetY > diffY) {
        if ([self.ctTableViewDelegate respondsToSelector:@selector(ctTableViewDidPullUpToLoad:)]) {
            if ([self.ctTableViewDelegate ctTableViewDidPullUpToLoad:self]) {
                self.status = CTTableViewStatusLoading;
                self.pullUpView.status = CTTableViewStatusLoading;
                self.pullUpView.title.text = @"正在加载...";
                self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
                [self.pullUpView.indicator startAnimating];
                self.pullUpView.arrow.hidden = YES;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            }
        }
        return;
    }
}

- (void)reloadData
{
    [super reloadData];
    self.status = CTTableViewStatusNormal;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    // 重置 下拉刷新 视图
    if (self.pullDownView.status != CTTableViewStatusNormal) {
        self.pullDownView.status = CTTableViewStatusNormal;
        self.pullDownView.title.text = @"下拉可以刷新...";
        self.pullDownView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后更新：%@", [self.dateFmt stringFromDate:[NSDate date]]];
        self.pullUpView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后加载：%@", [self.dateFmt stringFromDate:[NSDate date]]];
        [self.pullDownView.indicator stopAnimating];
        self.pullDownView.arrow.hidden = NO;
        [UIView animateWithDuration:0.4 animations:^{
            self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
            self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        } completion:^(BOOL isFinish){
            if ([self.ctTableViewDelegate respondsToSelector:@selector(pullDownRefreshAnimationEnd:)]) {
                [self.ctTableViewDelegate pullDownRefreshAnimationEnd:self];
            }
        }];
    }
    
    // 重置 上拖加载 视图
    if (self.pullUpView.status != CTTableViewStatusNormal) {
        self.pullUpView.status = CTTableViewStatusNormal;
        self.pullUpView.title.text = @"更多...";
        self.pullUpView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后加载：%@", [self.dateFmt stringFromDate:[NSDate date]]];
        [self.pullUpView.indicator stopAnimating];
        self.pullUpView.arrow.hidden = NO;
        [UIView animateWithDuration:0.2 animations:^{
            self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        }];
    }
    
    if (self.pullUpViewEnabled) {
        if (self.hidesPullUpViewWhenEmpty && self.contentSize.height - CTTVHFView_HEIGHT <= 0) {
            // 当“上拖加载”视图被启用，并且当前table内容为空时，隐藏“上拖加载”视图
            self.pullUpView.hidden = YES;
        } else {
            self.pullUpView.hidden = NO;
        }
    }
}

- (void)setHidesPullUpViewWhenEmpty:(BOOL)hidesPullUpViewWhenEmpty
{
    _hidesPullUpViewWhenEmpty = hidesPullUpViewWhenEmpty;
    if (self.pullUpViewEnabled) {
        if (_hidesPullUpViewWhenEmpty && self.contentSize.height - CTTVHFView_HEIGHT <= 0) {
            // 当“上拖加载”视图被启用，并且当前table内容为空时，隐藏“上拖加载”视图
            self.pullUpView.hidden = YES;
        } else {
            self.pullUpView.hidden = NO;
        }
    }
}

- (void)setPullDownViewEnabled:(BOOL)pullDownViewEnabled
{
    _pullDownViewEnabled = pullDownViewEnabled;
    if (_pullDownViewEnabled) {
        self.pullDownView.hidden = NO;
    } else {
        self.pullDownView.hidden = YES;
    }
}

- (void)setPullUpViewEnabled:(BOOL)pullUpViewEnabled
{
    _pullUpViewEnabled = pullUpViewEnabled;
    if (_pullUpViewEnabled) {
        self.pullUpView.hidden = NO;
    } else {
        self.pullUpView.hidden = YES;
    }
}

// 标识数据已全部加载（此时上拖不会再执行相关方法）
- (void)dataAllLoaded
{
    self.pullUpView.status = CTTableViewStatusAllLoaded;
    self.pullUpView.title.text = @"已加载全部数据";
    [self.pullUpView.indicator stopAnimating];
    self.pullUpView.arrow.hidden = YES;
    self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
}

// 设置最后更新时间 yyyy-MM-dd HH:mm
- (void)setLastestUpdatedTime:(NSString *)time
{
    self.pullDownView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后更新：%@", time];
    self.pullUpView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后加载：%@", time];
}

- (void)showPullDownView
{
    if (self.pullDownViewEnabled) {
        self.status = CTTableViewStatusLoading;
        self.pullDownView.status = CTTableViewStatusLoading;
        self.pullDownView.title.text = @"正在刷新...";
        self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        [self.pullDownView.indicator startAnimating];
        self.pullDownView.arrow.hidden = YES;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [UIView animateWithDuration:0.2 animations:^{
            self.contentInset = UIEdgeInsetsMake(CTTVHFView_HEIGHT * 1.2, 0, 0, 0);
        }];
    }
}

@end
