//
//  MessageCell.m
//  15-QQ聊天布局
//
//  Created by Liu Feng on 13-12-3.
//  Copyright (c) 2013年 Liu Feng. All rights reserved.
//

#import "MessageCell.h"
#import "MessageFrame.h"
#import "RYKChatMessage.h"
#import "CTImageView.h"
#import "TGRImageViewController.h"
#import "OHAttributedLabel.h"

#define IMAGE_CACHE_PATH ([NSString stringWithFormat:@"%@/images", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]])

@interface MessageCell ()<OHAttributedLabelDelegate, CTImageViewDelegate>
{
    UIButton     *_timeBtn;
    UILabel *_nameLabel;
    UIButton    *_contentBtn;
    UIImageView    *_imageView;
    UIImageView    *_warnningImageView;
}

@end

@implementation MessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
#warning 必须先设置为clearColor，否则tableView的背景会被遮住
        self.backgroundColor = [UIColor clearColor];
        
        // 1、创建时间按钮
        _timeBtn = [[UIButton alloc] init];
        [_timeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _timeBtn.titleLabel.font = kTimeFont;
        _timeBtn.enabled = NO;
        [self.contentView addSubview:_timeBtn];
        
        // 2、创建头像
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_nameLabel];
        
        // 3、创建内容
        _contentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_contentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        _contentBtn.titleLabel.font = kContentFont;
        _contentBtn.titleLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentBtn];
        
        // 4、转圈圈
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading"]];
        _imageView.frame = CGRectMake(0, 0, 16, 16);
        [self.contentView addSubview:_imageView];
        
        // 5、发送失败图片
        _warnningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"warm"]];
        _warnningImageView.frame = CGRectMake(0, 0, 16, 16);
        [self.contentView addSubview:_warnningImageView];
    }
    return self;
}

- (void)setMessageFrame:(MessageFrame *)messageFrame{
    
    _messageFrame = messageFrame;
    RYKChatMessage *message = _messageFrame.message;
    
    // 1、设置时间
    
    NSString *timeStr = [message.msgDate substringFromIndex:5];
    [_timeBtn setTitle:timeStr forState:UIControlStateNormal];
    [_timeBtn setBackgroundImage:[[UIImage imageNamed:@"chat_timeline_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 5, 10, 5)] forState:UIControlStateNormal];

    _timeBtn.frame = _messageFrame.timeF;
    
    // 2、设置头像
    if ([message.operType isEqualToString:@"0"]) {
        _nameLabel.text = @"我";
    } else {
        _nameLabel.text = message.showName;
    }
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.frame = _messageFrame.iconF;
    
    OHAttributedLabel *messageLabel = (OHAttributedLabel *)[_contentBtn viewWithTag:39];
    [messageLabel removeFromSuperview];
    UIImageView *img = (UIImageView *)[self.contentView viewWithTag:40];
    [img removeFromSuperview];
    // 3、设置内容
    if ([message.contentType isEqualToString:@"2"]) {
        if ([message.operType isEqualToString:@"0"]) {
            UIImageView *imgView = [[CTImageView alloc] initWithFrame:_messageFrame.imageF];
            [self.contentView addSubview:imgView];
            imgView.tag = 40;
            imgView.image = [UIImage imageWithContentsOfFile:message.localUrl];
        } else {
            CTImageView *ctImg = [[CTImageView alloc] initWithFrame:_messageFrame.imageF];
            ctImg.url = message.msgContent;
            NSString* imgCachePath = [IMAGE_CACHE_PATH stringByAppendingPathComponent:ctImg.specailName];
            if ([[NSFileManager defaultManager] fileExistsAtPath:imgCachePath]) {
                UIImage *cacheImage = [UIImage imageWithContentsOfFile:imgCachePath];
                UIImageView *imgView = [[CTImageView alloc] initWithFrame:_messageFrame.imageF];
                [self.contentView addSubview:imgView];
                imgView.tag = 40;
                imgView.image = cacheImage;
            } else {
                [self.contentView addSubview:ctImg];
                ctImg.tag = 40;
                ctImg.delegate = self;
                [ctImg loadImage];
            }
        }
        
        UIImageView *imgView = (UIImageView *)[self.contentView viewWithTag:40];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [imgView addGestureRecognizer:tap];
    } else {
//        _imageView.hidden = YES;
        OHAttributedLabel *messageLabel = [[OHAttributedLabel alloc]initWithFrame:CGRectZero];
        [_contentBtn addSubview:messageLabel];
        messageLabel.tag = 39;
        
        [messageFrame creatAttributedLabel:message.msgContent Label:messageLabel];
        [CustomMethod drawImage:messageLabel];
        if ([message.operType isEqualToString:@"0"]) {
            messageLabel.frame = CGRectMake(kContentRight,kContentTop,CGRectGetWidth(messageLabel.frame),CGRectGetHeight(messageLabel.frame));
        } else {
            messageLabel.frame = CGRectMake(kContentLeft,kContentTop,CGRectGetWidth(messageLabel.frame),CGRectGetHeight(messageLabel.frame));
        }
    }
    
    [_contentBtn setTitle:@"" forState:UIControlStateNormal];
    _contentBtn.contentEdgeInsets = UIEdgeInsetsMake(kContentTop, kContentLeft, kContentBottom, kContentRight);
    _contentBtn.frame = _messageFrame.contentF;
    
    if ([message.operType isEqualToString:@"0"]) {
        _contentBtn.contentEdgeInsets = UIEdgeInsetsMake(kContentTop, kContentRight, kContentBottom, kContentLeft);
    }
    
    UIImage *normal;
    if ([message.operType isEqualToString:@"0"]) {
    
        normal = [UIImage imageNamed:@"Chatto_bg_normal"];
        normal = [normal stretchableImageWithLeftCapWidth:normal.size.width * 0.5 topCapHeight:normal.size.height * 0.7];
    }else{
        
        normal = [UIImage imageNamed:@"Chatfrom_bg_normal"];
        normal = [normal stretchableImageWithLeftCapWidth:normal.size.width * 0.5 topCapHeight:normal.size.height * 0.7];
        
    }
    [_contentBtn setBackgroundImage:normal forState:UIControlStateNormal];
    [_contentBtn setBackgroundImage:normal forState:UIControlStateHighlighted];
    
    // 转圈圈
    _imageView.hidden = YES;
    [_imageView.layer removeAnimationForKey:@"transform.z"];
    _warnningImageView.hidden = YES;
    if ([message.operType isEqualToString:@"0"]) {
        if ([message.sendStatus isEqualToString:@"1"]) {
            // 发送完成
        } else {
            CGRect ivFrame = CGRectMake(_messageFrame.contentF.origin.x - _imageView.frame.size.width - 5, _messageFrame.contentF.origin.y + 12, _imageView.frame.size.width, _imageView.frame.size.height);
            
            if ([message.sendStatus isEqualToString:@"2"]) {
                _warnningImageView.hidden = NO;
                _warnningImageView.frame = ivFrame;
            } else {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *sendDate = [dateFormatter dateFromString:message.msgDate];
                
                NSDate *nowDate = [NSDate date];
                NSTimeInterval pastTime = [nowDate timeIntervalSinceDate:sendDate];
                if (pastTime > 90) {
                    // 发送超时失败
                    _warnningImageView.hidden = NO;
                    _warnningImageView.frame = ivFrame;
                } else {
                    // 发送中
                    _imageView.hidden = NO;
                    _imageView.frame = ivFrame;
                    
                    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                    animation.duration = 1.0;
                    animation.speed = 1.0;
                    animation.cumulative = NO;
                    animation.toValue = [NSNumber numberWithFloat:M_PI * 2];
                    animation.repeatCount = HUGE_VALF;
                    
                    [_imageView.layer addAnimation:animation forKey:@"transform.z"];
                }
            }
        }
    }
    
}

- (void)singleTap
{
    UIImageView *imgView = (UIImageView *)[self.contentView viewWithTag:40];
    if (imgView.image == nil) {
        return;
    }
    TGRImageViewController *controller = [[TGRImageViewController alloc] initWithImage:imgView.image];
    [[[UIApplication sharedApplication] delegate].window.rootViewController presentViewController:controller animated:YES completion:nil];
    return;
}

-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo{
    return YES;
}

- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state
{
//    NSLog(@"ctImageViewLoadDone() imageName=%@ state=%d", ctImageView.specailName, state);
    if (state != CTImageViewStateLoadFromUrl) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(messageCellLoadUrlImageOk:)]) {
        [self.delegate messageCellLoadUrlImageOk:self];
    }
    return;
}

@end
