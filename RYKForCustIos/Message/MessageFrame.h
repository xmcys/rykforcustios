//
//  MessageFrame.h
//  15-QQ聊天布局
//
//  Created by Liu Feng on 13-12-3.
//  Copyright (c) 2013年 Liu Feng. All rights reserved.
//

#define kMargin 5 //间隔
#define kContentW 180 //内容宽度

#define kTimeMarginW 15 //时间文本与边框间隔宽度方向
#define kTimeMarginH 10 //时间文本与边框间隔高度方向

#define kContentTop 10 //文本内容与按钮上边缘间隔
#define kContentLeft 25 //文本内容与按钮左边缘间隔
#define kContentBottom 15 //文本内容与按钮下边缘间隔
#define kContentRight 15 //文本内容与按钮右边缘间隔

#define kImageMaxWidth ([[UIScreen mainScreen] bounds].size.width / 2) // 图片显示最大宽度

#define kTimeFont [UIFont systemFontOfSize:12] //时间字体
#define kContentFont [UIFont systemFontOfSize:16] //内容字体

#import <Foundation/Foundation.h>
#import "CustomMethod.h"
#import "OHAttributedLabel.h"
#import "MarkUpParser.h"
#import "NSAttributedString+Attributes.h"

@class RYKChatMessage;

@interface MessageFrame : NSObject <OHAttributedLabelDelegate>

@property (nonatomic, assign, readonly) CGRect iconF;
@property (nonatomic, assign, readonly) CGRect timeF;
@property (nonatomic, assign, readonly) CGRect contentF;
@property (nonatomic, assign, readonly) CGRect imageF;

@property (nonatomic, assign, readonly) CGFloat cellHeight; //cell高度

@property (nonatomic, strong) RYKChatMessage *message;

@property (nonatomic, assign) BOOL showTime;

-(void)creatAttributedLabel:(NSString *)text Label:(OHAttributedLabel *)label;

@end
