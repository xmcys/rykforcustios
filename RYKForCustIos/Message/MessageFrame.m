//
//  MessageFrame.m
//  15-QQ聊天布局
//
//  Created by Liu Feng on 13-12-3.
//  Copyright (c) 2013年 Liu Feng. All rights reserved.
//

#define IMAGE_CACHE_PATH ([NSString stringWithFormat:@"%@/images", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]])


#import "MessageFrame.h"
#import "RYKChatMessage.h"
#import "CTImageView.h"

@implementation MessageFrame

- (void)setMessage:(RYKChatMessage *)message{
    
    _message = message;
    
    // 0、获取屏幕宽度
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    // 1、计算时间的位置
    if (_showTime){
        
        CGFloat timeY = kMargin;
//        CGSize timeSize = [_message.time sizeWithAttributes:@{UIFontDescriptorSizeAttribute: @"16"}];
        CGSize timeSize = [_message.msgDate sizeWithFont:kTimeFont];
//        NSLog(@"----%@", NSStringFromCGSize(timeSize));
        CGFloat timeX = (screenW - timeSize.width) / 2;
        _timeF = CGRectMake(timeX, timeY, timeSize.width + kTimeMarginW, timeSize.height + kTimeMarginH);
    }
    // 2、计算头像位置
    CGFloat iconX = kMargin;
    // 2.1 如果是自己发得，头像在右边
    CGSize nameSize = [_message.showName sizeWithFont:[UIFont systemFontOfSize:14]];
    if ([_message.operType isEqualToString:@"0"]) {
        iconX = screenW - kMargin - nameSize.width;
    }

    CGFloat iconY = CGRectGetMaxY(_timeF) + kMargin;
    _iconF = CGRectMake(iconX, iconY, nameSize.width, 40);
    
    // 3、计算内容位置
    CGFloat contentX = CGRectGetMaxX(_iconF) + kMargin;
    CGFloat contentY = iconY;
    CGSize contentSize;
    if ([_message.contentType isEqualToString:@"2"]) {
        if ([_message.operType isEqualToString:@"0"]) {
            // 发图片
            UIImage *image = [UIImage imageWithContentsOfFile:_message.localUrl];
            contentSize = image.size;
            if (image.size.width > kImageMaxWidth) {
                contentSize = CGSizeMake(kImageMaxWidth, image.size.height / image.size.width * kImageMaxWidth);
            }
        } else {
            // 收图片
            contentSize = CGSizeMake(100, 200);
            CTImageView *ctImg = [[CTImageView alloc] initWithFrame:CGRectZero];
            ctImg.url = _message.msgContent;
            NSString* imgCachePath = [IMAGE_CACHE_PATH stringByAppendingPathComponent:ctImg.specailName];
            if ([[NSFileManager defaultManager] fileExistsAtPath:imgCachePath]) {
                UIImage *cacheImage = [UIImage imageWithContentsOfFile:imgCachePath];
                contentSize = cacheImage.size;
                if (cacheImage.size.width > kImageMaxWidth) {
                    contentSize = CGSizeMake(kImageMaxWidth, cacheImage.size.height / cacheImage.size.width * kImageMaxWidth);
                }
            }
        }
    } else {
        OHAttributedLabel *_messageLabel = [[OHAttributedLabel alloc]initWithFrame:CGRectZero];
        [self creatAttributedLabel:message.msgContent Label:_messageLabel];
        [CustomMethod drawImage:_messageLabel];
        contentSize = _messageLabel.frame.size;
    }
    
    if ([_message.operType isEqualToString:@"0"]) {
        contentX = iconX - kMargin - contentSize.width - kContentLeft - kContentRight;
    }
    
    _contentF = CGRectMake(contentX, contentY, contentSize.width + kContentLeft + kContentRight, contentSize.height + kContentTop + kContentBottom);
    if ([_message.contentType isEqualToString:@"2"]) {
        if ([_message.operType isEqualToString:@"0"]) {
            _imageF = CGRectMake(contentX + kContentRight, contentY + kContentTop, contentSize.width, contentSize.height);
        } else {
            _imageF = CGRectMake(contentX + kContentLeft, contentY + kContentTop, contentSize.width, contentSize.height);
        }
        
    }

    // 4、计算高度
    _cellHeight = MAX(CGRectGetMaxY(_contentF), CGRectGetMaxY(_iconF))  + kMargin;
}

-(void)creatAttributedLabel:(NSString *)text Label:(OHAttributedLabel *)label
{
    [label setNeedsDisplay];
    
    NSMutableArray *httpArr = [CustomMethod addHttpArr:text];
    NSMutableArray *phoneNumArr = [CustomMethod addPhoneNumArr:text];
    NSMutableArray *emailArr = [CustomMethod addEmailArr:text];
    
    NSString *expressionPlistPath = [[NSBundle mainBundle]pathForResource:@"expression" ofType:@"plist"];
    NSDictionary *expressionDic   = [[NSDictionary alloc]initWithContentsOfFile:expressionPlistPath];
    
    NSString *o_text = [CustomMethod transformString:text emojiDic:expressionDic];
    o_text = [NSString stringWithFormat:@"<font color='black' strokeColor='gray' face='Palatino-Roman'>%@",o_text];
    
    MarkUpParser *wk_markupParser = [[MarkUpParser alloc] init];
    NSMutableAttributedString* attString = [wk_markupParser attrStringFromMarkUp:o_text];
    [attString setFont:[UIFont systemFontOfSize:16]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setAttString:attString withImages:wk_markupParser.images];
    
    NSString *string = attString.string;
    
    if ([emailArr count])
    {
        for (NSString *emailStr in emailArr)
        {
            [label addCustomLink:[NSURL URLWithString:emailStr] inRange:[string rangeOfString:emailStr]];
        }
    }
    
    if ([phoneNumArr count])
    {
        for (NSString *phoneNum in phoneNumArr)
        {
            [label addCustomLink:[NSURL URLWithString:phoneNum] inRange:[string rangeOfString:phoneNum]];
        }
    }
    if ([httpArr count])
    {
        for (NSString *httpStr in httpArr)
        {
            [label addCustomLink:[NSURL URLWithString:httpStr] inRange:[string rangeOfString:httpStr]];
        }
    }
    label.delegate = self;
    
    CGRect labelRect = label.frame;
    labelRect.size.width = [label sizeThatFits:CGSizeMake(200, CGFLOAT_MAX)].width;
    labelRect.size.height = [label sizeThatFits:CGSizeMake(200, CGFLOAT_MAX)].height;
    
    label.frame = labelRect;
    
    label.underlineLinks = NO;
    [label.layer display];
}

-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo{
    return YES;
}

@end
