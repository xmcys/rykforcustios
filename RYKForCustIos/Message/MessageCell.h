//
//  MessageCell.h
//  15-QQ聊天布局
//
//  Created by Liu Feng on 13-12-3.
//  Copyright (c) 2013年 Liu Feng. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MessageFrame;
@class MessageCell;

@protocol MessageCellDelegate <NSObject>

- (void)messageCellLoadUrlImageOk:(MessageCell *)msgCell;

@end

@interface MessageCell : UITableViewCell

@property (nonatomic, strong) MessageFrame *messageFrame;
@property (nonatomic, weak) id<MessageCellDelegate>delegate;

@end
