//
//  RYKGoodsSortViewController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#define URL_TAG_GOODS_SORT 1
#define URL_TAG_MOVE_UP 2
#define URL_TAG_MOVE_DOWN 3
#define URL_TAG_MOVE_TOP 4

#import "RYKGoodsSortViewController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKGoodsSortInfo.h"
#import "ConsSystemMsg.h"
#import "RYKPikcer.h"

@interface RYKGoodsSortCell ()

@end

@implementation RYKGoodsSortCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setGoodSortInfo:(RYKGoodsSortInfo *)goodSortInfo
{
    UILabel *labTitle = (UILabel *)[self viewWithTag:1];
    UIButton *btnMoveUp = (UIButton *)[self viewWithTag:2];
    UIButton *btnMoveDown = (UIButton *)[self viewWithTag:3];
    UIButton *btnMoveToTop = (UIButton *)[self viewWithTag:4];
    
    labTitle.text = goodSortInfo.brandName;
    [btnMoveUp addTarget:self action:@selector(btnMoveUpOnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnMoveDown addTarget:self action:@selector(btnMoveDownOnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnMoveToTop addTarget:self action:@selector(btnMoveToTopOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    return;
}

- (void)btnMoveUpOnClick
{
    [self.delegate goodsSortCellBtnMoveUpOnClick:self];
}

- (void)btnMoveDownOnClick
{
    [self.delegate goodsSortCellBtnMoveDownOnClick:self];
}

- (void)btnMoveToTopOnClick
{
    [self.delegate goodsSortCellBtnMoveToTopOnClick:self];
}

@end


@interface RYKGoodsSortViewController () <CTTableViewDelegate, UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate, GoodsSortCellDelegate, RYKPikcerDelegate>

@property (nonatomic, strong) RYKPikcer *datePicker;
@property (nonatomic, strong) CTTableView *tv;  // 未处理订单列表
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, assign) NSInteger curOperIndex;

- (void)loadGoodsSortData;

@end

@implementation RYKGoodsSortViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"商品排序";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_SPPX, CONS_MODULE_SPPX, nil]];
    
    self.indicator.backgroundTouchable = NO;
    self.array = [[NSMutableArray alloc] init];
    self.curOperIndex = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.datePicker == nil) {
        self.datePicker = [[RYKPikcer alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 1)];
        PickerItem *pickerItem1 = [[PickerItem alloc] init];
        pickerItem1.key = @"0";
        pickerItem1.value = @"非烟类";
        PickerItem *pickerItem2 = [[PickerItem alloc] init];
        pickerItem2.key = @"1";
        pickerItem2.value = @"卷烟类";
        NSMutableArray *tempArray = [NSMutableArray arrayWithObjects:pickerItem1, pickerItem2, nil];
        
        [self.datePicker setItemArray:tempArray];
        self.datePicker.delegate = self;
        [self.view addSubview:self.datePicker];
        [self.datePicker setSelectedIndex:0];
    }
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.datePicker.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.datePicker.frame.size.height)];
        
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.pullDownViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        
        [self loadGoodsSortData];
    }
    
}

- (void)loadGoodsSortData
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    PickerItem *curItem = [self.datePicker.itemArray objectAtIndex:self.datePicker.selectedIndex];
    NSString *isTabacco = curItem.key;
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getGoodsSortDisplay:isTabacco] delegate:self];
    conn.tag = URL_TAG_GOODS_SORT;
    [conn start];
    return;
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    RYKGoodsSortCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[RYKGoodsSortCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
        
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKTypeManageCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 5, self.tv.frame.size.width - 10, view.frame.size.height);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        
        [cell addSubview:view];
    }
    
    RYKGoodsSortInfo *goodsSortInfo = [self.array objectAtIndex:indexPath.row];
    [cell setGoodSortInfo:goodsSortInfo];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == URL_TAG_GOODS_SORT) {
        self.indicator.text = @"加载失败";
    } else {
        self.indicator.text = @"提交失败";
    }
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_GOODS_SORT) {
        RYKGoodsSortInfo *typeSort = [[RYKGoodsSortInfo alloc] init];
        [typeSort parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self.tv reloadData];
        }];
    } else if (connection.tag == URL_TAG_MOVE_UP) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            if (![curMsg.code isEqualToString:@"1"]) {
                self.indicator.text = @"提交失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self loadGoodsSortData];
        }];
    } else if (connection.tag == URL_TAG_MOVE_DOWN) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            if (![curMsg.code isEqualToString:@"1"]) {
                self.indicator.text = @"提交失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self loadGoodsSortData];
        }];
    } else if (connection.tag == URL_TAG_MOVE_TOP) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            if (![curMsg.code isEqualToString:@"1"]) {
                self.indicator.text = @"提交失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self loadGoodsSortData];
        }];
    }
}

#pragma mark -
#pragma mark RYKPikcer Delegate
- (void)rykPickerDelegate:(RYKPikcer *)picker didSelectRowAtIndex:(int)index
{
    [self loadGoodsSortData];
    return;
}

#pragma mark - RYKGoodsSortCell delegate
- (void)goodsSortCellBtnMoveUpOnClick:(RYKGoodsSortCell *)cell
{
    NSIndexPath *indexPath = [self.tv indexPathForCell:cell];
    if (indexPath.row == 0) {
        self.indicator.text = @"第一条只能下移";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力提交中..";
    [self.indicator show];
    
    self.curOperIndex = indexPath.row;
    PickerItem *curItem = [self.datePicker.itemArray objectAtIndex:self.datePicker.selectedIndex];
    NSString *isTabacco = curItem.key;
    RYKGoodsSortInfo *goodsSortInfo = [self.array objectAtIndex:indexPath.row];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService goodsMoveUpUrl] params:[RYKWebService goodsSortParams:goodsSortInfo.brandId serialNo:goodsSortInfo.serialNo isTabacco:isTabacco] delegate:self];
    conn.tag = URL_TAG_MOVE_UP;
    [conn start];
    return;
}

- (void)goodsSortCellBtnMoveDownOnClick:(RYKGoodsSortCell *)cell
{
    NSIndexPath *indexPath = [self.tv indexPathForCell:cell];
    if (indexPath.row == self.array.count - 1) {
        self.indicator.text = @"最后一条不能下移";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力提交中..";
    [self.indicator show];
    
    self.curOperIndex = indexPath.row;
    PickerItem *curItem = [self.datePicker.itemArray objectAtIndex:self.datePicker.selectedIndex];
    NSString *isTabacco = curItem.key;
    RYKGoodsSortInfo *goodsSortInfo = [self.array objectAtIndex:indexPath.row];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService goodsMoveDownUrl] params:[RYKWebService goodsSortParams:goodsSortInfo.brandId serialNo:goodsSortInfo.serialNo isTabacco:isTabacco] delegate:self];
    conn.tag = URL_TAG_MOVE_DOWN;
    [conn start];
    return;
}

- (void)goodsSortCellBtnMoveToTopOnClick:(RYKGoodsSortCell *)cell
{
    NSIndexPath *indexPath = [self.tv indexPathForCell:cell];
    if (indexPath.row == 0) {
        self.indicator.text = @"第一条只能下移";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力提交中..";
    [self.indicator show];
    
    self.curOperIndex = indexPath.row;
    PickerItem *curItem = [self.datePicker.itemArray objectAtIndex:self.datePicker.selectedIndex];
    NSString *isTabacco = curItem.key;
    RYKGoodsSortInfo *goodsSortInfo = [self.array objectAtIndex:indexPath.row];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService goodsMoveToTopUrl] params:[RYKWebService goodsSortParams:goodsSortInfo.brandId serialNo:goodsSortInfo.serialNo isTabacco:isTabacco] delegate:self];
    conn.tag = URL_TAG_MOVE_TOP;
    [conn start];
    return;
}

@end
