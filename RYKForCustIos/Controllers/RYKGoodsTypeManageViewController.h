//
//  RYKGoodsTypeManageViewController.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-11-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class RYKTypeManageCell;
@class RYKGoodsTypeSort;

@protocol TypeManageCellDelegate <NSObject>

- (void)typeManageCellBtnMoveUpOnClick:(RYKTypeManageCell *)cell;
- (void)typeManageCellBtnMoveDownOnClick:(RYKTypeManageCell *)cell;
- (void)typeManageCellBtnMoveToTopOnClick:(RYKTypeManageCell *)cell;

@end

@interface RYKTypeManageCell : UITableViewCell

@property (nonatomic, weak) id<TypeManageCellDelegate> delegate;
@property (nonatomic, strong) RYKGoodsTypeSort *typeInfo;

@end

@interface RYKGoodsTypeManageViewController : CTViewController

@end
