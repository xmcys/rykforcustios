//
//  RYKOrderDetailViewController.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"
#import "RYKCustOrderStatus.h"

@protocol RYKOrderDetailViewControllerDelegate <NSObject>

- (void)rykOrderDetailViewControllerConfirmOK;

@end

@interface RYKOrderDetailViewController : CTViewController

@property (nonatomic, weak) id<RYKOrderDetailViewControllerDelegate> delegate;
@property (nonatomic, strong) RYKCustOrderStatus *curOrder;

@end
