//
//  RYKAddGoodsViewController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKAddGoodsViewController.h"
#import "CTScrollView.h"
#import "CTEditorController.h"
#import "RYKWebService.h"
#import "CTDatePicker.h"
#import "RYKGoodsType.h"
#import "RYKGoodsUnit.h"
#import "ConsSystemMsg.h"
#import "CTImageView.h"
#import "ASIFormDataRequest.h"
#import "VPImageCropperViewController.h"
#import "ZbCommonFunc.h"
#import "CTEditorController.h"

#define ORIGINAL_MAX_WIDTH 640.0f
#define FLAG_IMG  200

#define LABEL_TYPE_BARCODE 11
#define LABEL_TYPE_NAME 12
#define LABEL_TYPE_GOODSTYPE 13
#define LABEL_TYPE_UNIT 14
#define LABEL_TYPE_PRICE 15
#define LABEL_TYPE_ONSALE_PRICE 16
#define LABEL_TYPE_ONSALE_BEGINDATE 17
#define LABEL_TYPE_ONSALE_ENDDATE 18

#define BTN_TYPE_NAME 21
#define BTN_TYPE_GOODSTYPE 22
#define BTN_TYPE_UNIT 23
#define BTN_TYPE_PRICE 24
#define BTN_TYPE_ONSALE_PRICE 25
#define BTN_TYPE_ONSALE_BEGINDATE 26
#define BTN_TYPE_ONSALE_ENDDATE 27

#define PROCESS_GET_UNITINFO 1
#define PROCESS_GET_TYPEINFO 2
#define PROCESS_ADD_NEWGOODS 3
#define PROCESS_MODIFY_GOODS 4
#define PROCESS_ONSALE_INFO 5
#define PROCESS_QUERY_GOODSINFO 6

@interface RYKAddGoodsViewController ()<CTEditorControllerDelegate, CTDatePickerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CTImageViewDelegate, ASIHTTPRequestDelegate,VPImageCropperDelegate,UITextViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;   //顶部Tab栏
@property (nonatomic, strong) IBOutlet UIButton *baseBtn;    //基础信息
@property (nonatomic, strong) IBOutlet UIButton *detailBtn;  //商品详情
@property (nonatomic, strong) IBOutlet CTScrollView *baseContainer;     // 基础信息主容器
@property (nonatomic, strong) UIScrollView *detailContainer; //商品详情主容器
@property (nonatomic, strong) IBOutlet UIView *cpView;     //产品介绍
@property (nonatomic, strong) IBOutlet UIView *cxView;     //促销说明
@property (nonatomic, strong) IBOutlet UITextView *cpText; //产品介绍文本
@property (nonatomic, strong) IBOutlet UITextView *cxText; //促销介绍文本
@property (nonatomic) BOOL ispushPhoto;
@property (nonatomic, strong) CTImageView *img;
@property (nonatomic, strong) CTImageView *img1;
@property (nonatomic, strong) CTImageView *img2;
@property (nonatomic, strong) CTImageView *img3;
@property (nonatomic, strong) CTImageView *img4;
@property (nonatomic, strong) NSMutableArray *imgValueArray;     //上传的图片数据数组 value
@property (nonatomic, strong) NSMutableArray *imgNameArray;      //上传图片名称数组
@property (nonatomic, strong) IBOutlet UIButton *btnIsDefaultUpload;
@property (nonatomic, strong) IBOutlet UIButton *btnIsOpenOnSale;
@property (nonatomic, strong) IBOutlet UIView *onSaleView;
@property (nonatomic, strong) RYKGoods *curGoods;
@property (nonatomic, strong) RYKGoodsType *goodsType;
@property (nonatomic, strong) NSMutableArray *unitNameArray;
@property (nonatomic) BOOL isPickImg;
@property (nonatomic, strong) NSString *uploadImgName;
@property (nonatomic, strong) UIScrollView *smScrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic) int curPageImg;

- (void)drawGoodsInfo;
- (void)drawGoodsOnSaleInfo;
- (void)drawScrollViewFrame;
- (void)drawCheckBoxImage;
- (IBAction)btnEditClicked:(id)sender;
- (IBAction)btnIsDefaultUploadClicked:(id)sender;
- (IBAction)btnIsOpenOnSaleClicked:(id)sender;
- (void)btnComitClicked:(UIButton *)sender;

- (void)queryGoodsInfo;
- (void)loadGoodsTypeData;
- (void)loadGoodsUnitData;
- (void)addNewGoods;
- (void)saveOnSaleInfo;
- (void)modifyGoods;
- (void)uploadImage;

- (IBAction)onTabChanged:(UIButton *)btn;

@end

@implementation RYKAddGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"编辑商品";
        self.curGoods = [[RYKGoods alloc] init];
        self.curGoods.isQuerySingleGoods = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TabBg"]];
//    [self.view addSubview:self.baseContainer];
    
    UIButton *btnComit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnComit setBackgroundImage:[UIImage imageNamed:@"提交按钮"] forState:UIControlStateNormal];
    btnComit.frame = CGRectMake(0, 0, 22, 22);
    [btnComit addTarget:self action:@selector(btnComitClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnComit];
    self.isPickImg = NO;
    self.ispushPhoto = NO;
    self.uploadImgName = @"";
    self.goodsType = [[RYKGoodsType alloc] init];
    self.unitNameArray = [[NSMutableArray alloc] init];
    self.imgValueArray = [[NSMutableArray alloc] init];
    self.imgNameArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self drawGoodsInfo];
    [self drawGoodsOnSaleInfo];
    [self drawScrollViewFrame];
    [self drawDetailInfo]; //商品详情
    [self drawCheckBoxImage];
    if (self.goodsType.nameArray.count <= 0) {
        [self loadGoodsTypeData];
    }
    
    if (self.curGoods.goodsId == nil || self.curGoods.goodsId.length == 0) {
        self.title = @"新增商品";
    }
    if (self.ispushPhoto == YES) {
        return;
    }
    
    [self onTabChanged:self.baseBtn];
    
    //注册通知,监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    //注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden:)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setGoodsInfo:(RYKGoods *)tagGoods
{
    [self.curGoods infoFromOtherGoods:tagGoods];
    return;
}

- (void)drawGoodsInfo
{
    UILabel *labBarCode = (UILabel *)[self.view viewWithTag:LABEL_TYPE_BARCODE];
    UILabel *labName = (UILabel *)[self.view viewWithTag:LABEL_TYPE_NAME];
    UILabel *labType = (UILabel *)[self.view viewWithTag:LABEL_TYPE_GOODSTYPE];
    UILabel *labUnit = (UILabel *)[self.view viewWithTag:LABEL_TYPE_UNIT];
    UILabel *labPrice = (UILabel *)[self.view viewWithTag:LABEL_TYPE_PRICE];
    
    // 无条码商品
    if ([self.curGoods.goodsCode isEqualToString:@""]) {
        labBarCode.text = @"系统将自动分配";
    } else {
        labBarCode.text = self.curGoods.goodsCode;
    }
    labName.text = self.curGoods.goodsName;
    labType.text = self.curGoods.goodsTypeName;
    labUnit.text = self.curGoods.goodsUnit;
    if (self.curGoods.goodsPrice.length == 0) {
        labPrice.text = [NSString stringWithFormat:@"%@", self.curGoods.goodsPrice];
    } else {
        labPrice.text = [NSString stringWithFormat:@"%@元", self.curGoods.goodsPrice];
    }
    
    return;
}

- (void)drawGoodsOnSaleInfo
{
    UILabel *labOnSalePrice = (UILabel *)[self.view viewWithTag:LABEL_TYPE_ONSALE_PRICE];
    UILabel *labBeginDate = (UILabel *)[self.view viewWithTag:LABEL_TYPE_ONSALE_BEGINDATE];
    UILabel *labEndDate = (UILabel *)[self.view viewWithTag:LABEL_TYPE_ONSALE_ENDDATE];
    
    labBeginDate.text = self.curGoods.onSaleBeginDate;
    labEndDate.text = self.curGoods.onSaleEndDate;
    if (self.curGoods.onSalePrice.length == 0) {
        labOnSalePrice.text = [NSString stringWithFormat:@"%@", self.curGoods.onSalePrice];
    } else {
        labOnSalePrice.text = [NSString stringWithFormat:@"%@元", self.curGoods.onSalePrice];
    }
    return;
}

- (void)drawScrollViewFrame
{
    self.onSaleView.hidden = !self.curGoods.isOnSale;
//    CGSize cSize;
//    if (self.curGoods.isOnSale) {
//        cSize = CGSizeMake(self.baseContainer.frame.size.width, self.onSaleView.frame.origin.y + self.onSaleView.frame.size.height);
//    } else {
//        cSize = CGSizeMake(self.baseContainer.frame.size.width, self.onSaleView.frame.origin.y);
//    }
//    
//    self.baseContainer.frame = CGRectMake(0, self.headerView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.headerView.frame.size.height);
//    if (cSize.height > self.view.bounds.size.height) {
//        self.baseContainer.contentSize = cSize;
//    } else {
//        self.baseContainer.contentSize = CGSizeMake(self.baseContainer.frame.size.width, self.view.bounds.size.height + 1);
//    }
}

- (void)drawCheckBoxImage
{
    if (self.curGoods.isDefaultUpload) {
        [self.btnIsDefaultUpload setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateNormal];
        [self.btnIsDefaultUpload setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateHighlighted];
    } else {
        [self.btnIsDefaultUpload setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateNormal];
        [self.btnIsDefaultUpload setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateHighlighted];
    }
    
    if (self.curGoods.isOnSale) {
        [self.btnIsOpenOnSale setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateNormal];
        [self.btnIsOpenOnSale setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateHighlighted];
    } else {
        [self.btnIsOpenOnSale setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateNormal];
        [self.btnIsOpenOnSale setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateHighlighted];
    }
    return;
}

- (void)drawDetailInfo
{
    if (self.detailContainer == nil) {
        self.detailContainer = [[UIScrollView alloc] init];
        self.detailContainer.frame = CGRectMake(0, self.headerView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.headerView.frame.size.height);
        self.detailContainer.contentSize = CGSizeMake(self.view.frame.size.width, 460);
        self.detailContainer.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.detailContainer];
        
        int pageCount = 5;
        
        self.smScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width , 115)];
        self.smScrollView.contentSize = CGSizeMake((self.view.frame.size.width) * pageCount , 115);
        self.smScrollView.showsHorizontalScrollIndicator = NO;
        self.smScrollView.showsVerticalScrollIndicator = NO;
        self.smScrollView.backgroundColor = [UIColor clearColor];
        self.smScrollView.pagingEnabled = YES;
        self.smScrollView.delegate = self;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0 * (self.view.frame.size.width ), 0, self.view.frame.size.width , 115)];
        self.img = [[CTImageView alloc] initWithFrame:CGRectMake(80, 0, self.view.frame.size.width - 80 * 2, 115)];
        self.img.image = [UIImage imageNamed:@"Addphoto"] ;
        self.img.contentMode = UIViewContentModeScaleToFill;
        self.img.userInteractionEnabled = YES;
        self.img.tag = 301;
        self.img.delegate = self;
        [view addSubview:self.img];
        [self.smScrollView addSubview:view];
        
        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(1 * (self.view.frame.size.width ), 0, self.view.frame.size.width , 115)];
        self.img1 = [[CTImageView alloc] initWithFrame:CGRectMake(80, 0, self.view.frame.size.width - 80 * 2, 115)];
        self.img1.image = [UIImage imageNamed:@"Addphoto"] ;
        self.img1.contentMode = UIViewContentModeScaleToFill;
        self.img1.userInteractionEnabled = YES;
        self.img1.tag = 302;
        self.img1.delegate = self;
        [view1 addSubview:self.img1];
        [self.smScrollView addSubview:view1];
        
        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(2 * (self.view.frame.size.width ), 0, self.view.frame.size.width , 115)];
        self.img2 = [[CTImageView alloc] initWithFrame:CGRectMake(80, 0, self.view.frame.size.width - 80 * 2, 115)];
        self.img2.image = [UIImage imageNamed:@"Addphoto"] ;
        self.img2.contentMode = UIViewContentModeScaleToFill;
        self.img2.userInteractionEnabled = YES;
        self.img2.tag = 303;
        self.img2.delegate = self;
        [view2 addSubview:self.img2];
        [self.smScrollView addSubview:view2];
        
        
        UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake(3 * (self.view.frame.size.width ), 0, self.view.frame.size.width , 115)];
        self.img3 = [[CTImageView alloc] initWithFrame:CGRectMake(80, 0, self.view.frame.size.width - 80 * 2, 115)];
        self.img3.image = [UIImage imageNamed:@"Addphoto"] ;
        self.img3.contentMode = UIViewContentModeScaleToFill;
        self.img3.userInteractionEnabled = YES;
        self.img3.tag = 304;
        self.img3.delegate = self;
        [view3 addSubview:self.img3];
        [self.smScrollView addSubview:view3];
        
        UIView *view4 = [[UIView alloc] initWithFrame:CGRectMake(4 * (self.view.frame.size.width ), 0, self.view.frame.size.width , 115)];
        self.img4 = [[CTImageView alloc] initWithFrame:CGRectMake(80, 0, self.view.frame.size.width - 80 * 2, 115)];
        self.img4.image = [UIImage imageNamed:@"Addphoto"] ;
        self.img4.contentMode = UIViewContentModeScaleToFill;
        self.img4.userInteractionEnabled = YES;
        self.img4.tag = 305;
        self.img4.delegate = self;
        [view4 addSubview:self.img4];
        [self.smScrollView addSubview:view4];

        
        self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 30)];
        self.pageControl.backgroundColor = [UIColor clearColor];
        self.pageControl.numberOfPages = pageCount;
        self.pageControl.currentPage = 0;
        self.pageControl.pageIndicatorTintColor = [UIColor blackColor];
        self.pageControl.currentPageIndicatorTintColor = XCOLOR(254, 161, 30, 1);
        [self.pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
        
        UIView *selectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 160)];
        
        [selectView addSubview:self.pageControl];
        [selectView addSubview:self.smScrollView];
        [self.detailContainer addSubview:selectView];
        
        self.cpView.frame = CGRectMake(5, selectView.frame.size.height + selectView.frame.origin.y, CGRectGetWidth(self.detailContainer.frame) - 10, self.cpView.frame.size.height);
        [self.detailContainer addSubview:self.cpView];
        self.cxView.frame = CGRectMake(5, self.cpView.frame.size.height + self.cpView.frame.origin.y + 10, CGRectGetWidth(self.detailContainer.frame) - 10, self.cxView.frame.size.height);
        [self.detailContainer addSubview:self.cxView];
        self.cpText.delegate = self;
        self.cxText.delegate = self;
        
        // 加载图片
        if (self.curGoods.picUrl01.length > 0) {
            self.img.url = self.curGoods.picUrl01;
            [self.img loadImage];
        }
        if (self.curGoods.picUrl02.length > 0) {
            self.img1.url = self.curGoods.picUrl02;
            [self.img1 loadImage];
        }
        if (self.curGoods.picUrl03.length > 0) {
            self.img2.url = self.curGoods.picUrl03;
            [self.img2 loadImage];
        }
        if (self.curGoods.picUrl04.length > 0) {
            self.img3.url = self.curGoods.picUrl04;
            [self.img3 loadImage];
        }
        if (self.curGoods.picUrl05.length > 0) {
            self.img4.url = self.curGoods.picUrl05;
            [self.img4 loadImage];
        }
        // 加载商品详情 中的促销介绍,产品介绍
        UILabel *lab = (UILabel *)[self.cpView viewWithTag:1];
        UILabel *lab2 = (UILabel *)[self.cxView viewWithTag:1];
        if (self.curGoods.brandDesc.length !=0) {
            self.cpText.text = self.curGoods.brandDesc;
            lab.hidden = YES;
        }
        if (self.curGoods.brandPromotion.length != 0) {
            self.cxText.text = self.curGoods.brandPromotion;
            lab2.hidden = YES;
        }
    }
}

- (void)changePage:(id)sender
{
    int page = self.pageControl.currentPage;
    CGRect frame = self.smScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.smScrollView scrollRectToVisible:frame animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.cpText resignFirstResponder];
    [self.cxText resignFirstResponder];
}
#pragma mark - KeyBoard
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1f];
    self.view.frame = CGRectMake(0, - 100, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

- (void)handleKeyboardDidHidden:(NSNotification*)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1f];
    self.view.frame = CGRectMake(0, + 64, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

#pragma mark - textView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    UILabel *lab = (UILabel *)[self.cpView viewWithTag:1];
    UILabel *lab2 = (UILabel *)[self.cxView viewWithTag:1];
    if (textView == self.cpText) {
        if (self.cpText.text.length == 0) {
            lab.hidden = NO;
        } else {
            lab.hidden = YES;
        }
    } else if (textView == self.cxText) {
        if (self.cxText.text.length == 0) {
            lab2.hidden = NO;
        } else {
            lab2.hidden = YES;
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [self.cpText resignFirstResponder];
        [self.cxText resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark -
#pragma mark all button event
- (IBAction)onTabChanged:(UIButton *)btn
{
    UIImage *img = [[UIImage imageNamed:@"on"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
    
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    [self.baseBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.baseBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    [self.detailBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.detailBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    
    [self.baseBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateNormal];
    [self.baseBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateHighlighted];
    [self.detailBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateNormal];
    [self.detailBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateHighlighted];
    
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    if (btn == self.baseBtn) {
        self.baseContainer.hidden = NO;
        self.detailContainer.hidden = YES;
        [self.view endEditing:YES];
    } else {
        self.baseContainer.hidden = YES;
        self.detailContainer.hidden = NO;
    }
}


- (IBAction)btnEditClicked:(id)sender
{
    UIControl *control = (UIControl *)sender;
    CTEditorController *controller = [[CTEditorController alloc] init];
    controller.delegate = self;
    if (control.tag == BTN_TYPE_NAME) {
        controller.title = @"商品名称";
        [controller setKeyboardType:UIKeyboardTypeDefault];
        [controller setEditorType:CTEditorTypeTextField];
        [controller setDefaultValue:self.curGoods.goodsName];
    } else if (control.tag == BTN_TYPE_GOODSTYPE) {
        controller.title = @"类别";
        [controller setEditorType:CTEditorTypePicker];
        [controller setKeys:[NSArray arrayWithArray:self.goodsType.codeArray] withValues:[NSArray arrayWithArray:self.goodsType.nameArray]];
        controller.enableSearch = YES;
    } else if (control.tag == BTN_TYPE_UNIT) {
        if (self.curGoods.goodsTypeId == nil || self.curGoods.goodsTypeId.length == 0) {
            self.indicator.text = @"请先选择类别";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
            return;
        }
        [self loadGoodsUnitData];
        return;
    } else if (control.tag == BTN_TYPE_PRICE) {
        controller.title = @"零售价";
        [controller setKeyboardType:UIKeyboardTypeDecimalPad];
        [controller setEditorType:CTEditorTypeTextField];
        [controller setDefaultValue:self.curGoods.goodsPrice];
    } else if (control.tag == BTN_TYPE_ONSALE_PRICE) {
        controller.title = @"促销价";
        [controller setKeyboardType:UIKeyboardTypeDecimalPad];
        [controller setEditorType:CTEditorTypeTextField];
        [controller setDefaultValue:self.curGoods.onSalePrice];
    } else if (control.tag == BTN_TYPE_ONSALE_BEGINDATE) {
        CTDatePicker *picker = [[CTDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
        picker.delegate = self;
        picker.tag = control.tag;
        if (self.curGoods.onSaleBeginDate.length != 0) {
            [picker setCurrentDate:self.curGoods.onSaleBeginDate];
        }
        [picker show];
        return;
    } else if (control.tag == BTN_TYPE_ONSALE_ENDDATE) {
        CTDatePicker *picker = [[CTDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
        picker.delegate = self;
        picker.tag = control.tag;
        if (self.curGoods.onSaleEndDate.length != 0) {
            [picker setCurrentDate:self.curGoods.onSaleEndDate];
        }
        [picker show];
        return;
    }
    [self.navigationController pushViewController:controller animated:YES];
    controller.view.tag = control.tag;
}

- (void)btnIsDefaultUploadClicked:(id)sender
{
    self.curGoods.isDefaultUpload = !self.curGoods.isDefaultUpload;
    [self drawCheckBoxImage];
}

- (IBAction)btnIsOpenOnSaleClicked:(id)sender
{
    self.curGoods.isOnSale = !self.curGoods.isOnSale;
    [self drawCheckBoxImage];
    [self drawScrollViewFrame];
    
    if (self.curGoods.isOnSale) {
        [self drawGoodsOnSaleInfo];
    }
}

- (void)btnComitClicked:(UIButton *)sender
{
    if (self.curGoods.goodsName == nil || self.curGoods.goodsName.length == 0) {
        self.indicator.text = @"请输入商品名称";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    if (self.curGoods.goodsTypeName == nil || self.curGoods.goodsTypeName.length == 0) {
        self.indicator.text = @"请选择类别";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    if (self.curGoods.goodsUnit == nil || self.curGoods.goodsUnit.length == 0) {
        self.indicator.text = @"请输入规格";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    if (self.curGoods.goodsPrice == nil || self.curGoods.goodsPrice.length == 0) {
        self.indicator.text = @"请输入零售价";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    if (self.curGoods.isOnSale) {
        if (self.curGoods.onSalePrice == nil || self.curGoods.onSalePrice.length == 0) {
            self.indicator.text = @"请输入促销价";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
            return;
        }
        if (self.curGoods.onSaleBeginDate == nil || self.curGoods.onSaleBeginDate.length == 0) {
            self.indicator.text = @"请选择促销开始时间";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
            return;
        }
        if (self.curGoods.onSaleEndDate == nil || self.curGoods.onSaleEndDate.length == 0) {
            self.indicator.text = @"请选择促销结束时间";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
            return;
        }
    }
    
    
    //上传图片数据
    if (self.isPickImg) {
        [self uploadImage];
        return;
    }
    
    //调接口插入数据
    if (self.curGoods.goodsId == nil || self.curGoods.goodsId.length <= 0) {
        [self addNewGoods];
    } else {
        [self modifyGoods];
    }
}

#pragma mark -
#pragma mark all url operation
- (void)queryGoodsInfo
{
    // 添加无条码商品，不查询
    if ([self.curGoods.goodsCode isEqualToString:@""]) {
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService queryGoodsInfoUrl:self.curGoods.goodsCode] delegate:self];
    conn.tag = PROCESS_QUERY_GOODSINFO;
    [conn start];
    return;
}

- (void)loadGoodsTypeData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getGoodsTypeUrl] delegate:self];
    conn.tag = PROCESS_GET_TYPEINFO;
    [conn start];
    return;
}
- (void)loadGoodsUnitData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getGoodsUnitUrl:self.curGoods.goodsTypeId] delegate:self];
    conn.tag = PROCESS_GET_UNITINFO;
    [conn start];
    return;
}

- (void)addNewGoods
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    if ([self.curGoods.goodsTypeName isEqualToString:@"卷烟"]) {
        self.curGoods.isTabacco = @"1";
    } else {
        self.curGoods.isTabacco = @"0";
    }
    
    
    //拼接字符串
    NSMutableString *str = [[NSMutableString alloc] init];
    for (int i = 0; i < self.imgValueArray.count; i ++) {
        UIImage *xImage = [self.imgValueArray objectAtIndex:i];
        if ([xImage isEqual:self.img.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl01>%@</picUrl01>",[self.imgNameArray objectAtIndex:i]]];
        }  else if ([xImage isEqual:self.img1.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl02>%@</picUrl02>",[self.imgNameArray objectAtIndex:i]]];
        } else if ([xImage isEqual:self.img2.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl03>%@</picUrl03>",[self.imgNameArray objectAtIndex:i]]];
        } else if ([xImage isEqual:self.img3.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl04>%@</picUrl04>",[self.imgNameArray objectAtIndex:i]]];
        } else if ([xImage isEqual:self.img4.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl05>%@</picUrl05>",[self.imgNameArray objectAtIndex:i]]];
        }
    }
    [str appendString:@"</Test>"];
    
    NSString * partparms = [NSString stringWithFormat:@"<Test><userId>%@</userId><orgCode>%@</orgCode><brandType>%@</brandType><brandName>%@</brandName><pyIndex>%@</pyIndex><userCode>%@</userCode><unit>%@</unit><retailPrice>%@</retailPrice><barCode>%@</barCode><isTabacco>%@</isTabacco><brandDesc>%@</brandDesc><brandPromotion>%@</brandPromotion>", [RYKWebService sharedUser].userId, @"13500100", self.curGoods.goodsTypeId, self.curGoods.goodsName, @"", [RYKWebService sharedUser].userCode, self.curGoods.goodsUnit, self.curGoods.goodsPrice, self.curGoods.goodsCode,self.curGoods.isTabacco,self.cpText.text,self.cxText.text];
    NSString * params = [NSString stringWithFormat:@"%@%@",partparms,str];
    
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService addNewGoodsUrl] params:params delegate:self];
    conn.tag = PROCESS_ADD_NEWGOODS;
    [conn start];
    return;
}

- (void)saveOnSaleInfo
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><brandId>%@</brandId><unit>%@</unit><finalPrice>%@</finalPrice><startDate>%@</startDate><endDate>%@</endDate></Test>", [RYKWebService sharedUser].userId, self.curGoods.goodsId, self.curGoods.goodsUnit, self.curGoods.onSalePrice, self.curGoods.onSaleBeginDate, self.curGoods.onSaleEndDate];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService saveGoodsOnSaleInfoUrl] params:params delegate:self];
    conn.tag = PROCESS_ONSALE_INFO;
    [conn start];
    return;
}

- (void)modifyGoods
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    NSString *stateName = @"0";
    if (self.curGoods.isOnSale) {
        stateName = @"1";
    }
    
    //拼接字符串
    NSMutableString *str = [[NSMutableString alloc] init];
    for (int i = 0; i < self.imgValueArray.count; i ++) {
        UIImage *xImage = [self.imgValueArray objectAtIndex:i];
        if ([xImage isEqual:self.img.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl01>%@</picUrl01>",[self.imgNameArray objectAtIndex:i]]];
        }  else if ([xImage isEqual:self.img1.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl02>%@</picUrl02>",[self.imgNameArray objectAtIndex:i]]];
        } else if ([xImage isEqual:self.img2.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl03>%@</picUrl03>",[self.imgNameArray objectAtIndex:i]]];
        } else if ([xImage isEqual:self.img3.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl04>%@</picUrl04>",[self.imgNameArray objectAtIndex:i]]];
        } else if ([xImage isEqual:self.img4.image]) {
            [str appendString:[NSString stringWithFormat:@"<picUrl05>%@</picUrl05>",[self.imgNameArray objectAtIndex:i]]];
        }
    }
    [str appendString:@"</Test>"];
    
    NSString * partparms = [NSString stringWithFormat:@"<Test><userId>%@</userId><brandId>%@</brandId><brandName>%@</brandName><brandType>%@</brandType><unit>%@</unit><barCode>%@</barCode><finalPrice>%@</finalPrice><retailPrice>%@</retailPrice><startDate>%@</startDate><endDate>%@</endDate><judgeUsedFlag>%@</judgeUsedFlag><brandDesc>%@</brandDesc><brandPromotion>%@</brandPromotion>", [RYKWebService sharedUser].userId, self.curGoods.goodsId,self.curGoods.goodsName, self.curGoods.goodsTypeId, self.curGoods.goodsUnit,self.curGoods.goodsCode,self.curGoods.onSalePrice, self.curGoods.goodsPrice,self.curGoods.onSaleBeginDate,self.curGoods.onSaleEndDate,stateName,self.cpText.text,self.cxText.text];
    NSString * params = [NSString stringWithFormat:@"%@%@",partparms,str];
    NSLog(@"%@",params);

    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService modifyGoodsUrl] params:params delegate:self];
    conn.tag = PROCESS_MODIFY_GOODS;
    [conn start];
    return;
}

- (void)uploadImage
{
    self.indicator.text = @"正在上传图片..";
    [self.indicator show];
    
    NSString * path = [RYKWebService postImageUrl];
    NSURL* url = [NSURL URLWithString:path];
    ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:url];
    request.showAccurateProgress = YES;
    request.shouldContinueWhenAppEntersBackground = YES;
    request.delegate = self;
    [request setRequestMethod:@"POST"];
    [request setPostFormat:ASIMultipartFormDataPostFormat];
    
    NSDate *nowDate = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[nowDate timeIntervalSince1970]];
    //遍历全部图片，数据加在一起请求
    for (int i = 0; i < self.imgValueArray.count; i ++) {
        
        NSString *imgName = [NSString stringWithFormat:@"iOS%@%d.jpg", timeSp,i];
        self.uploadImgName = [[@"uniService/Resource/custMobile/" stringByAppendingPathComponent:[RYKWebService sharedUser].userId] stringByAppendingPathComponent:imgName];
        
        [self.imgNameArray addObject:self.uploadImgName];
        
        NSData* imgData = UIImageJPEGRepresentation([self.imgValueArray objectAtIndex:i], 0.85);
        [request addData:imgData withFileName:imgName andContentType:@"image/jpeg" forKey:@"pic[]"];
        
    }
    
    [request start];
    return;
}

- (void)isExist:(id)img inArray:(NSMutableArray *)array
{
    if ([array indexOfObject:img]) {
        [array removeObject:img];
        [array addObject:img];
    } else {
        [array addObject:img];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

#pragma mark -
#pragma mark CTEditorController Delegate
- (void)ctEditorController:(CTEditorController *)controller didPickedAtIndex:(NSInteger)index withKey:(NSString *)selectedKey andValue:(NSArray *)value
{
    [controller.navigationController popViewControllerAnimated:YES];
    if (controller.view.tag == BTN_TYPE_GOODSTYPE) {
        self.curGoods.goodsTypeName = (NSString *)value;
        self.curGoods.goodsTypeId = selectedKey;
    } else if (controller.view.tag == BTN_TYPE_UNIT) {
        self.curGoods.goodsUnit = selectedKey;
    }
    return;
}

- (void)ctEditorController:(CTEditorController *)controller editDoneWithText:(NSString *)text
{
    NSInteger tag = controller.view.tag;
    [controller.navigationController popViewControllerAnimated:YES];
    if (text.length == 0) {
        return;
    }
    
    if (tag == BTN_TYPE_NAME) {
        self.curGoods.goodsName = text;
    } else if (tag == BTN_TYPE_UNIT) {
        self.curGoods.goodsUnit = text;
    } else if (tag == BTN_TYPE_PRICE) {
        self.curGoods.goodsPrice = text;
    } else if (tag == BTN_TYPE_ONSALE_PRICE) {
        self.curGoods.onSalePrice = text;
        [self drawGoodsOnSaleInfo];
        return;
    }
    return;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    if (connection.tag == PROCESS_GET_UNITINFO) {
        self.indicator.text = @"加载规格失败";
    }
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_TYPEINFO) {
        [self.indicator hide];
        self.indicator.showing = NO;
        [self.goodsType parseData:data complete:^(NSArray *array){
            if (self.curGoods.goodsId == nil || self.curGoods.goodsId.length <= 0) {
                [self queryGoodsInfo];
            }
        }];
    } else if (connection.tag == PROCESS_QUERY_GOODSINFO) {
        [self.indicator hide];
        [self.curGoods parseData:data complete:^(NSArray *array) {
            [self drawGoodsInfo];
            return;
        }];
    } else if (connection.tag == PROCESS_GET_UNITINFO) {
        [self.indicator hide];
        RYKGoodsUnit *goodsUnit = [[RYKGoodsUnit alloc] init];
        [goodsUnit parseData:data complete:^(NSArray *array){
            [self.unitNameArray removeAllObjects];
            [self.unitNameArray addObjectsFromArray:array];
            
            CTEditorController *controller = [[CTEditorController alloc] init];
            controller.delegate = self;
            controller.title = @"规格";
            [controller setEditorType:CTEditorTypePicker];
            [controller setKeys:[NSArray arrayWithArray:self.unitNameArray] withValues:[NSArray arrayWithArray:self.unitNameArray]];
            controller.enableSearch = YES;
            [self.navigationController pushViewController:controller animated:YES];
            controller.view.tag = BTN_TYPE_UNIT;
        }];
    } else if (connection.tag == PROCESS_ADD_NEWGOODS) {
        [self.indicator hide];
        self.indicator.showing = NO;
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg){
            if (![curMsg.code isEqualToString:@"1"]) {
                // 新增商品失败
                self.indicator.text = curMsg.msg;
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            if (!self.curGoods.isOnSale) {
                // 新增商品完成
                self.indicator.text = @"新增商品成功";
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1 complete:^(void){
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                return;
            } else {
                self.curGoods.goodsId = curMsg.msg;
                // 新增商品完成，继续上传促销信息
                [self saveOnSaleInfo];
            }
        }];
    } else if (connection.tag == PROCESS_ONSALE_INFO) {
        [self.indicator hide];
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg){
            if (![curMsg.code isEqualToString:@"1"]) {
                // 新增促销信息失败
                self.indicator.text = curMsg.msg;
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            self.indicator.text = @"新增商品成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1 complete:^(void){
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    } else if (connection.tag == PROCESS_MODIFY_GOODS) {
        [self.indicator hide];
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg){
            if (![curMsg.code isEqualToString:@"1"]) {
                // 修改商品信息失败
                self.indicator.text = curMsg.msg;
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            self.indicator.text = @"修改商品信息成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1 complete:^(void){
                if (self.delegate && [self.delegate respondsToSelector:@selector(rykAddGoodsViewControllerModifyOK:)]) {
                    [self.delegate rykAddGoodsViewControllerModifyOK:self.curGoods];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    }
    return;
}

#pragma mark -
#pragma mark CTDatePicker Delegate
- (void)ctDatePicker:(CTDatePicker *)datePicker didSelectDate:(NSString *)date
{
    if (datePicker.tag == BTN_TYPE_ONSALE_BEGINDATE) {
        self.curGoods.onSaleBeginDate = date;
    } else if (datePicker.tag == BTN_TYPE_ONSALE_ENDDATE) {
        self.curGoods.onSaleEndDate = date;
    }
    [self drawGoodsOnSaleInfo];
    return;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.ispushPhoto = YES;
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    } else {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        // 重新生成原图，去除图片的横、竖等属性
        UIGraphicsBeginImageContext(portraitImg.size);
        [portraitImg drawInRect:CGRectMake(0, 0, portraitImg.size.width, portraitImg.size.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // 裁剪
        VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:reSizeImage cropFrame:CGRectMake(0, (self.view.frame.size.height - self.view.frame.size.width / 1.3) / 2, self.view.frame.size.width, self.view.frame.size.width / 1.3) limitScaleRatio:3.0];
        imgEditorVC.delegate = self;
        [self presentViewController:imgEditorVC animated:YES completion:nil];
    }];
}

#pragma mark - VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    // 缩小图片
    UIGraphicsBeginImageContext(CGSizeMake(390, 300));
    [editedImage drawInRect:CGRectMake(0, 0, 390, 300)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.isPickImg = YES;

    
    self.img.url = nil;
    self.img1.url = nil;
    self.img2.url = nil;
    self.img3.url = nil;
    self.img4.url = nil;
    
    switch (self.curPageImg) {
        case 301:
            self.img.image = reSizeImage;
            [self isExist:self.img.image inArray:self.imgValueArray];
            break;
            
        case 302:
            self.img1.image = reSizeImage;
            [self isExist:self.img1.image inArray:self.imgValueArray];
            break;
            
        case 303:
            self.img2.image = reSizeImage;
            [self isExist:self.img2.image inArray:self.imgValueArray];
            break;
            
        case 304:
            self.img3.image = reSizeImage;
            [self isExist:self.img3.image inArray:self.imgValueArray];
            break;
            
        case 305:
            self.img4.image = reSizeImage;
            [self isExist:self.img4.image inArray:self.imgValueArray];
            break;
            
        default:
            break;
    }
    return;
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController
{
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    return;
}

#pragma mark - ctImageView delegate
- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state
{
    return;
}

- (void)ctImageViewDidClicked:(CTImageView *)ctImageView
{
    self.curPageImg = ctImageView.tag;
    UIActionSheet *actSheet;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从手机相册选择", @"拍照", nil];
    } else {
        actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从手机相册选择", nil];
    }
    [actSheet showInView:self.view];
    return;
}

#pragma mark -
#pragma mark ASIHttpRequest Delegate
- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"%@", [request error]);
    self.indicator.text = @"上传失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data
{
    ConsSystemMsg * sysMsg = [[ConsSystemMsg alloc] init];
    [sysMsg parseData:data];
    if ([sysMsg.code isEqualToString:@"1"]) {
        [self.indicator hide];
        self.indicator.showing = NO;
        self.curGoods.picUrl01 = [RYKWebService itemImageFullUrl:self.uploadImgName];
        

        if (self.curGoods.goodsId == nil || self.curGoods.goodsId.length <= 0) {
            [self addNewGoods];
        } else {
            [self modifyGoods];
        }
    } else {
        self.indicator.text = sysMsg.msg;
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    }
}

@end
