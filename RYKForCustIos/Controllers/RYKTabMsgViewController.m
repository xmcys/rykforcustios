//
//  RYKTabMsgViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-8-5.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKTabMsgViewController.h"
#import "CTTableView.h"
#import "RYKChatMessage.h"
#import "RYKSqlLite.h"
#import "RYKChatToTargetController.h"
#import "RYKXmppManager.h"
#import "RYKWebService.h"
#import "RYKCustomer.h"

@interface RYKTabMsgViewController () <CTTableViewDelegate, UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate, rykXmppManagerRecvMsgDelegate>

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSMutableArray *unreadCntArray;

- (void)loadChatTargetListData;

@end

@implementation RYKTabMsgViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"消息中心" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:116.0/255.0 alpha:1] image:[UIImage imageNamed:@"Tabmsg"] selectedImage:[UIImage imageNamed:@"TabmsgHighlighted"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_XXZX, CONS_MODULE_XXZX, nil]];
    
    self.array = [[NSMutableArray alloc] init];
    self.unreadCntArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"消息中心";
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    [RYKXmppManager sharedManager].recvMsgDelegate = self;
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.pullDownViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        
        [self loadCustomerArrayData];
        return;
    } else {
        [self loadChatTargetListData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadChatTargetListData
{
    [self.array removeAllObjects];
    [self.unreadCntArray removeAllObjects];
    self.array = [NSMutableArray arrayWithArray:[[RYKSqlLite shareManager] getChatTargetList]];
    for (RYKChatMessage *chatMsg in self.array) {
        NSInteger unreadCnt = [[RYKSqlLite shareManager] getUnReadCntByTarget:chatMsg.msgTarget];
        [self.unreadCntArray addObject:[NSNumber numberWithInteger:unreadCnt]];
    }
    
    [self.tv reloadData];
    if (self.array.count == 0) {
        self.indicator.text = @"努力加载中...";
        [self.indicator autoHide:CTIndicateStateLoading afterDelay:1.0 complete:^(void) {
            self.indicator.text = @"暂无消息";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        }];
    }
    return;
}

- (void)loadCustomerArrayData
{
    if ([RYKWebService sharedUser].customerDict != nil) {
        return;
    }
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在加载顾客资料..";
    [self.indicator show];
    
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><keyWord></keyWord></Test>", [RYKWebService sharedUser].userId];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getCustInfo] params:params delegate:self];
    [conn start];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKChatTargetCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGRectGetHeight(view.frame));
        
        [cell addSubview:view];
    }
    
    UILabel *labTargetName = (UILabel *)[cell viewWithTag:1];
    UILabel *labDate = (UILabel *)[cell viewWithTag:2];
    UILabel *labContent = (UILabel *)[cell viewWithTag:3];
    UIImageView *ivUnRead = (UIImageView *)[cell viewWithTag:4];
    
    RYKChatMessage *curChatMsg = [self.array objectAtIndex:indexPath.row];
    labTargetName.text = curChatMsg.showName;
    NSString *timeStr = [curChatMsg.msgDate substringFromIndex:5];
    labDate.text = timeStr;
    NSString *showContent = curChatMsg.msgContent;
    if ([curChatMsg.contentType isEqualToString:@"2"]) {
        showContent = @"[图片]";
    }
    labContent.text = showContent;
    
    NSInteger unreadCnt = [self.unreadCntArray[indexPath.row] intValue];
    if (unreadCnt > 0) {
        ivUnRead.hidden = NO;
        CGSize size = [labTargetName.text sizeWithFont:labTargetName.font constrainedToSize:labTargetName.frame.size];
        CGRect ivUnReadFrame = ivUnRead.frame;
        ivUnReadFrame.origin.x = labTargetName.frame.origin.x + size.width + 2;
        ivUnRead.frame = ivUnReadFrame;
    } else {
        ivUnRead.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RYKChatMessage *curChatMsg = [self.array objectAtIndex:indexPath.row];
    RYKChatToTargetController *controller = [[RYKChatToTargetController alloc] initWithNibName:@"RYKChatToTargetController" bundle:nil];
    controller.msgTarget = curChatMsg.msgTarget;
    controller.showName = curChatMsg.showName;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - rykXmppManager DidRecvMsg
- (void)rykXmppManagerDidRecvMsg:(RYKChatMessage *)chatMsg
{
    [self loadChatTargetListData];
    return;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"顾客资料加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    RYKCustomer *info = [[RYKCustomer alloc] init];
    [info parseData:data complete:^(NSArray *array){
        [[RYKWebService sharedUser] updateCustomerDict:array];
        [self loadChatTargetListData];
//        [self performSelector:@selector(loadChatTargetListData) withObject:self afterDelay:0.1];
    }];
}

@end
