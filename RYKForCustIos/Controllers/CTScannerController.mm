//
//  ConsScannerController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTScannerController.h"
#import "CTUtil.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ZXingObjC.h"

@interface CTScannerController () <ZXCaptureDelegate>

@property (nonatomic, strong) UIImageView * scanFrame;
@property (nonatomic, strong) UIImageView * scanLine;
@property (nonatomic, strong) NSTimer * animTimer;
@property (nonatomic,strong) ZXCapture *capture;
@property (nonatomic, assign) BOOL isInit;

- (void)scanLineAnimation;
- (void)addCover:(CGRect )rect;
- (void)initCapture;

@end

@implementation CTScannerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"扫一扫";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isInit = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    BOOL available = YES;
    
    
    
    if (!VERSION_LESS_THAN_IOS7) {
        NSString * mediaType = AVMediaTypeVideo; // Or AVMediaTypeAudio
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
        
        //        if (authStatus == AVAuthorizationStatusRestricted){
        //            NSLog(@"AVAuthorizationStatusRestricted");
        //        }
        
        if (authStatus == AVAuthorizationStatusDenied) {
            available = NO;
        }
        
    }
    
    if (!available) {
        [self.indicator hide];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"扫描提示" message:@"您需要在 设置 - 隐私 - 相机 中打开应用的相机访问权限" delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (!self.isInit) {
        self.isInit = YES;
        [self initCapture];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.capture stop];
    self.capture = nil;
    [self.animTimer invalidate];
    self.animTimer = nil;
}

- (void)initCapture
{
    CGRect scanRect = CGRectMake((self.view.bounds.size.width - 230) / 2, 75, 230, 230);
    
    self.capture = [[ZXCapture alloc] init];
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    self.capture.rotation = 90.0f;
    self.capture.layer.frame = self.view.bounds;
    [self.view.layer addSublayer:self.capture.layer];
    self.capture.delegate = self;
    self.capture.layer.frame = self.view.bounds;
    self.capture.scanRect = scanRect;
    
    [self addCover:CGRectMake(0, 0, self.view.bounds.size.width, scanRect.origin.y)];
    [self addCover:CGRectMake(0, scanRect.origin.y, scanRect.origin.x, scanRect.size.height)];
    [self addCover:CGRectMake(scanRect.origin.x + scanRect.size.width, scanRect.origin.y, scanRect.origin.x, scanRect.size.height)];
    [self addCover:CGRectMake(0, scanRect.origin.y + scanRect.size.height, self.view.bounds.size.width, self.view.bounds.size.height - (scanRect.origin.y + scanRect.size.height))];
    
    UILabel * tip = [[UILabel alloc] initWithFrame:CGRectMake((self.view.bounds.size.width - 140) / 2, 20, 140, 30)];
    tip.textColor = [UIColor blackColor];
    tip.font = [UIFont boldSystemFontOfSize:15.0f];
    tip.text = @"条码 · 二维码";
    tip.textAlignment = NSTextAlignmentCenter;
    tip.backgroundColor = self.view.backgroundColor;
    tip.layer.cornerRadius = 15.0f;
    [self.view addSubview:tip];
    
    self.scanFrame = [[UIImageView alloc] initWithFrame:scanRect];
    self.scanFrame.contentMode = UIViewContentModeScaleAspectFill;
    self.scanFrame.image = [UIImage imageNamed:@"ScanFrame"];
    [self.view addSubview:self.scanFrame];
    
    self.scanLine = [[UIImageView alloc] initWithFrame:CGRectMake(self.scanFrame.frame.origin.x, self.scanFrame.frame.origin.y + 10, self.scanFrame.frame.size.width, 10)];
    self.scanLine.contentMode = UIViewContentModeScaleAspectFit;
    self.scanLine.image = [UIImage imageNamed:@"ScanLine"];
    self.scanLine.tag = 1;
    [self.view addSubview:self.scanLine];
    
    self.animTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(scanLineAnimation) userInfo:nil repeats:YES];
    
    
}

- (void)addCover:(CGRect)rect
{
    UIView * view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    [self.view addSubview:view];
}

- (void)scanLineAnimation
{
    if (self.scanLine.tag == 1) {
        self.scanLine.center = CGPointMake(self.scanLine.center.x, self.scanLine.center.y + 2);
        if (self.scanLine.frame.origin.y + self.scanLine.frame.size.height >= self.scanFrame.frame.origin.y + self.scanFrame.frame.size.height - 10) {
            self.scanLine.tag = 2;
        }
    } else {
        self.scanLine.center = CGPointMake(self.scanLine.center.x, self.scanLine.center.y - 2);
        if (self.scanLine.frame.origin.y <= self.scanFrame.frame.origin.y + 10) {
            self.scanLine.tag = 1;
        }
    }
}

#pragma mark - ZXCaptureDelegate
- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
    if (!result){
        return;
    }
    
    NSLog(@"%@",[NSString stringWithFormat:@"条码内容:%@", result.text]);
    if (self.delegate && [self.delegate respondsToSelector:@selector(ctScannerController:didReceivedScanResult:)]) {
        [self.capture stop];
        [self.delegate ctScannerController:self didReceivedScanResult:result.text];
    }
}


@end
