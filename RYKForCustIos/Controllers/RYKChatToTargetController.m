//
//  RYKChatToTargetController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-9.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKChatToTargetController.h"
#import "RYKChatMessage.h"
#import "RYKSqlLite.h"
#import "MessageFrame.h"
#import "MessageCell.h"
#import "RYKXmppManager.h"
#import "ASIFormDataRequest.h"
#import "ConsSystemMsg.h"
#import "MyTableView.h"
#import "ZBMessageManagerFaceView.h"

#define ROWS_PER_TIME 20

typedef NS_ENUM(NSInteger,ZBMessageViewState) {
    ZBMessageViewStateShowFace,
    ZBMessageViewStateShowNone,
};

@interface RYKChatToTargetController () <CTTableViewDelegate, UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate, rykXmppManagerRecvMsgDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ASIHTTPRequestDelegate, UIGestureRecognizerDelegate, ZBMessageManagerFaceViewDelegate, MessageCellDelegate, UITextViewDelegate>
{
    double animationDuration;
    CGRect keyboardRect;
}


@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) IBOutlet UIView *bottomView;
@property (nonatomic, strong) IBOutlet UITextView *tvMessage;
@property (nonatomic, strong) IBOutlet UIButton *btnSend;
@property (nonatomic, strong) IBOutlet UIButton *btnPressSpeak;
@property (nonatomic,strong) ZBMessageManagerFaceView *faceView;

@property (nonatomic, strong) NSMutableArray *allMsgFrameArray;
@property (nonatomic, strong) NSString *curImageName;
@property (nonatomic) BOOL isLoadMore;
@property (nonatomic, strong) NSString *lastImgSendDate;

- (IBAction)chatImageClicked:(id)sender;
- (IBAction)btnSendClicked:(id)sender;
- (IBAction)btnVoiceClicked:(id)sender;
- (IBAction)btnPressSpeakClicked:(id)sender;
- (void)loadData;
- (void)loadMoreData;
- (void)sendMessageLogic;

@end

@implementation RYKChatToTargetController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboardWillShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboardWillHide:)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboardChange:)
                                                name:UIKeyboardDidChangeFrameNotification
                                              object:nil];
    
    [self.btnSend setBackgroundImage:[[UIImage imageNamed:@"ChatSend"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 3, 7, 3)] forState:UIControlStateNormal];
    [self.btnSend setBackgroundImage:[[UIImage imageNamed:@"ChatSendHighLight"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 3, 7, 3)] forState:UIControlStateHighlighted];
    
//    //初始化录音vc
//    self.recorderVC = [[ChatVoiceRecorderVC alloc]init];
//    self.recorderVC.vrbDelegate = self;
//    //初始化播放器
//    self.player = [[AVAudioPlayer alloc]init];
    
    self.btnPressSpeak.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
    self.btnPressSpeak.layer.borderWidth = 1;
    //添加长按手势
    UILongPressGestureRecognizer *longPrees = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(recordBtnLongPressed:)];
    longPrees.delegate = self;
    [self.btnPressSpeak addGestureRecognizer:longPrees];
    
    self.tvMessage.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
    self.tvMessage.layer.borderWidth = 1;
    
    [RYKXmppManager sharedManager].recvMsgDelegate = self;
    
    self.allMsgFrameArray = [[NSMutableArray alloc] init];
    self.curImageName = @"";
    self.isLoadMore = NO;
    self.lastImgSendDate = @"";
    animationDuration = 0.25;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = self.showName;
    
    if (self.tv == nil) {
        CGRect tvFrame = self.view.bounds;
        tvFrame.size.height -= self.bottomView.frame.size.height;
        self.tv = [[CTTableView alloc] initWithFrame:tvFrame];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.pullDownViewEnabled = YES;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tv.allowsSelection = NO;
        [self.view addSubview:self.tv];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tvSingleTap)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [self.tv addGestureRecognizer:tap];
        
        [self.view addSubview:self.bottomView];
        
        [self loadData];
    }
    
    if (!self.faceView)
    {
        self.faceView = [[ZBMessageManagerFaceView alloc]initWithFrame:CGRectMake(0.0f,
                                                                                  CGRectGetHeight(self.view.bounds), CGRectGetWidth(self.view.bounds), 160)];
        self.faceView.delegate = self;
        [self.view addSubview:self.faceView];
        
    }
    CGRect bottomFrame = self.bottomView.frame;
    bottomFrame.origin.y = self.view.bounds.size.height - bottomFrame.size.height;
    self.bottomView.frame = bottomFrame;
    CGRect faceFrame  = self.faceView.frame;
    faceFrame.origin.y = self.view.bounds.size.height;
    self.faceView.frame = faceFrame;
    CGRect tvFrame  = self.tv.frame;
    tvFrame.origin.y = 0;
    self.tv.frame = tvFrame;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadData
{
    [self.allMsgFrameArray removeAllObjects];
    
    NSString *previousTime = nil;
    NSArray *msgArray = [[RYKSqlLite shareManager] getMsgByTarget:self.msgTarget fromIndex:0 withRows:ROWS_PER_TIME];
    for (int i = msgArray.count - 1; i >= 0; i--) {
        MessageFrame *messageFrame = [[MessageFrame alloc] init];
        
        RYKChatMessage *chatMsg = [msgArray objectAtIndex:i];
        if (previousTime == nil) {
            messageFrame.showTime = YES;
            previousTime = chatMsg.msgDate;
        } else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *preDate = [dateFormatter dateFromString:previousTime];
            NSDate *curDate = [dateFormatter dateFromString:chatMsg.msgDate];
            NSTimeInterval pastTime = [curDate timeIntervalSinceDate:preDate];
            if (pastTime < 60) {
                messageFrame.showTime = NO;
            } else {
                messageFrame.showTime = YES;
                previousTime = chatMsg.msgDate;
            }
        }
        messageFrame.message = chatMsg;
        
        [self.allMsgFrameArray addObject:messageFrame];
    }
    
    [self.tv reloadData];
    NSInteger tagY = self.tv.contentSize.height - self.tv.frame.size.height;
    if (tagY < 0) {
        tagY = 0;
    }
    [self.tv setContentOffset:CGPointMake(0, tagY) animated:YES];
    return;
}

- (void)loadMoreData
{
    NSArray *moreMsgArray = [[RYKSqlLite shareManager] getMsgByTarget:self.msgTarget fromIndex:self.allMsgFrameArray.count withRows:ROWS_PER_TIME];
    NSString *previousTime = nil;
    NSMutableArray *msgFrameArray = [[NSMutableArray alloc]init];
    for (int i = moreMsgArray.count - 1; i >= 0; i--)
    {
        RYKChatMessage *chatMsg = [moreMsgArray objectAtIndex:i];
        MessageFrame *messageFrame = [[MessageFrame alloc] init];
        
        if (previousTime == nil) {
            messageFrame.showTime = YES;
            previousTime = chatMsg.msgDate;
        } else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *preDate = [dateFormatter dateFromString:previousTime];
            NSDate *curDate = [dateFormatter dateFromString:chatMsg.msgDate];
            NSTimeInterval pastTime = [curDate timeIntervalSinceDate:preDate];
            if (pastTime < 60) {
                messageFrame.showTime = NO;
            } else {
                messageFrame.showTime = YES;
                previousTime = chatMsg.msgDate;
            }
        }
        messageFrame.message = chatMsg;
        [msgFrameArray insertObject:messageFrame atIndex:0];
    }
    
    for (MessageFrame *msgFrame in msgFrameArray) {
        [self.allMsgFrameArray insertObject:msgFrame atIndex:0];
    }
    
    [self.tv reloadData];
    return;
}

- (void)sendMessageLogic
{
    NSString *content = self.tvMessage.text;
    if (content == nil || content.length == 0) {
        return;
    }
    
    if (![[RYKXmppManager sharedManager].xmppStream isAuthenticated]) {
        [self.view endEditing:YES];
        [self messageViewAnimationWithMessageRect:CGRectZero andDuration:animationDuration andState:ZBMessageViewStateShowNone];
        self.indicator.text = @"网络异常，发送失败";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    
    RYKChatMessage *newMsg = [[RYKChatMessage alloc] init];
    newMsg.msgContent = content;
    newMsg.msgTarget = self.msgTarget;
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    NSString *dateStr = [dateFormatter stringFromDate:nowDate];
    newMsg.msgDate = dateStr;
    newMsg.operType = @"0";
    
    [[RYKXmppManager sharedManager] sendMessage:content toUser:self.msgTarget withDate:newMsg.msgDate];
    [[RYKSqlLite shareManager] addNewMsg:newMsg];
    
    newMsg.msgDate = [newMsg.msgDate componentsSeparatedByString:@"."][0];
    [self showNewMessage:newMsg];
    self.tvMessage.text = @"";
    return;
}

- (void)showNewMessage:(RYKChatMessage *)newMsg
{
    MessageFrame *newMsgFrame = [[MessageFrame alloc] init];
    if (self.allMsgFrameArray.count <= 0) {
        newMsgFrame.showTime = YES;
    } else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        MessageFrame *preMsgFrame = (MessageFrame *)[self.allMsgFrameArray lastObject];
        NSDate *preDate = [dateFormatter dateFromString:preMsgFrame.message.msgDate];
        NSDate *curDate = [dateFormatter dateFromString:newMsg.msgDate];
        NSTimeInterval pastTime = [curDate timeIntervalSinceDate:preDate];
        if (pastTime < 60) {
            newMsgFrame.showTime = NO;
        } else {
            newMsgFrame.showTime = YES;
        }
    }
    newMsgFrame.message = newMsg;
    NSArray *arr = [NSArray arrayWithObjects:[NSIndexPath indexPathForRow:self.allMsgFrameArray.count inSection:0], nil];
    [self.allMsgFrameArray addObject:newMsgFrame];
    [self.tv insertRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];
    NSInteger tagY = self.tv.contentSize.height - self.tv.frame.size.height;
    if (tagY < 0) {
        tagY = 0;
    }
    [self.tv setContentOffset:CGPointMake(0, tagY) animated:YES];
    return;
}

- (void)uploadImage:(NSData *)imgData;
{
    self.indicator.text = @"正在上传图片..";
    [self.indicator show];
    
    NSString * path = [RYKWebService chatPostImageUrl];
    NSURL* url = [NSURL URLWithString:path];
    ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:url];
    request.showAccurateProgress = YES;
    request.shouldContinueWhenAppEntersBackground = YES;
    request.delegate = self;
    [request setRequestMethod:@"POST"];
    [request setPostFormat:ASIMultipartFormDataPostFormat];
    
    NSDate *nowDate = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[nowDate timeIntervalSince1970]];
    self.curImageName = [NSString stringWithFormat:@"%@.jpg", timeSp];
    
    [request addData:imgData withFileName:self.curImageName andContentType:@"image/jpeg" forKey:@"pic[]"];
    
    [request start];
    return;
}

#pragma mark - click event
- (void)tvSingleTap
{
    [self.view endEditing:YES];
    [self messageViewAnimationWithMessageRect:CGRectZero andDuration:animationDuration andState:ZBMessageViewStateShowNone];
    return;
}

- (IBAction)chatImageClicked:(id)sender
{
    if (self.indicator.showing) {
        return;
    }
    
    [self.view endEditing:YES];
    [self messageViewAnimationWithMessageRect:CGRectZero andDuration:animationDuration andState:ZBMessageViewStateShowNone];
    
    if (![[RYKXmppManager sharedManager].xmppStream isAuthenticated]) {
        self.indicator.text = @"网络异常，无法发送";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    
    UIActionSheet *actSheet;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从手机相册选择", @"拍照", nil];
    } else {
        actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从手机相册选择", nil];
    }
    [actSheet showInView:self.view];
    return;
}

- (IBAction)btnSendClicked:(id)sender
{
    [self sendMessageLogic];
    return;
}

- (IBAction)btnVoiceClicked:(id)sender
{
    [self.tvMessage resignFirstResponder];
    [self messageViewAnimationWithMessageRect:self.faceView.frame andDuration:animationDuration andState:ZBMessageViewStateShowFace];
    return;
}

- (IBAction)btnPressSpeakClicked:(id)sender
{
    return;
}

- (void)recordBtnLongPressed:(UILongPressGestureRecognizer*) longPressedRecognizer{
    //长按开始
    if(longPressedRecognizer.state == UIGestureRecognizerStateBegan) {
        
    }//长按结束
    else if(longPressedRecognizer.state == UIGestureRecognizerStateEnded || longPressedRecognizer.state == UIGestureRecognizerStateCancelled){
        
    }
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self sendMessageLogic];
    return YES;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate * DataSource
- (void)pullDownRefreshAnimationEnd:(CTTableView *)ctTableView
{
    if (!self.isLoadMore) {
        return;
    }
    self.isLoadMore = NO;
    CGSize oldPoint = [self.tv contentSize];
    [self loadMoreData];
    CGSize newSize = [self.tv contentSize];
    NSInteger tagY = newSize.height - oldPoint.height;
    if (tagY > 30) {
        tagY -= 30;
    }
    
    [self.tv setContentOffset:CGPointMake(0, tagY) animated:NO];
    return;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pullDownRefreshEnd) userInfo:nil repeats:NO];
    return YES;
}

- (void)pullDownRefreshEnd
{
    [self.tv reloadData];
    self.isLoadMore = YES;
    return;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allMsgFrameArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.allMsgFrameArray[indexPath.row] cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // 设置数据
    cell.messageFrame = self.allMsgFrameArray[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - MessageCell delegate
- (void)messageCellLoadUrlImageOk:(MessageCell *)msgCell
{
    NSArray *visibleArray = [self.tv indexPathsForVisibleRows];
    NSIndexPath *curIndexPath = [self.tv indexPathForCell:msgCell];
    for (NSIndexPath *indexPath in visibleArray) {
        if (curIndexPath.section == indexPath.section && curIndexPath.row == indexPath.row) {
            MessageFrame *curFrame = [self.allMsgFrameArray objectAtIndexedSubscript:indexPath.row];
            // 重算图片大小
            curFrame.message = curFrame.message;
            [self.tv reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
    return;
}

#pragma mark - FaceView delegate
- (void)SendTheFaceStr:(NSString *)faceStr isDelete:(BOOL)dele
{
//    NSLog(@"faceStr=%@ isDelete=%hhd", faceStr, dele);
    if (!dele) {
        if (faceStr == nil) {
            return;
        }
        self.tvMessage.text = [self.tvMessage.text stringByAppendingString:faceStr];
        CGSize cSize = self.tvMessage.contentSize;
        CGSize mSize = self.tvMessage.frame.size;
        CGPoint bottomPoint = CGPointMake(0, cSize.height - mSize.height);
        [self.tvMessage setContentOffset:bottomPoint animated:YES];
    } else {
        NSString *msg = self.tvMessage.text;
        if (msg.length <= 0) {
            return;
        }
        NSString *lastStr = [msg substringFromIndex:msg.length - 1];
        if (![lastStr isEqualToString:@"]"]) {
            self.tvMessage.text = [msg substringToIndex:msg.length - 1];
            return;
        }
        
        BOOL isFindFace = NO;
        NSInteger facePreSignIndex = -1;
        if (msg.length > 2) {
            // 判断前两位
            NSString *curStr = [msg substringWithRange:NSMakeRange(msg.length - 3, 1)];
            if ([curStr isEqualToString:@"["]) {
                isFindFace = YES;
                facePreSignIndex = msg.length - 3;
            }
            
        }
        if (!isFindFace && msg.length > 3) {
            // 判断前三位
            NSString *curStr = [msg substringWithRange:NSMakeRange(msg.length - 4, 1)];
            if ([curStr isEqualToString:@"["]) {
                isFindFace = YES;
                facePreSignIndex = msg.length - 4;
            }
            
        }
        if (!isFindFace && msg.length > 4) {
            // 判断前四位
            NSString *curStr = [msg substringWithRange:NSMakeRange(msg.length - 5, 1)];
            if ([curStr isEqualToString:@"["]) {
                isFindFace = YES;
                facePreSignIndex = msg.length - 5;
            }
            
        }
        
        if (isFindFace) {
            self.tvMessage.text = [msg substringToIndex:facePreSignIndex];
            CGPoint bottomPoint = CGPointMake(0, self.tvMessage.contentSize.height - self.tvMessage.frame.size.height);
            [self.tvMessage setContentOffset:bottomPoint animated:YES];
        } else {
            self.tvMessage.text = [msg substringToIndex:msg.length - 1];
        }
    }
    return;
}

#pragma mark - UITextView delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self messageViewAnimationWithMessageRect:keyboardRect andDuration:animationDuration andState:ZBMessageViewStateShowNone];
    return;
}

- (BOOL)textView: (UITextView *)textview shouldChangeTextInRange: (NSRange)range replacementText: (NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [self sendMessageLogic];
        return NO;
    }
    
    return YES;
}

#pragma mark -keyboard
- (void)keyboardWillHide:(NSNotification *)notification{
    
    keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    animationDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
}

- (void)keyboardWillShow:(NSNotification *)notification{
    keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    animationDuration= [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
}

- (void)keyboardChange:(NSNotification *)notification{
    if ([[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y<CGRectGetHeight(self.view.frame)) {
        [self messageViewAnimationWithMessageRect:[[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue] andDuration:0.25 andState:ZBMessageViewStateShowNone];
    }
}

#pragma mark - messageView animation
- (void)messageViewAnimationWithMessageRect:(CGRect)rect andDuration:(double)duration andState:(ZBMessageViewState)state
{
    
    [UIView animateWithDuration:duration animations:^{
        self.tv.frame = CGRectMake(0.0f,0-CGRectGetHeight(rect),CGRectGetWidth(self.view.frame),CGRectGetHeight(self.tv.frame));
        self.bottomView.frame = CGRectMake(0.0f,CGRectGetHeight(self.view.frame)-CGRectGetHeight(rect)-CGRectGetHeight(self.bottomView.frame),CGRectGetWidth(self.view.frame),CGRectGetHeight(self.bottomView.frame));
        
        if (state == ZBMessageViewStateShowNone) {
            self.faceView.frame = CGRectMake(0.0f,CGRectGetHeight(self.view.frame),CGRectGetWidth(self.view.frame),CGRectGetHeight(self.faceView.frame));
        } else {
            self.faceView.frame = CGRectMake(0.0f,CGRectGetHeight(self.view.frame)-CGRectGetHeight(rect),CGRectGetWidth(self.view.frame),CGRectGetHeight(self.faceView.frame));
        }
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - rykXmppManager delegate
- (void)rykXmppManagerDidRecvMsg:(RYKChatMessage *)chatMsg
{
    if ([chatMsg.msgTarget isEqualToString:self.msgTarget])
    {
        [self loadData];
    }
    return;
}

- (void)rykXmppManagerDidSendMsg:(NSString *)msgDate
{
    [self msgSendResult:msgDate withResult:@"1"];
    return;
}

- (void)msgSendResult:(NSString *)msgDate withResult:(NSString *)status
{
    NSString *msgDateStr = [msgDate componentsSeparatedByString:@"."][0];
    NSInteger tagMsgIndex = -1;
    for (MessageFrame *msgFrame in self.allMsgFrameArray) {
        if ([msgFrame.message.msgDate isEqualToString:msgDateStr]) {
            msgFrame.message.sendStatus = status;
            tagMsgIndex = [self.allMsgFrameArray indexOfObject:msgFrame];
        }
    }
    
    if (tagMsgIndex != -1) {
        NSArray *visibleArray = [self.tv indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visibleArray) {
            if (indexPath.row == tagMsgIndex) {
                [self.tv reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }
    return;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    } else {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    NSString *dateStr = [dateFormatter stringFromDate:nowDate];
    self.lastImgSendDate = dateStr;
    
    NSData* imgData = UIImageJPEGRepresentation(portraitImg, 0.5);
    [self uploadImage:imgData];
    
    NSString* imgPath = [[RYKWebService msgTargetDir:self.msgTarget] stringByAppendingPathComponent:self.curImageName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:[RYKWebService msgTargetDir:self.msgTarget]]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:[RYKWebService msgTargetDir:self.msgTarget] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    [imgData writeToFile:imgPath atomically:YES];
    
    
    RYKChatMessage *newMsg = [[RYKChatMessage alloc] init];
    newMsg.msgContent = @"";
    newMsg.contentType = @"2";
    newMsg.msgTarget = self.msgTarget;
    newMsg.msgDate = dateStr;
    newMsg.operType = @"0";
    newMsg.localUrl = imgPath;
    
    [[RYKSqlLite shareManager] addNewMsg:newMsg];
    newMsg.msgDate = [newMsg.msgDate componentsSeparatedByString:@"."][0];
    [self showNewMessage:newMsg];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    return;
}

#pragma mark - ASIHttpRequest Delegate
- (void)requestFailed:(ASIHTTPRequest *)request
{
    self.indicator.text = @"图片上传失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self msgSendResult:self.lastImgSendDate withResult:@"2"];
}

- (void)request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    // 图片上传成功
    if ([msg hasPrefix:@"http:"] && ([msg hasSuffix:@".jpg"] || [msg hasSuffix:@".png"])) {
        [self.indicator hide];
        [[RYKXmppManager sharedManager] sendMessage:msg toUser:self.msgTarget withDate:self.lastImgSendDate];
        return;
    }
    
    // 失败
    self.indicator.text = @"图片上传失败";
    [[RYKSqlLite shareManager] changeSendStatus:self.lastImgSendDate toStatus:@"2"];
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self msgSendResult:self.lastImgSendDate withResult:@"2"];
    return;
}

@end
