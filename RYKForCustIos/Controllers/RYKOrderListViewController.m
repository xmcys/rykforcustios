//
//  RYKOrderListViewController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKOrderListViewController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKCustOrderStatus.h"
#import "RYKOrderDetailViewController.h"
#import "ConsSystemMsg.h"
#import "ZbCommonFunc.h"
#import "CXButton.h"

#define BTN_TYPE_UNCHECKED 1
#define BTN_TYPE_CHECKED 2
#define BTN_TYPE_COMPLETED 3

#define PROCESS_COMFIRM_RECV 4    // 收货确认
#define PROCESS_UNREAD_CHECKED 5  // 获取已确定订单数量

@interface RYKOrderListViewController ()<CTTableViewDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, CTURLConnectionDelegate, RYKOrderDetailViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray * unCheckedArray;      // 待确定列表
@property (nonatomic, strong) NSMutableArray * checkedArray;        // 待收货列表
@property (nonatomic, strong) NSMutableArray * completedArray;      // 已完成列表
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) CTTableView *unCheckedTv;
@property (strong, nonatomic) CTTableView *checkedTv;
@property (strong, nonatomic) CTTableView *completedTv;
@property (nonatomic) NSInteger curShowType;
@property (nonatomic) RYKCustOrderStatus *curOperOrder;

@property (nonatomic, strong) IBOutlet UILabel *flagLab;

- (void)drawUnreadStatus:(int)flag;
- (IBAction)onTabChanged:(UIButton *)btn;
- (void)btnConfirmOnClicked:(UIButton *)btn;
- (void)loadUnreadCheckedData;
- (void)loadUnCheckedData;
- (void)loadCheckedData;
- (void)loadCompletedData;

@end

@implementation RYKOrderListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"订单处理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_DDGL, CONS_MODULE_DDGL, nil]];
    
    self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TabBg"]];
    
    self.flagLab.layer.cornerRadius = 5;
    self.flagLab.layer.masksToBounds = YES;
    
    self.unCheckedArray = [[NSMutableArray alloc] init];
    self.checkedArray = [[NSMutableArray alloc] init];
    self.completedArray = [[NSMutableArray alloc] init];
    self.curShowType = BTN_TYPE_UNCHECKED;
    self.curOperOrder = [[RYKCustOrderStatus alloc] init];
    [self drawUnreadStatus:0];
    [self loadCheckedData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.unCheckedTv == nil) {
        self.unCheckedTv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.headerView.frame.size.height)];
        self.unCheckedTv.backgroundColor = [UIColor clearColor];
        self.unCheckedTv.pullUpViewEnabled = NO;
        self.unCheckedTv.delegate = self;
        self.unCheckedTv.dataSource = self;
        self.unCheckedTv.ctTableViewDelegate = self;
        self.unCheckedTv.tag = 10 + BTN_TYPE_UNCHECKED;
        [self.view addSubview:self.unCheckedTv];
        
        self.checkedTv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.headerView.frame.size.height)];
        self.checkedTv.backgroundColor = [UIColor clearColor];
        self.checkedTv.pullUpViewEnabled = NO;
        self.checkedTv.delegate = self;
        self.checkedTv.dataSource = self;
        self.checkedTv.ctTableViewDelegate = self;
        self.checkedTv.tag = 10 + BTN_TYPE_CHECKED;
        [self.view addSubview:self.checkedTv];
        
        self.completedTv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.headerView.frame.size.height)];
        self.completedTv.backgroundColor = [UIColor clearColor];
        self.completedTv.pullUpViewEnabled = NO;
        self.completedTv.delegate = self;
        self.completedTv.dataSource = self;
        self.completedTv.ctTableViewDelegate = self;
        self.completedTv.tag = 10 + BTN_TYPE_COMPLETED;
        [self.view addSubview:self.completedTv];
        
        [self onTabChanged:((UIButton *)[self.headerView viewWithTag:BTN_TYPE_UNCHECKED])];
        [self loadUnreadCheckedData];
    }
}

- (void)drawUnreadStatus:(int)flag
{
    if (flag == 0) {
        self.flagLab.hidden = YES;
    } else if (flag > 99) {
        self.flagLab.hidden = NO;
        self.flagLab.text = @"99+";
    } else {
        self.flagLab.hidden = NO;
        self.flagLab.text = [NSString stringWithFormat:@"%d",flag];
    }
    [self.flagLab sizeToFit];
    CGRect labFrame = self.flagLab.frame;
    labFrame.size.width = labFrame.size.width + 5;
    self.flagLab.frame = labFrame;
}

#pragma mark -
#pragma mark all button operation
-(IBAction)onTabChanged:(UIButton *)btn
{
    UIImage *img = [[UIImage imageNamed:@"on"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    for (int i = BTN_TYPE_UNCHECKED; i <= BTN_TYPE_COMPLETED; i++) {
        UIButton *curBtn = (UIButton *)[self.headerView viewWithTag:i];
        // 无选中图片
        [curBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
        [curBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
        // 字色黑色
        [curBtn setTitleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] forState:UIControlStateNormal];
        [curBtn setTitleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] forState:UIControlStateHighlighted];
        
        CTTableView *tbView = (CTTableView *)[self.view viewWithTag:10 + i];
        tbView.hidden = YES;
    }
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    CTTableView *showTbView = (CTTableView *)[self.view viewWithTag:10 + btn.tag];
    showTbView.hidden = NO;
    self.curShowType = btn.tag;
    if (self.curShowType == BTN_TYPE_UNCHECKED) {
        [self drawUnreadStatus:self.checkedArray.count];
        if (self.unCheckedArray.count <= 0) {
            [self loadUnCheckedData];
        }
    } else if (self.curShowType == BTN_TYPE_CHECKED) {
        if (self.checkedArray.count <= 0) {
            [self loadCheckedData];
        }
    } else {
        if (self.completedArray.count <= 0) {
            [self loadCompletedData];
        }
    }
    
}

- (void)btnConfirmOnClicked:(CXButton *)btn
{
    self.curOperOrder = [self.checkedArray objectAtIndex:btn.index];
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderConfirmUrl:self.curOperOrder.orderId withFlowCode:@"FINISH" withTime:@"-1"] delegate:self];
    conn.tag = PROCESS_COMFIRM_RECV;
    [conn start];
    return;
}

#pragma mark -
#pragma mark all url operation
- (void)loadUnreadCheckedData
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderFlow:@"DELV_PACK" withFlowStatus:@"1"] delegate:self];
    conn.tag = PROCESS_UNREAD_CHECKED;
    [conn start];
    return;
}

- (void)loadUnCheckedData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderFlow:@"STORE_CONFIRM" withFlowStatus:@"0"] delegate:self];
    conn.tag = BTN_TYPE_UNCHECKED;
    [conn start];
    return;
}

- (void)loadCheckedData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderFlow:@"DELV_PACK" withFlowStatus:@"1"] delegate:self];
    conn.tag = BTN_TYPE_CHECKED;
    [conn start];
    return;
}

- (void)loadCompletedData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderFlow:@"FINISH" withFlowStatus:@"1"] delegate:self];
    conn.tag = BTN_TYPE_COMPLETED;
    [conn start];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.curShowType == BTN_TYPE_UNCHECKED) {
        [self.unCheckedTv ctTableViewDidScroll];
    } else if (self.curShowType == BTN_TYPE_CHECKED) {
        [self.checkedTv ctTableViewDidScroll];
    } else {
        [self.completedTv ctTableViewDidScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.curShowType == BTN_TYPE_UNCHECKED) {
        [self.unCheckedTv ctTableViewDidEndDragging];
    } else if (self.curShowType == BTN_TYPE_CHECKED) {
        [self.checkedTv ctTableViewDidEndDragging];
    } else {
        [self.completedTv ctTableViewDidEndDragging];
    }
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.curShowType == BTN_TYPE_UNCHECKED) {
        [self loadUnCheckedData];
    } else if (self.curShowType == BTN_TYPE_CHECKED) {
        [self loadCheckedData];
    } else {
        [self loadCompletedData];
    }
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger row = 0;
    if (tableView.tag == 10 + BTN_TYPE_UNCHECKED) {
        row = self.unCheckedArray.count;
    } else if (tableView.tag == 10 + BTN_TYPE_CHECKED) {
        row = self.checkedArray.count;
    } else {
        row = self.completedArray.count;
    }
    return row;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height = 0;
    if (tableView.tag == 10 + BTN_TYPE_CHECKED) {
        height = 160 + 5;
    } else {
        height = 96 + 5;
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        NSString *cellXibName = @"RYKUnCheckedOrderCell";
        if (tableView.tag == 10 + BTN_TYPE_CHECKED) {
            cellXibName = @"RYKCheckedOrderCell";
        }
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:cellXibName owner:nil options:nil];
        
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 5, tableView.frame.size.width - 10, view.frame.size.height);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        
        [cell addSubview:view];
    }
    
    if (tableView.tag == 10 + BTN_TYPE_CHECKED) {
        UILabel *labTime = (UILabel *)[cell viewWithTag:1];
        UILabel *labAddr = (UILabel *)[cell viewWithTag:2];
        UILabel *labMoney = (UILabel *)[cell viewWithTag:3];
        UILabel *labCnt = (UILabel *)[cell viewWithTag:4];
        CXButton *btnConfirm = (CXButton *)[cell viewWithTag:5];
        
        RYKCustOrderStatus *curOrder = [self.checkedArray objectAtIndex:indexPath.row];
        labTime.text = curOrder.orderDate;
        labAddr.text = curOrder.orderAddr;
        labMoney.text = [NSString stringWithFormat:@"¥ %@", curOrder.amtOrderSum];
        labCnt.text = curOrder.qtyOrderSum;
        
        UIImage *img = [[UIImage imageNamed:@"btn_1"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 5, 3, 5)];
        UIImage *imgHl = [[UIImage imageNamed:@"btn_2"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 5, 3, 5)];
        btnConfirm.index = indexPath.row;
        [btnConfirm setBackgroundImage:img forState:UIControlStateNormal];
        [btnConfirm setBackgroundImage:imgHl forState:UIControlStateHighlighted];
        [btnConfirm addTarget:self action:@selector(btnConfirmOnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        UILabel *labTime = (UILabel *)[cell viewWithTag:1];
        UILabel *labMoney = (UILabel *)[cell viewWithTag:2];
        UILabel *labCnt = (UILabel *)[cell viewWithTag:3];
        UILabel *labDes = (UILabel *)[cell viewWithTag:4];
        
        RYKCustOrderStatus *curOrder;
        if (tableView.tag == 10 + BTN_TYPE_UNCHECKED) {
            curOrder = [self.unCheckedArray objectAtIndex:indexPath.row];
            labDes.text = @"待确定";
        } else {
            curOrder = [self.completedArray objectAtIndex:indexPath.row];
            labDes.text = @"";
        }
        
        labTime.text = curOrder.orderDate;
        labMoney.text = [NSString stringWithFormat:@"¥ %@", curOrder.amtOrderSum];
        labCnt.text = curOrder.qtyOrderSum;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RYKCustOrderStatus *curOrder;
    if (tableView.tag == 10 + BTN_TYPE_UNCHECKED) {
        curOrder = [self.unCheckedArray objectAtIndex:indexPath.row];
        curOrder.flowCode = @"STORE_CONFIRM";
    } else if (tableView.tag == 10 + BTN_TYPE_CHECKED) {
        curOrder = [self.checkedArray objectAtIndex:indexPath.row];
        curOrder.flowCode = @"DELV_PACK";
    } else {
        curOrder = [self.completedArray objectAtIndex:indexPath.row];
        curOrder.flowCode = @"FINISH";
    }
    
    RYKOrderDetailViewController *controller = [[RYKOrderDetailViewController alloc] initWithNibName:@"RYKOrderDetailController" bundle:nil];
    controller.curOrder = curOrder;
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == BTN_TYPE_UNCHECKED){
        RYKCustOrderStatus *orderList = [[RYKCustOrderStatus alloc] init];
        [orderList parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            
            [self.unCheckedArray removeAllObjects];
            [self.unCheckedArray addObjectsFromArray:array];
            [self.unCheckedTv reloadData];
        }];
    } else if (connection.tag == BTN_TYPE_CHECKED){
        RYKCustOrderStatus *orderList = [[RYKCustOrderStatus alloc] init];
        [orderList parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            
            [self.checkedArray removeAllObjects];
            [self.checkedArray addObjectsFromArray:array];
            [self.checkedTv reloadData];
            [self drawUnreadStatus:self.checkedArray.count];
        }];
    } else if (connection.tag == BTN_TYPE_COMPLETED){
        RYKCustOrderStatus *orderList = [[RYKCustOrderStatus alloc] init];
        [orderList parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            
            [self.completedArray removeAllObjects];
            [self.completedArray addObjectsFromArray:array];
            [self.completedTv reloadData];
        }];
    } else if (connection.tag == PROCESS_COMFIRM_RECV) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            self.indicator.text = curMsg.msg;
            if (![curMsg.code isEqualToString:@"1"]) {
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0 complete:^(void){
                NSInteger index = [self.checkedArray indexOfObject:self.curOperOrder];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [self.checkedArray removeObject:self.curOperOrder];
                [self.checkedTv deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationRight];
                    [self drawUnreadStatus:self.checkedArray.count];
            }];
        }];
    } else if (connection.tag == PROCESS_UNREAD_CHECKED) {
        RYKCustOrderStatus *orderList = [[RYKCustOrderStatus alloc] init];
        [orderList parseData:data complete:^(NSArray *array){
        }];
    }
}

#pragma mark -
#pragma mark RYKOrderDetailViewController Delegate
- (void)rykOrderDetailViewControllerConfirmOK
{
    if (self.curShowType == BTN_TYPE_UNCHECKED) {
        [self loadUnCheckedData];
        // 确认后，已发货订单肯定有新内容
        [self loadCheckedData];
        [self.checkedArray removeAllObjects];
    } else if (self.curShowType == BTN_TYPE_CHECKED) {
        [self loadCheckedData];
    } else {
        [self loadCompletedData];
    }
    return;
}

@end
