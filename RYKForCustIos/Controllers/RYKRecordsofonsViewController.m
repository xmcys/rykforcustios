//
//  RYKRecordsofonsViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKRecordsofonsViewController.h"
#import "CTTableView.h"
#import "RYKRecordersofGoodsViewController.h"
#import "RYKWebService.h"
#import "RYKCustOrder.h"
#import "RYKListRecords.h"

#define PROCESS_GET_ORDER    1
#define PROCESS_GET_ORDERDETAIL 2
#define PROCESS_GET_ORDERINFO  100

@interface RYKRecordsofonsViewController ()<UITableViewDelegate,UITableViewDataSource,CTTableViewDelegate>
@property (nonatomic, strong) CTTableView     *tableview;
@property (nonatomic, strong) IBOutlet UIView *headerview;
@property (nonatomic, strong) NSMutableArray  *recorderarray;        //总订单量和总金额数组
@property (nonatomic, strong) NSMutableArray  *recordersumarray;
@property (nonatomic, strong) NSMutableArray  *msgarray;
@property (nonatomic, strong) IBOutlet UILabel *labname;
@property (nonatomic, strong) IBOutlet UILabel *labphone;
@property (nonatomic, strong) IBOutlet UILabel *labordersum;
@property (nonatomic, strong) IBOutlet UILabel *labpricesum;
@property (nonatomic, strong) NSMutableArray * tmparray;


@end

@implementation RYKRecordsofonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithuserId:(NSString *)userId userName:(NSString *)username mobilePhone:(NSString *)mobilephone
{
    if (self = [super init]) {
        self.userId = userId;
        self.username = username;
        self.mobilephone = mobilephone;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.recorderarray = [[NSMutableArray alloc] init];
    self.recordersumarray = [[NSMutableArray alloc] init];
    self.tmparray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    self.title = @"消费记录查询";
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.pullDownViewEnabled = NO;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        [self.tableview setTableHeaderView:self.headerview];
        [self.view addSubview:self.tableview];
        [self loadData];
    }
    [self drawUserInfo];
    [self drawUserOrder];
}

- (void)drawUserInfo
{
    self.labname.text = self.username;
    self.labphone.text = self.mobilephone;
}
- (void)drawUserOrder
{
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderIdDetail:self.userId]  delegate:self];
    conn.tag = PROCESS_GET_ORDERDETAIL;
    [conn start];
    return;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    
    CTURLConnection *conn1 = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderId:self.userId] delegate:self];
    conn1.tag = PROCESS_GET_ORDER;
    [conn1 start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

#pragma mark - CTTableViewDelegate * Datasource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordersumarray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 270;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKRecordersListCell" owner:nil        options:nil];
        UIView *view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 10, tableView.frame.size.width-5*2, 260);
        view.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        [cell addSubview:view];
    }
    UIView *xuxian = (UIView *)[cell viewWithTag:20];
    UILabel *labdate = (UILabel *)[cell viewWithTag:100];
    UILabel *labordersum = (UILabel *)[cell viewWithTag:101];
    UILabel *labamtordersum = (UILabel *)[cell viewWithTag:102];
    UILabel *labelname = (UILabel *)[cell viewWithTag:103];
    UILabel *labelunit = (UILabel *)[cell viewWithTag:105];
    UILabel *labelorder = (UILabel *)[cell viewWithTag:106];
    UILabel *labOtherGoodsCnt = (UILabel *)[cell viewWithTag:108];
    xuxian.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    RYKCustName *msg = (RYKCustName *)[self.recordersumarray objectAtIndex:indexPath.row];
    NSString *str = [msg.orderDate substringToIndex:19];
    labdate.text = str;
    labamtordersum.text = msg.qtyOrderSum;
    labordersum.text = [NSString stringWithFormat:@"¥ %@",msg.amtOrderSum];

    labelname.text = msg.firstGoodsName;
    labelunit.text = [NSString stringWithFormat:@"¥ %@/%@", msg.firstGoodsPrice, msg.firstGoodsUnit];
    labelorder.text = [NSString stringWithFormat:@"x%@", msg.firstGoodsCnt];
    labOtherGoodsCnt.text = msg.otherGoodsCnt;
    return cell;
}

- (void)drawdetail
{


}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RYKCustName *msg = [self.recordersumarray objectAtIndex:indexPath.row];
    RYKRecordersofGoodsViewController *controller = [[RYKRecordersofGoodsViewController alloc] initWithOrder:msg.orderId];
    controller.orderDate = msg.orderDate;
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
     if (connection.tag == PROCESS_GET_ORDER) {
        RYKCustOrder *msg = [[RYKCustOrder alloc] init];
        [msg parseData:data complete:^(RYKCustOrder *msg){
            [self.indicator hide];
            self.labordersum.text = msg.qtyOrderSum;
            self.labpricesum.text = msg.amtOderSum;
            if ([msg.amtOderSum isEqualToString:@"null"]) {
                self.labpricesum.text = @"";
            }
            if ([msg.qtyOrderSum isEqualToString:@"null"]) {
                self.labordersum.text = @"";
            }
        }];
    }else if (connection.tag == PROCESS_GET_ORDERDETAIL) {
        RYKCustName *msg = [[RYKCustName alloc] init];
        [msg parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.recordersumarray removeAllObjects];
            [self.recordersumarray addObjectsFromArray:array];
    
            for (int i = 0; i < self.recordersumarray.count; i++) {
                RYKCustName *msg = (RYKCustName *)[self.recordersumarray objectAtIndex:i];
                NSString *params = [NSString stringWithFormat:@"<Test><orderId>%@</orderId></Test>",msg.orderId];
                CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getListRecords] params:params delegate:self];
                conn.tag = PROCESS_GET_ORDERINFO + i;
                [conn start];
            }
        }];
    } else if (connection.tag >= PROCESS_GET_ORDERINFO){
        RYKListRecords *listRecords = [[RYKListRecords alloc] init];
        [listRecords parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            NSInteger orderIndex = connection.tag - PROCESS_GET_ORDERINFO;
            if (array.count == 0) {
                return;
            }
            RYKListRecords *firstRecord = [array objectAtIndex:0];
            RYKCustName *msg = (RYKCustName *)[self.recordersumarray objectAtIndex:orderIndex];
            msg.firstGoodsName = firstRecord.brandName;
            msg.firstGoodsCnt = firstRecord.qtyOrder;
            msg.firstGoodsPrice = firstRecord.price;
            msg.firstGoodsUnit = firstRecord.brandUnit;
            msg.otherGoodsCnt = [NSString stringWithFormat:@"%d", (array.count - 1)];
            [self.tableview reloadData];
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
