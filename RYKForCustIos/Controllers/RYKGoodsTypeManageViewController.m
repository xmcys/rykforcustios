//
//  RYKGoodsTypeManageViewController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-11-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#define URL_TAG_TYPE 1
#define URL_TAG_MOVE_UP 2
#define URL_TAG_MOVE_DOWN 3
#define URL_TAG_MOVE_TOP 4

#import "RYKGoodsTypeManageViewController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKGoodsTypeSort.h"
#import "ConsSystemMsg.h"

@interface RYKTypeManageCell ()

@end

@implementation RYKTypeManageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setTypeInfo:(RYKGoodsTypeSort *)typeInfo
{
    UILabel *labTitle = (UILabel *)[self viewWithTag:1];
    UIButton *btnMoveUp = (UIButton *)[self viewWithTag:2];
    UIButton *btnMoveDown = (UIButton *)[self viewWithTag:3];
    UIButton *btnMoveToTop = (UIButton *)[self viewWithTag:4];
    
    labTitle.text = typeInfo.brandTypeName;
    [btnMoveUp addTarget:self action:@selector(btnMoveUpOnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnMoveDown addTarget:self action:@selector(btnMoveDownOnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnMoveToTop addTarget:self action:@selector(btnMoveToTopOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    return;
}

- (void)btnMoveUpOnClick
{
    [self.delegate typeManageCellBtnMoveUpOnClick:self];
}

- (void)btnMoveDownOnClick
{
    [self.delegate typeManageCellBtnMoveDownOnClick:self];
}

- (void)btnMoveToTopOnClick
{
    [self.delegate typeManageCellBtnMoveToTopOnClick:self];
}

@end


@interface RYKGoodsTypeManageViewController () <CTTableViewDelegate, UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate, TypeManageCellDelegate>

@property (nonatomic, strong) CTTableView *tv;  // 未处理订单列表
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, assign) NSInteger curOperIndex;

- (void)loadTypeData;

@end

@implementation RYKGoodsTypeManageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"类别管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_LBPX, CONS_MODULE_LBPX, nil]];
    
    self.indicator.backgroundTouchable = NO;
    self.array = [[NSMutableArray alloc] init];
    self.curOperIndex = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.pullDownViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        
        [self loadTypeData];
    }
    
}

- (void)loadTypeData
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getBrandTypeList] delegate:self];
    conn.tag = URL_TAG_TYPE;
    [conn start];
    return;
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    RYKTypeManageCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[RYKTypeManageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
        
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKTypeManageCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 5, self.tv.frame.size.width - 10, view.frame.size.height);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        
        [cell addSubview:view];
    }
    
    RYKGoodsTypeSort *typeInfo = [self.array objectAtIndex:indexPath.row];
    [cell setTypeInfo:typeInfo];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == URL_TAG_TYPE) {
        self.indicator.text = @"加载失败";
    } else {
        self.indicator.text = @"提交失败";
    }
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_TYPE) {
        RYKGoodsTypeSort *typeSort = [[RYKGoodsTypeSort alloc] init];
        [typeSort parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self.tv reloadData];
        }];
    } else if (connection.tag == URL_TAG_MOVE_UP) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            if (![curMsg.code isEqualToString:@"1"]) {
                self.indicator.text = @"提交失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self loadTypeData];
        }];
    } else if (connection.tag == URL_TAG_MOVE_DOWN) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            if (![curMsg.code isEqualToString:@"1"]) {
                self.indicator.text = @"提交失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self loadTypeData];
        }];
    } else if (connection.tag == URL_TAG_MOVE_TOP) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            if (![curMsg.code isEqualToString:@"1"]) {
                self.indicator.text = @"提交失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self loadTypeData];
        }];
    }
}

#pragma mark - RYKTypeManageCell delegate
- (void)typeManageCellBtnMoveUpOnClick:(RYKTypeManageCell *)cell
{
    NSIndexPath *indexPath = [self.tv indexPathForCell:cell];
    if (indexPath.row == 0) {
        self.indicator.text = @"第一条只能下移";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力提交中..";
    [self.indicator show];
    
    self.curOperIndex = indexPath.row;
    RYKGoodsTypeSort *typeSortInfo = [self.array objectAtIndex:indexPath.row];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService typeMoveUpUrl] params:[RYKWebService typeSortParams:typeSortInfo.brandType serialNo:typeSortInfo.serialNo] delegate:self];
    conn.tag = URL_TAG_MOVE_UP;
    [conn start];
    return;
}

- (void)typeManageCellBtnMoveDownOnClick:(RYKTypeManageCell *)cell
{
    NSIndexPath *indexPath = [self.tv indexPathForCell:cell];
    if (indexPath.row == self.array.count - 1) {
        self.indicator.text = @"最后一条不能下移";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力提交中..";
    [self.indicator show];
    
    self.curOperIndex = indexPath.row;
    RYKGoodsTypeSort *typeSortInfo = [self.array objectAtIndex:indexPath.row];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService typeMoveDownUrl] params:[RYKWebService typeSortParams:typeSortInfo.brandType serialNo:typeSortInfo.serialNo] delegate:self];
    conn.tag = URL_TAG_MOVE_DOWN;
    [conn start];
    return;
}

- (void)typeManageCellBtnMoveToTopOnClick:(RYKTypeManageCell *)cell
{
    NSIndexPath *indexPath = [self.tv indexPathForCell:cell];
    if (indexPath.row == 0) {
        self.indicator.text = @"第一条只能下移";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力提交中..";
    [self.indicator show];
    
    self.curOperIndex = indexPath.row;
    RYKGoodsTypeSort *typeSortInfo = [self.array objectAtIndex:indexPath.row];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService typeMoveToTopUrl] params:[RYKWebService typeSortParams:typeSortInfo.brandType serialNo:typeSortInfo.serialNo] delegate:self];
    conn.tag = URL_TAG_MOVE_TOP;
    [conn start];
    return;
}

@end
