//
//  RYKChatToTargetController.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-9.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@interface RYKChatToTargetController : CTViewController

@property (nonatomic, strong) NSString *msgTarget;
@property (nonatomic, strong) NSString *showName;

@end
