//
//  CustMainController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKCustMainController.h"
#import "RYKModule.h"
#import "RYKTabActController.h"
#import "RYKTabInfoController.h"
#import "RYKTabSaleController.h"
#import "RYKTabMsgViewController.h"

@interface RYKCustMainController ()

- (void)custModuleRecord:(NSNotification *)notification;

@end

@implementation RYKCustMainController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBar.backgroundImage = [UIImage imageNamed:@"TabBg"];
        self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(custModuleRecord:) name:RYK_NOTIFICATION_MODULE object:nil];
    
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.backgroundColor = [UIColor clearColor];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"NavLeftMenu"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"NavLeftMenu"] forState:UIControlStateHighlighted];
    leftBtn.showsTouchWhenHighlighted = YES;
    [leftBtn addTarget:self action:@selector(showOrHideLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.viewControllers.count == 0) {
        
        RYKTabSaleController * sale = [[RYKTabSaleController alloc] initWithNibName:@"RYKTabSaleController" bundle:nil];
        RYKTabInfoController * info = [[RYKTabInfoController alloc] initWithNibName:@"RYKTabInfoController" bundle:nil];
        RYKTabActController * act = [[RYKTabActController alloc] init];
        RYKTabMsgViewController * msg = [[RYKTabMsgViewController alloc] initWithNibName:@"RYKTabMsgViewController" bundle:nil];
        
        self.viewControllers = [NSArray arrayWithObjects:sale, info, act, msg, nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)custModuleRecord:(NSNotification *)notification
{
    NSArray * array = [[notification object] copy];
    if (array == nil || array.count < 2) {
        return;
    }
    [RYKModule recordWithModCode:[array objectAtIndex:0] operateId:[array objectAtIndex:1]];
}

- (void)showOrHideLeftMenu
{
    if ([self.delegate respondsToSelector:@selector(rykCustMainControllerLeftMenuButtonDidClicked:)]) {
        [self.delegate rykCustMainControllerLeftMenuButtonDidClicked:self];
    }
}

@end
