//
//  CustMainController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTTabBarController.h"

@class RYKCustMainController;

@protocol RYKCustMainControllerDelegate <NSObject>

- (void)rykCustMainControllerLeftMenuButtonDidClicked:(RYKCustMainController *)controller;

@end


@interface RYKCustMainController : CTTabBarController

@property (nonatomic, weak) id<RYKCustMainControllerDelegate> delegate;

// 显示或隐藏左侧侧滑菜单
- (void)showOrHideLeftMenu;

@end
