//
//  RYKGoodsLibViewController.m
//  RYKForCustIos
//
//  Created by ArcEsaka on 14/11/18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKGoodsLibViewController.h"
#import "RYKGoodsMgrViewController.h"
#import "CTTableView.h"
#import "RYKWebservice.h"
#import "RYKGoodsLib.h"
#import "RYKGoodsTypeManageViewController.h"
#import "RYKGoodsSortViewController.h"
#import "RYKSaleInfoViewController.h"

@interface RYKGoodsLibViewController ()<UITableViewDataSource,UITableViewDelegate,CTTableViewDelegate,CTURLConnectionDelegate>


@property (nonatomic, strong) IBOutlet UIView *headView;
@property (nonatomic, strong) IBOutlet UILabel *sumGoods;
@property (nonatomic, strong) IBOutlet UILabel *sumYan;
@property (nonatomic, strong) IBOutlet UILabel *sumFyan;
@property (nonatomic, strong) IBOutlet UIView *lView;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;


- (IBAction)btnMgrGoodsClicked:(UIControl *)sender;   // 商品管理点击
- (IBAction)btnCtgGoodsClicked:(UIControl *)sender;   // 类别管理点击
- (IBAction)btnSortGoodsClicked:(UIControl *)sender;  // 商品排序点击

@end

@implementation RYKGoodsLibViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"商品库";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.array = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_SPK, CONS_MODULE_SPK, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        
        CGFloat width = CGRectGetWidth(self.view.bounds) / 3;
        CGFloat diff = width - 107;
        CGRect headerFrame = self.headView.frame;
        headerFrame.size.width = CGRectGetWidth(self.view.bounds);
        headerFrame.size.height = CGRectGetHeight(self.headView.frame) + diff;
        self.headView.frame = headerFrame;
        [self.tv setTableHeaderView:self.headView];
        
        [self loadData];
    }
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getBrandList:[RYKWebService sharedUser].userId] delegate:self];
    [conn start];
}

- (void)drawData:(NSMutableArray *)sarray
{
    if (sarray.count != 0) {
        RYKGoodsLib *item = [sarray objectAtIndex:0];
        self.sumGoods.text = item.sumTotal;
        self.sumYan.text = item.sumTabacco;
        self.sumFyan.text = item.sumOther;
    }
}

- (IBAction)btnMgrGoodsClicked:(UIControl *)sender
{
    RYKGoodsMgrViewController *controller = [[RYKGoodsMgrViewController alloc] initWithNibName:@"RYKGoodsMgrController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)btnCtgGoodsClicked:(UIControl *)sender
{
    RYKGoodsTypeManageViewController *controller = [[RYKGoodsTypeManageViewController alloc] initWithNibName:@"RYKGoodsTypeManageViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)btnSortGoodsClicked:(UIControl *)sender
{
    RYKGoodsSortViewController *controller = [[RYKGoodsSortViewController alloc] initWithNibName:@"RYKGoodsSortViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - CTTableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"CELL";
    UITableViewCell *cell = [self.tv dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *sview = [[[NSBundle mainBundle] loadNibNamed:@"RYKGoodsLibCell" owner:self options:nil] lastObject];
        sview.frame = CGRectMake(0, 0, self.view.frame.size.width, sview.frame.size.height);
        [cell addSubview:sview];
    }
    UILabel *nameLab = (UILabel *)[cell viewWithTag:1];
    UILabel *qtyLab = (UILabel *)[cell viewWithTag:2];
    UIImageView *lineview = (UIImageView *)[cell viewWithTag:3];
    lineview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    if (indexPath.row == self.array.count - 1) {
        lineview.backgroundColor = XCOLOR(202, 202, 202, 1);
    }
    RYKGoodsLib *item = (RYKGoodsLib *)[self.array objectAtIndex:indexPath.row];
    nameLab.text = item.brandName;
    qtyLab.text = item.qtyOrders;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tv deselectRowAtIndexPath:indexPath animated:YES];
    RYKGoodsLib *item = (RYKGoodsLib *)[self.array objectAtIndex:indexPath.row];
    RYKSaleInfoViewController *controller = [[RYKSaleInfoViewController alloc] init];
    controller.curBarCode = item.barCode;
    [self.navigationController pushViewController:controller animated:YES];
}


//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}


#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    RYKGoodsLib *item = [[RYKGoodsLib alloc] init];
    [item parseData:data complete:^(NSArray *array) {
        [self.indicator hide];
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        
        if (self.array.count > 0) {
            self.lView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
        }
        
        if (array.count > 0) {
            RYKGoodsLib *totalInfo = [array objectAtIndex:0];
            self.sumGoods.text = totalInfo.sumTotal;
            self.sumYan.text = totalInfo.sumTabacco;
            self.sumFyan.text = totalInfo.sumOther;
            [self.array removeObjectAtIndex:0];
        }
        [self.tv reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
