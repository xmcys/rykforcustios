//
//  CustMenuController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKCustMenuController.h"
#import "RYKCustMainController.h"
#import "CTUtil.h"
#import "CTNavigationController.h"
#import "RYKWebService.h"
#import "RYKVersion.h"
#import "CTAlertView.h"
#import "RYKAboutController.h"
#import "RYKSettingController.h"
#import <QuartzCore/QuartzCore.h>
#import "ConsSystemMsg.h"
#import "RYKDeviceToken.h"
#import "RYKAppDelegate.h"
#import "RYKXmppManager.h"

#define MENU_WIDTH 240

#define PROCESS_VERSION 0
#define PROCESS_UP_DEVICETOKEN 1

@interface RYKCustMenuController () <RYKCustMainControllerDelegate, UIActionSheetDelegate, CTURLConnectionDelegate, CTAlertViewDelegate>

@property (nonatomic, strong) RYKCustMainController * mainController;
@property (nonatomic, strong) CTNavigationController * navController;
@property (strong, nonatomic) IBOutlet UIView *leftMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (nonatomic, strong) UIControl * cover;
@property (nonatomic) float startX;
@property (nonatomic) BOOL versionChecking;
@property (nonatomic, strong) CTIndicateView * indicator;
@property (nonatomic) BOOL showVersionNotice;
@property (nonatomic, strong) RYKVersion * version;

// 隐藏左侧侧滑菜单
- (void)hideLeftMenu;
- (void)slideLeftMenu:(UIPanGestureRecognizer *)gesture;
- (void)initNavController;
- (IBAction)logout:(UIButton *)sender;
- (IBAction)about:(UIButton *)sender;
- (IBAction)settting:(UIButton*)sender;
- (IBAction)checkVersion:(UIButton *)sender;
- (void)upDeviceToken:(NSString *)token;

@end

@implementation RYKCustMenuController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recvLogOutNotify:) name:RYK_NOTIFICATION_LOGOUT object:nil];
    
    self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    self.startX = 0;
    [self.btnLogout setBackgroundImage:[[UIImage imageNamed:@"BtnGreenNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 3, 10, 3)] forState:UIControlStateNormal];
    [self.btnLogout setBackgroundImage:[[UIImage imageNamed:@"BtnGreenHighLight"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 3, 10, 3)] forState:UIControlStateHighlighted];
    [self.view addSubview:self.leftMenu];
    self.leftMenu.hidden = YES;
    
    [self checkVersion:nil];
    
    if ([RYKDeviceToken sharedManager].deviceToken.length > 0) {
        [self upDeviceToken:[RYKDeviceToken sharedManager].deviceToken];
        return;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self initNavController];
    
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)recvLogOutNotify:(NSNotification *)notification
{
    [self.navController.view removeFromSuperview];
    [RYKWebService setUser:nil];
    self.mainController = nil;
    self.navController = nil;
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:@"USER_CODE"];
    [defaults setValue:@"" forKey:@"PASSWORD"];
    
    RYKAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.window.rootViewController = appDelegate.signIn;
    
    [[RYKXmppManager sharedManager] disconnect];
    return;
}

- (void)hideLeftMenu
{
    [self rykCustMainControllerLeftMenuButtonDidClicked:self.mainController];
}

- (void)slideLeftMenu:(UIPanGestureRecognizer *)gesture
{
    if (self.navController.view.frame.origin.x > MENU_WIDTH) {
        return;
    }
    CGPoint point = [gesture locationInView:self.view];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        self.startX = point.x;
    } else if (gesture.state == UIGestureRecognizerStateEnded){
        int offsetX = point.x - self.startX;
        offsetX = offsetX < 0 ? 0 : offsetX;
        offsetX = offsetX > MENU_WIDTH ? MENU_WIDTH : offsetX;
        if (offsetX > MENU_WIDTH / 3) {
            [UIView animateWithDuration:0.2 animations:^{
                self.navController.view.frame = CGRectMake(MENU_WIDTH + 1, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
                self.cover.hidden = NO;
                [self.view bringSubviewToFront:self.cover];
            }];
        } else {
            [UIView animateWithDuration:0.2 animations:^{
                self.navController.view.frame = CGRectMake(0, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
                self.cover.hidden = YES;
            }];
        }
    } else {
        int offsetX = point.x - self.startX;
        offsetX = offsetX < 0 ? 0 : offsetX;
        offsetX = offsetX > MENU_WIDTH ? MENU_WIDTH : offsetX;
        self.navController.view.frame = CGRectMake(offsetX, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
    }
}

- (void)initNavController
{
    if (self.mainController == nil) {
        self.mainController = [[RYKCustMainController alloc] initWithNibName:nil bundle:nil];
        self.mainController.delegate = self;
        self.navController = [[CTNavigationController alloc] initWithRootViewController:self.mainController];
        [self.navController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                  [UIColor whiteColor], UITextAttributeTextColor,nil]];
        [self.navController setNavigationBarBackgroundImage:@"NavBg-44" imgName64:@"NavBg-64"];
        self.navController.view.layer.shadowRadius = 5.0f;
        self.navController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.navController.view.layer.shadowOpacity = 0.5;
        if (VERSION_LESS_THAN_IOS7) {
            self.view.bounds = CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height);
            self.leftMenu.frame = CGRectMake(0, 20, MENU_WIDTH + 1, self.view.bounds.size.height);
        } else {
            self.leftMenu.frame = CGRectMake(0, 0, MENU_WIDTH + 1, self.view.bounds.size.height);
        }
        
        self.btnLogout.center = CGPointMake(self.leftMenu.frame.size.width / 2, self.leftMenu.frame.size.height - 10 - self.btnLogout.frame.size.height / 2);
        
        [self.view addSubview:self.navController.view];
        
//        UIPanGestureRecognizer * pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(slideLeftMenu:)];
//        [self.view addGestureRecognizer:pan];
        
        self.leftMenu.hidden = NO;
    }
    
    if (self.cover == nil) {
        self.cover = [[UIControl alloc] initWithFrame:CGRectMake(MENU_WIDTH, 0, self.view.bounds.size.width - MENU_WIDTH, self.view.bounds.size.height)];
        self.cover.hidden = YES;
        self.cover.backgroundColor = [UIColor clearColor];
        [self.cover addTarget:self action:@selector(hideLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cover];
    }
}

- (void)logout:(UIButton *)sender
{
    UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:@"确定注销当前账号？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定" otherButtonTitles: nil];
    action.tag = 1;
    [action showInView:self.view];
}

- (void)about:(UIButton *)sender
{
    RYKAboutController * controller = [[RYKAboutController alloc] initWithNibName:@"RYKAboutController" bundle:nil];
    [self.navController pushViewController:controller animated:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navController.view.frame = CGRectMake(0, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
        self.cover.hidden = YES;
    }];
}

- (IBAction)settting:(UIButton*)sender
{
    RYKSettingController * controller = [[RYKSettingController alloc] initWithNibName:@"RYKSettingController" bundle:nil];
    [self.navController pushViewController:controller animated:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navController.view.frame = CGRectMake(0, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
        self.cover.hidden = YES;
    }];
    return;
}

- (void)checkVersion:(UIButton *)sender
{
    if (sender != nil) {
        self.showVersionNotice = YES;
        if (self.indicator.showing) {
            return;
        }
        self.indicator.text = @"正在检查更新..";
        [self.indicator show];
    } else {
        self.showVersionNotice = NO;
    }
    
    if (self.versionChecking) {
        return;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navController.view.frame = CGRectMake(0, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
        self.cover.hidden = YES;
    }];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService cmappVersionCheckUrl] delegate:self];
    conn.tag = PROCESS_VERSION;
    [conn start];
}

- (void)upDeviceToken:(NSString *)token
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService upDeviceToken:token] delegate:self];
    conn.tag = PROCESS_UP_DEVICETOKEN;
    [conn start];
    return;
}

#pragma mark -
#pragma mark CustMainController Delegate
- (void)rykCustMainControllerLeftMenuButtonDidClicked:(RYKCustMainController *)controller
{
    if (self.navController.view.frame.origin.x == 0) {
        [UIView animateWithDuration:0.2 animations:^{
            self.navController.view.frame = CGRectMake(MENU_WIDTH + 1, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
            self.cover.hidden = NO;
            [self.view bringSubviewToFront:self.cover];
        }];
        
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.navController.view.frame = CGRectMake(0, 0, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
            self.cover.hidden = YES;
        }];
    }
}

#pragma mark -
#pragma mark UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_LOGOUT object:nil];
    }
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == PROCESS_VERSION) {
        connection.delegate = nil;
        connection = nil;
        self.indicator.text = @"版本更新失败";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.2];
    } else if (connection.tag == PROCESS_UP_DEVICETOKEN) {
        NSLog(@"upDeviceToken failed!");
    }
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_VERSION) {
        self.version = [[RYKVersion alloc] init];
        [self.version parseData:data];
        NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
        int current = [[infoDict objectForKey:@"CFBundleVersion"] intValue];
        [self.indicator hide];
        if (self.version.buildCode != nil && [self.version.buildCode intValue] > current) {
            CTAlertView * alert = [[CTAlertView alloc] initWithTitle:[NSString stringWithFormat:@"有新版本啦  %@", self.version.versionNo] message:[NSString stringWithFormat:@"更新内容：%@", self.version.content] delegate:self cancelButtonTitle:@"下次再说" confirmButtonTitle:@"马上更新"];
            alert.tag = 1;
            [alert show];
        } else {
            if (self.showVersionNotice) {
                self.indicator.text = @"系统已是最新版本";
                [self.indicator autoHide:CTIndicateStateDone];
            }
            
        }
    } else if (connection.tag == PROCESS_UP_DEVICETOKEN) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *tagMsg) {
        }];
    }
}

#pragma mark -
#pragma mark CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (alertView.tag == 1 && index == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.version.url]];
    }
}

@end
