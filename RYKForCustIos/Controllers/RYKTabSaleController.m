//
//  RYKTabSaleController.m
//  RYKForCustIos
//
//  Created by 宏超 陈 on 14-6-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKTabSaleController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKUserMessage.h"
#import "RYKCustOrderStatus.h"
#import "RYKOrderListViewController.h"
#import "RYKGoodsMgrViewController.h"
#import "RYKOrderDetailViewController.h"
#import "RYKCustomerInfoViewController.h"
#import "ConsSystemMsg.h"
#import "RYKXmppManager.h"
#import "RYKGoodsLibViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "RYKOrderShare.h"

#define PROCESS_GET_USER_MESSAGE 1
#define PROCESS_LOAD_UNCHECKED_ORDER 2
#define PROCESS_LOAD_SHARE 3

@interface RYKTabSaleController () <CTTableViewDelegate, UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate, RYKOrderDetailViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *labShopName;
@property (strong, nonatomic) IBOutlet UILabel *labUserCode;
@property (strong, nonatomic) IBOutlet UILabel *labNewCareCnt;
@property (strong, nonatomic) IBOutlet UILabel *labTotalCareCnt;
@property (strong, nonatomic) IBOutlet UILabel *labCompleteOrderCnt;
@property (strong, nonatomic) IBOutlet UILabel *labNewOrderCnt;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcControlWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcControlHeight;
@property (nonatomic, strong) CTTableView *tv;  // 未处理订单列表
@property (nonatomic, strong) NSMutableArray *orderArray;

- (IBAction)btnMgrOrderClicked:(UIControl *)sender;   // 订单管理点击
- (IBAction)btnMgrGoodsClicked:(UIControl *)sender;   // 商品库点击
- (IBAction)btnMgrOnSaleClicked:(UIControl *)sender;  // 促销管理点击
- (IBAction)userMessageClicked:(UIControl *)sender;   // 顶部关注消费者人数点击
- (void)loadUserMessageData;
- (void)loadUnCheckedData;
- (void)drawUserInfo;

@end

@implementation RYKTabSaleController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"销售管理" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:116.0/255.0 alpha:1] image:[UIImage imageNamed:@"TabSale"] selectedImage:[UIImage imageNamed:@"TabSaleHighlighted"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_XSGL, CONS_MODULE_XSGL, nil]];
    
    self.orderArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"销售管理";
    
    UIButton *btnshare = [UIButton buttonWithType:UIButtonTypeCustom];
    btnshare.frame = CGRectMake(0, 0, 26, 26);
    [btnshare setImage:[UIImage imageNamed:@"Share"] forState:UIControlStateNormal];
    [btnshare addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
    self.tabBarController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnshare];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        
        CGFloat width = CGRectGetWidth(self.view.bounds) / 3;
        CGFloat wannaHeight = width * 119 / 107;
        CGFloat diff = width - self.alcControlWidth.constant;
        self.alcControlWidth.constant = width;
        self.alcControlHeight.constant = wannaHeight;
        CGRect headerFrame = self.headerView.frame;
        headerFrame.size.width = CGRectGetWidth(self.view.bounds);
        headerFrame.size.height = headerFrame.size.height + diff;
        self.headerView.frame = headerFrame;
        [self.tv setTableHeaderView:self.headerView];
        
        [self loadUserMessageData];
    }
    
    [self loadUnCheckedData];
//    [self drawUserInfo];
}

- (void)shareClick:(UIButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在获取分享数据..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getLinkAddress:[RYKWebService sharedUser].userId] delegate:self];
    conn.tag = PROCESS_LOAD_SHARE;
    [conn start];
}


- (IBAction)btnMgrOrderClicked:(UIControl *)sender
{
    RYKOrderListViewController *controller = [[RYKOrderListViewController alloc] initWithNibName:@"RYKOrderListViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)btnMgrGoodsClicked:(UIControl *)sender
{
    RYKGoodsLibViewController *controller = [[RYKGoodsLibViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)btnMgrOnSaleClicked:(UIControl *)sender
{
    RYKGoodsMgrViewController *controller = [[RYKGoodsMgrViewController alloc] initWithNibName:@"RYKGoodsMgrController" bundle:nil];
    controller.curType = 2;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)userMessageClicked:(UIControl *)sender
{
    RYKCustomerInfoViewController *controller = [[RYKCustomerInfoViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (void)loadUserMessageData
{
    if (self.indicator.showing) {
        return;
    }
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getUserMessage] delegate:self];
    conn.tag = PROCESS_GET_USER_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (void)loadUnCheckedData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderFlow:@"STORE_CONFIRM" withFlowStatus:@"0"] delegate:self];
    conn.tag = PROCESS_LOAD_UNCHECKED_ORDER;
    [conn start];
    return;
}

- (void)drawUserInfo
{
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadUserMessageData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.orderArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKUnHandledOrderCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 5, tableView.frame.size.width - 10, view.frame.size.height);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        
        UIView *viewLine = (UIView *)[view viewWithTag:11];
        viewLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
        
        [cell addSubview:view];
    }
    
    UILabel *labOrderId = (UILabel *)[cell viewWithTag:1];
    UILabel *labDate = (UILabel *)[cell viewWithTag:2];
    UILabel *labGoodsCnt = (UILabel *)[cell viewWithTag:3];
    UILabel *labMoney = (UILabel *)[cell viewWithTag:4];
    
    RYKCustOrderStatus *curOrderStatus = (RYKCustOrderStatus *)[self.orderArray objectAtIndex:indexPath.row];
    labOrderId.text = curOrderStatus.orderId;
    labDate.text = curOrderStatus.orderDate;
    labGoodsCnt.text = curOrderStatus.qtyOrderSum;
    labMoney.text = [NSString stringWithFormat:@"金额：%@", curOrderStatus.amtOrderSum];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RYKCustOrderStatus *curOrder = [self.orderArray objectAtIndex:indexPath.row];
    curOrder.flowCode = @"STORE_CONFIRM";
    RYKOrderDetailViewController *controller = [[RYKOrderDetailViewController alloc] initWithNibName:@"RYKOrderDetailController" bundle:nil];
    controller.curOrder = curOrder;
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_USER_MESSAGE) {
        RYKUserMessage * curMessage = [[RYKUserMessage alloc] init];
        [curMessage parseData:data complete:^(RYKUserMessage *msg){
            [self.indicator hide];
            self.indicator.showing = NO;
            self.labNewCareCnt.text = msg.user_num;
            self.labTotalCareCnt.text = msg.userTotalNum;
            self.labCompleteOrderCnt.text = msg.order_num;
            self.labShopName.text = msg.user_name;
            self.labUserCode.text = msg.user_code;
            [RYKWebService sharedUser].userName = msg.user_name;
            [self loadUnCheckedData];
        }];
    } else if (connection.tag == PROCESS_LOAD_UNCHECKED_ORDER) {
        RYKCustOrderStatus * orderStatus = [[RYKCustOrderStatus alloc] init];
        [orderStatus parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.orderArray removeAllObjects];
            
            [self.orderArray addObjectsFromArray:array];
            [self.tv reloadData];
            self.labNewOrderCnt.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.orderArray.count];
        }];
    } else if (connection.tag == PROCESS_LOAD_SHARE) {
        [self.indicator hide];
        RYKOrderShare *curShare = [[RYKOrderShare alloc] init];
        [curShare parseData:data complete:^(RYKOrderShare *share) {
            //构造分享内容
            id<ISSContent> publishContent = [ShareSDK content:share.summary
                                               defaultContent:share.summary
                                                        image:[ShareSDK pngImageWithImage:[UIImage imageNamed:@"ShareIcon"]]
                                                        title:@"快来小店看看吧！"
                                                          url:share.shareUrl
                                                  description:@"快来小店看看吧！"
                                                    mediaType:SSPublishContentMediaTypeNews];
            [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:share.summary title:share.summary url:share.shareUrl thumbImage:nil image:[ShareSDK pngImageWithImage:[UIImage imageNamed:@"ShareIcon"]] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
            
            //创建弹出菜单容器
            id<ISSContainer> container = [ShareSDK container];
            [container setIPadContainerWithView:self.view arrowDirect:UIPopoverArrowDirectionUp];
            
            //弹出分享菜单
            [ShareSDK showShareActionSheet:container
                                 shareList:nil
                                   content:publishContent
                             statusBarTips:YES
                               authOptions:nil
                              shareOptions:nil
                                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                        
                                        if (state == SSResponseStateSuccess)
                                        {
                                            NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                        }
                                        else if (state == SSResponseStateFail)
                                        {
                                            NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                        }
                                    }];
        }];
    }
    return;
}

#pragma mark -
#pragma mark RYKOrderDetailViewController Delegate
- (void)rykOrderDetailViewControllerConfirmOK
{
    [self loadUnCheckedData];
    return;
}

@end
