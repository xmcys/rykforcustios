//
//  RYKGoodsMgrViewController.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@interface RYKGoodsFilterCondition : NSObject

@property (nonatomic, strong) NSString *searchName;
@property (nonatomic, strong) NSString *typeName;
@property (nonatomic, strong) NSString *minPrice;
@property (nonatomic, strong) NSString *maxPrice;
@property (nonatomic, strong) NSString *pageStart;
@property (nonatomic, strong) NSString *pageEnd;
@property (nonatomic, strong) NSString *stateName;
@property (nonatomic, strong) NSString *discountFlag;
@property (nonatomic) NSInteger tag;

@end

@interface RYKGoodsMgrViewController : CTViewController

@property (nonatomic) NSInteger curType;

@end
