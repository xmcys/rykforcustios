//
//  RYKAddGoodsViewController.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"
#import "RYKGoods.h"

@protocol RYKAddGoodsViewControllerDelegate<NSObject>

- (void)rykAddGoodsViewControllerModifyOK:(RYKGoods *)goods;

@end

@interface RYKAddGoodsViewController : CTViewController

@property (nonatomic, weak) id<RYKAddGoodsViewControllerDelegate> delegate;
- (void)setGoodsInfo:(RYKGoods *)tagGoods;

@end
