//
//  RYKMsgReplyViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-25.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKMsgReplyViewController.h"
#import "CTAlertView.h"
#import "RYKWebService.h"
#import "ConsSystemMsg.h"
#import "RYKMsgListViewController.h"

#define PROCESS_POST_DELETEMSG 1
#define PROCESS_POST_SAVEMSG 2


@interface RYKMsgReplyViewController ()<UITextViewDelegate,CTAlertViewDelegate>
@property (nonatomic, strong) IBOutlet UIView *fullView;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) UILabel         *defautlab;
@property (nonatomic, strong) IBOutlet UIImageView     *img1;     //虚线
@property (nonatomic, strong) IBOutlet UIImageView     *img2;
@property (nonatomic, strong) IBOutlet UIImageView     *img3;
@property (nonatomic, strong) IBOutlet UILabel         *timelab;
@property (nonatomic, strong) IBOutlet UILabel         *phonelab;
@property (nonatomic, strong) IBOutlet UILabel         *namelab;
@property (nonatomic, strong) IBOutlet UILabel         *contlab;

- (IBAction)callLinkPhone:(id)sender;


@end

@implementation RYKMsgReplyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.fullView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.fullView.bounds.size.height);
    self.fullView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.fullView];
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    self.title = @"留言处理";
    self.textView.layer.cornerRadius = 5;
    self.textView.layer.borderColor = [[UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:0.5] CGColor];
    self.textView.layer.borderWidth = 1;
    self.textView.layer.masksToBounds = YES;
    self.textView.delegate = self;
    
    self.defautlab = [[UILabel alloc] init];
    self.defautlab.frame = CGRectMake(7, 5, 150, 20);
    self.defautlab.text = @"点击回复留言";
    self.defautlab.enabled = NO;
    self.defautlab.font = [UIFont systemFontOfSize:15.0];
    self.defautlab.textColor = [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1];
    [self.textView addSubview:self.defautlab];
    
    
    //虚线处理
    self.img1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    self.img2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    self.img3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];

    
    self.timelab.text = self.msg.opinionDate;
    self.namelab.text = self.msg.userName;
    self.phonelab.text = self.msg.linkPhone;
    self.contlab.text = self.msg.opinionCont;
    if ([self.phonelab.text isEqualToString:@"null"]) {
        self.phonelab.text = @"";
    }
    
    UIButton *btndelete = [UIButton buttonWithType:UIButtonTypeCustom];
    btndelete.frame = CGRectMake(0, 0, 20, 20);
    btndelete.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"close"]];
    [btndelete addTarget:self action:@selector(deleteclick:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *btnsure = [UIButton buttonWithType:UIButtonTypeCustom];
    btnsure.frame = CGRectMake(32, 0, 20, 20);
    btnsure.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"提交按钮"]];
    [btnsure addTarget:self action:@selector(sureclick:) forControlEvents:UIControlEventTouchUpInside];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 22)];
    [view addSubview:btndelete];
    [view addSubview:btnsure];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = bar;
}

- (void)deleteclick:(UIButton *)btn
{
    CTAlertView *alert = [[CTAlertView alloc] initWithTitle:nil message:@"确定删除本条留言?" delegate:self cancelButtonTitle:@"稍后再说" confirmButtonTitle:@"确定删除"];
    alert.tag = 1;
    [alert show];
}

- (void)sureclick:(UIButton *)btn
{
    if (self.textView.text.length == 0) {
        self.indicator.text = @"回复的留言不能为空";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><opinionSeq>%@</opinionSeq><answerPerson>%@</answerPerson><answerCont>%@</answerCont></Test>", [RYKWebService sharedUser].userId, self.msg.opinionSeq, @"", self.textView.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getSaveMessage] params:params delegate:self];
    conn.tag = PROCESS_POST_SAVEMSG;
    [conn start];
}

- (IBAction)callLinkPhone:(id)sender
{
    CTAlertView *alertview = [[CTAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"是否呼叫%@",self.msg.linkPhone] delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"确定"];
    alertview.tag = 2;
    [alertview show];
}


#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (self.textView.text.length == 0) {
        self.defautlab.text = @"点击回复留言";
    }
    else{
        self.defautlab.text = @"";
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textView resignFirstResponder];
}

#pragma mark - CTAlertview Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (alertView.tag == 1) {
        if (index == 0) {
            return;
        }
        if (self.indicator.showing) {
            return;
         }
        NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><opinionSeq>%@</opinionSeq></Test>",[RYKWebService sharedUser].userId,self.msg.opinionSeq];
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getDeleteMessage] params:params delegate:self];
        conn.tag = PROCESS_POST_DELETEMSG;
        [conn start];
        self.indicator.text = @"删除中..";
        [self.indicator show];
        return;
    }else if (alertView.tag == 2){
        if (index == 0) {
            return;
        }
        if ([self.phonelab.text isEqualToString:@"null"]) {
            return;
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.msg.linkPhone]]];
    }
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    
    if(connection.tag == PROCESS_POST_DELETEMSG){
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.text = @"删除成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate RefreshGetMoreMsg:YES];
        }
    } else if (connection.tag == PROCESS_POST_SAVEMSG) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.text = @"回复成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0 complete:^(void){
                [self.delegate RefreshGetMoreMsg:YES];
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        }
    }
}




@end
