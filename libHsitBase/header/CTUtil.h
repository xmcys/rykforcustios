//
//  CTUtil.h
//  ChyoTools
//
//  此文件基于IOS SDK7.0编译，如果使用IOS SDK6及以下编译部分方法可能会出问题，如位置偏差等
//
//
//  Created by 陈 宏超 on 13-12-27.
//  Copyright (c) 2013年 Chyo. All rights reserved.
//

#import <Foundation/Foundation.h>

// 判断系统版本
#define VERSION_LESS_THAN_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
#define VERSION_LESS_THAN_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
#define VERSION_LESS_THAN_IOS6 ([[[UIDevice currentDevice] systemVersion] floatValue] < 6.0)

// 判断设备型号
#define DEVICE_IS_IPHONE5 (CGSizeEqualToSize([[[UIScreen mainScreen] currentMode] size], CGSizeMake(640, 1136)))


@interface CTUtil : NSObject

@end
