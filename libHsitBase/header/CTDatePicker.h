//
//  TIIDatePicker.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-6-9.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTDatePicker;

@protocol CTDatePickerDelegate <NSObject>

@optional
- (void)ctDatePicker:(CTDatePicker *)datePicker didSelectDate:(NSString *)date;

@end

@interface CTDatePicker : UIView

@property (nonatomic, weak) id<CTDatePickerDelegate> delegate;
@property (nonatomic) NSInteger tag;

- (id)initWithDateFormatter:(NSString *)dateFormatter;

- (void)show;
- (void)dismiss;
- (void)setCurrentDate:(NSString *)currentDate;
- (NSString *)selectedFormattedDate;

@end
