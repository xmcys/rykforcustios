//
//  CTIndicateView.h 加载指示器
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-9.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CTIndicateState) {
    CTIndicateStateLoading,   // 加载（旋转）
    CTIndicateStateDone,      // 执行完毕
    CTIndicateStateWarning    // 警告
};

@interface CTIndicateView : UIView

@property (nonatomic) BOOL showing;  // 标识视图是否显示
@property (nonatomic) BOOL backgroundTouchable;  // 标识视图以外区域是否可以点击
@property (nonatomic, strong) NSString * text; // 提示文本（默认 加载中...)
@property (nonatomic, readonly) CTIndicateState currentState;  // 当前状态

// 使用此方法来实例化类，父类不需要调用addSubView方法
- (id)initInView:(UIView *)view;
// 显示指示器，默认CTIndicateStateLoading
- (void)show;
// 显示指示器
- (void)showWithState:(CTIndicateState)state;
// 隐藏指示器，默认currentState
- (void)hide;
// 隐藏指示器
- (void)hideWithSate:(CTIndicateState)state afterDelay:(float)timeInterval;
// 显示指示器，隔一段时间后自动隐藏
- (void)autoHide:(CTIndicateState)state;
- (void)autoHide:(CTIndicateState)state afterDelay:(float)timeInterval;
// 隐藏指示器
- (void)hideWithSate:(CTIndicateState)state afterDelay:(float)timeInterval complete:(void(^)())block;

// 隐藏指示器
- (void)autoHide:(CTIndicateState)state afterDelay:(float)timeInterval complete:(void(^)())block;
- (void)startAnimating;
- (void)stopAnimating;
@end
