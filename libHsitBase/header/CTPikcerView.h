//
//  CTPikcerView.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTPikcerView;

@protocol CTPikcerViewDelegate <NSObject>

@optional

- (void)ctPickerView:(CTPikcerView *)pickerView didClickedButtonOnIndex:(NSInteger)index;

@end

@interface CTPikcerView : UIView

@property (nonatomic, strong) NSArray * keyArray;
@property (nonatomic, strong) NSArray * valueArray;
@property (nonatomic, weak) id<CTPikcerViewDelegate> delegate;
@property (nonatomic) NSInteger tag;

- (id)initWithComponentNum:(NSInteger)componentNum keyArray:(NSArray *)keyArray valueArray:(NSArray *)valueArray delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle confirmButtonTitle:(NSString *)confirmButtonTitle;
- (UIPickerView *)pickerView;
- (void)show;
- (void)dismiss;

@end
