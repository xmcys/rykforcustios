//
//  CTSmoothChooseView.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTSmoothChooseView;

@protocol CTSmoothChooseViewDelegate <NSObject>

@optional
- (void)ctSmoothChooseView:(CTSmoothChooseView *)smoothChooseView didTouchedAtIndex:(int)index;

@end

@interface CTSmoothChooseView : UIView

@property (nonatomic, strong) NSArray * words;
@property (nonatomic) int topOffset;
@property (nonatomic) int rightOffset;
@property (nonatomic) int bottomOffset;
@property (nonatomic, weak) id<CTSmoothChooseViewDelegate> delegate;

- (id)initInView:(UIView *)view;
- (void)reset;

@end
